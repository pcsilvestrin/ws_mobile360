unit UntPrincipal;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.Memo.Types,
  FMX.ScrollBox, FMX.Memo, FMX.Controls.Presentation, FMX.StdCtrls;

type
  TfrmPrincipal = class(TForm)
    Label1: TLabel;
    memo: TMemo;
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmPrincipal: TfrmPrincipal;

implementation

{$R *.fmx}

uses Horse,
     Horse.Jhonson,
     Horse.CORS,
     Horse.OctetStream,  //Para trabalhar com Stream
     Horse.Upload,       //Para trabalhar com Upload de Arquivos
     Controllers.Usuario,
     Controllers.Cliente,
     Controllers.Produto,
     Controllers.Pedido,
     Controllers.CondPagto,
     Controllers.Notificacao;

procedure TfrmPrincipal.FormShow(Sender: TObject);
begin
   THorse.Use(Jhonson());
   THorse.Use(CORS);
   THorse.Use(OctetStream);
   THorse.Use(Upload);

   //Resgistra as rotas
   Controllers.Usuario.RegistrarRotas;
   Controllers.Cliente.RegistrarRotas;
   Controllers.Produto.RegistrarRotas;
   Controllers.Pedido.RegistrarRotas;
   Controllers.CondPagto.RegistrarRotas;
   Controllers.Notificacao.RegistrarRotas;

   THorse.Listen(9000);
   memo.Lines.Add('Servidor rodando na porta: '+THorse.Port.ToString);
end;

end.
