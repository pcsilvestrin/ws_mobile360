unit DataModule.Global;

interface

uses
  System.SysUtils, System.Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.FB,
  FireDAC.Phys.FBDef, FireDAC.FMXUI.Wait, Data.DB, FireDAC.Comp.Client,
  FireDAC.Phys.IBBase, System.IniFiles, DataSet.Serialize.Config, FireDAC.DApt,
  System.JSON, DataSet.Serialize, uMD5, FMX.Graphics, IdSSLOpenSSLHeaders,
  System.Variants;

type
  TdtmGlobal = class(TDataModule)
    connection: TFDConnection;
    FDPhysFBDriverLink: TFDPhysFBDriverLink;
    procedure DataModuleCreate(Sender: TObject);
    procedure connectionBeforeConnect(Sender: TObject);
  private
    procedure CarregarConfigDB(Connection: TFDConnection);
    function emailjacadastrado(email: string; cod_usuario: integer=0): boolean;
    function ListarItensPedido(cod_pedido: integer; qry: TFDQuery): TJsonArray;
    { Private declarations }
  public
    { Public declarations }
    //Usu�rios
    function login(email, senha: string): TJSONObject;
    function InserirUsuario(nome, email, senha: string): TJSONObject;
    function push(cod_usuario: integer; token_push: string): TJSONObject;
    function EditarUsuario(cod_usuario: integer; nome, email: string): TJSONObject;
    function ExcluirUsuario(cod_usuario: integer): TJsonObject;
    function EditarSenha(cod_usuario: integer; senha: string): TJSONObject;

    //Notifica��es
    function ListarNotificacoes(cod_usuario: integer): TJSONArray;

    //Clientes
    function ListarClientes(dt_ult_sincronizacao: string; pagina: integer): TJSONArray;
    function InserirEditarCliente(cod_usuario, cod_cliente_local: integer;
      cnpj_cpf, nome, fone, email, endereco, numero, complemento, bairro,
      cidade, uf, cep: string; latitude, longitude, limite_disponivel: double;
      cod_cliente_oficial: integer; dt_ult_sincronizacao: string): TJsonObject;

    //Produtos
    function ListarProdutos(dt_ult_sincronizacao: string; pagina: integer): TJsonArray;
    function InserirEditarProduto(cod_usuario, cod_produto_local: integer;
      descricao: string; valor, qtd_estoque: double;
      cod_produto_oficial: integer; dt_ult_sincronizacao: string): TJsonObject;
    function ListarFoto(cod_produto: integer): TMemoryStream;
    procedure EditarFoto(cod_produto: integer; foto: TBitmap);

    //Pedidos
    function ListarPedidos(dt_ult_sincronizacao: string; cod_usuario,
      pagina: integer): TJsonArray;
    function InserirEditarPedido(cod_usuario, cod_pedido_local,
      cod_cliente: integer; tipo_pedido, data_pedido, contato, obs: string;
      valor_total: double; cod_cond_pagto: integer; prazo_entrega,
      data_entrega: string; cod_pedido_oficial: integer;
      dt_ult_sincronizacao: string; itens: TJsonArray): TJsonObject;

    //Condi��o de Pagamento
    function ListarCondPagto: TJsonArray;
  end;

var
  dtmGlobal: TdtmGlobal;

const
  QTD_REG_PAGINA_CLIENTE = 15;
  QTD_REG_PAGINA_PRODUTO = 15;
  QTD_REG_PAGINA_PEDIDO  = 5;

implementation

{%CLASSGROUP 'FMX.Controls.TControl'}

{$R *.dfm}

procedure TDtmGlobal.CarregarConfigDB(Connection: TFDConnection);
var
    ini : TIniFile;
    arq: string;
begin
    try
        // Caminho do INI...
        arq := ExtractFilePath(ParamStr(0)) + 'config.ini';

        // Validar arquivo INI...
        if NOT FileExists(arq) then
            raise Exception.Create('Arquivo INI n�o encontrado: ' + arq);

        // Instanciar arquivo INI...
        ini := TIniFile.Create(arq);
        Connection.DriverName := ini.ReadString('Banco de Dados', 'DriverID', '');

        // Buscar dados do arquivo fisico...
        with Connection.Params do
        begin
            Clear;
            Add('DriverID=' + ini.ReadString('Banco de Dados', 'DriverID', ''));
            Add('Database=' + ini.ReadString('Banco de Dados', 'Database', ''));
            Add('User_Name=' + ini.ReadString('Banco de Dados', 'User_name', ''));
            Add('Password=' + ini.ReadString('Banco de Dados', 'Password', ''));

            if ini.ReadString('Banco de Dados', 'Port', '') <> '' then
                Add('Port=' + ini.ReadString('Banco de Dados', 'Port', ''));

            if ini.ReadString('Banco de Dados', 'Server', '') <> '' then
                Add('Server=' + ini.ReadString('Banco de Dados', 'Server', ''));

            if ini.ReadString('Banco de Dados', 'Protocol', '') <> '' then
                Add('Protocol=' + ini.ReadString('Banco de Dados', 'Protocol', ''));

            if ini.ReadString('Banco de Dados', 'VendorLib', '') <> '' then
                FDPhysFBDriverLink.VendorLib := ini.ReadString('Banco de Dados', 'VendorLib', '');
        end;

    finally
        if Assigned(ini) then
            ini.DisposeOf;
    end;
end;

procedure TdtmGlobal.connectionBeforeConnect(Sender: TObject);
begin
   TDataSetSerializeConfig.GetInstance.CaseNameDefinition := cndLower; //Ao montar o JSON, n�o altera o nome do campo
   TDataSetSerializeConfig.GetInstance.Import.DecimalSeparator := '.'; //Define o caracter separador

   CarregarConfigDB(connection);
end;

procedure TdtmGlobal.DataModuleCreate(Sender: TObject);
begin
   connection.Connected := true;
end;

function TdtmGlobal.login(email, senha: string): TJSONObject;
var qry : TFDQuery;
begin
   if (email.trim.IsEmpty) or (senha.Trim.IsEmpty) then begin
      raise Exception.Create('Informe o email e a senha');
   end;

   try
     qry := TFDQuery.Create(nil);
     qry.Connection := connection;
     qry.Active := false;
     qry.SQL.Add('select cod_usuario, nome, email');
     qry.SQL.Add('from tab_usuario');
     qry.SQL.Add('where email = :email and senha = :senha');

     qry.ParamByName('email').Value := email;
     qry.ParamByName('senha').Value := SaltPassword(senha);

     qry.Active := true;

     result := qry.ToJSONObject;
     {"cod_usuario": 1, "nome": "paulo", "email": "pcsilvestrin@hotmail.com"}
   finally
     freeAndNil(qry);
   end;
end;

function TdtmGlobal.InserirUsuario(nome, email, senha: string): TJSONObject;
var qry : TFDQuery;
begin
   if (nome.trim.IsEmpty) or (email.trim.IsEmpty) or (senha.Trim.IsEmpty) then begin
      raise Exception.Create('Informe o email e a senha do usu�rio');
   end;

   if (senha.Length < 5) then begin
      raise Exception.Create('A senha deve ter no m�nimo 5 caracteres');
   end;

   if (emailjacadastrado(email)) then begin
      raise Exception.Create('O email informado j� est� sendo utilizado.');
   end;

   try
     qry := TFDQuery.Create(nil);
     qry.Connection := connection;
     qry.Active := false;
     qry.SQL.Add('insert into tab_usuario (nome, email, senha, ind_excluido)');
     qry.SQL.Add('values (:nome, :email, :senha, :ind_excluido)');
     qry.SQL.Add('returning cod_usuario ');

     qry.ParamByName('nome').Value  := nome;
     qry.ParamByName('email').Value := email;
     qry.ParamByName('senha').Value := SaltPassword(senha); //Criptografa a senha
     qry.ParamByName('ind_excluido').Value := 'N'; //Criptografa a senha

     qry.Active := true;

     result := qry.ToJSONObject;
     {"cod_usuario": 1"}
   finally
     freeAndNil(qry);
   end;
end;

function TdtmGlobal.push(cod_usuario: integer; token_push: string): TJSONObject;
var qry : TFDQuery;
begin
   if (token_push.trim.IsEmpty) then begin
      raise Exception.Create('Informe o token push do usu�rio!');
   end;

   try
     qry := TFDQuery.Create(nil);
     qry.Connection := connection;
     qry.Active := false;
     qry.SQL.Add('update tab_usuario set token_push = :token_push ');
     qry.SQL.Add('where cod_usuario = :cod_usuario ');
     qry.SQL.Add('returning cod_usuario ');

     qry.ParamByName('cod_usuario').Value  := cod_usuario;
     qry.ParamByName('token_push').Value   := token_push;

     qry.Active := true;

     result := qry.ToJSONObject;
     {"cod_usuario": 1"}
   finally
     freeAndNil(qry);
   end;
end;

function TdtmGlobal.EditarUsuario(cod_usuario: integer; nome, email: string): TJSONObject;
var qry : TFDQuery;
begin
   if (nome.trim.IsEmpty) or (email.trim.IsEmpty) then begin
      raise Exception.Create('Informe o nome e email do usu�rio!');
   end;

   if (emailjacadastrado(email, cod_usuario)) then begin
      raise Exception.Create('O email informado j� est� sendo utilizado por outro usu�rio.');
   end;

   try
     qry := TFDQuery.Create(nil);
     qry.Connection := connection;
     qry.Active := false;
     qry.SQL.Add('update tab_usuario set nome = :nome, email = :email ');
     qry.SQL.Add('where cod_usuario = :cod_usuario ');
     qry.SQL.Add('returning cod_usuario ');

     qry.ParamByName('cod_usuario').Value  := cod_usuario;
     qry.ParamByName('nome').Value   := nome;
     qry.ParamByName('email').Value  := email;

     qry.Active := true;

     result := qry.ToJSONObject;
     {"cod_usuario": 1"}
   finally
     freeAndNil(qry);
   end;
end;

function TDtmGlobal.ExcluirUsuario(cod_usuario: integer): TJsonObject;
var qry: TFDQuery;
begin
   try
     qry := TFDQuery.Create(nil);
     qry.Connection := connection;

     try
       connection.StartTransaction;
       qry.Active := false;
       qry.SQL.Clear;
       qry.SQL.Add('update tab_usuario set ind_excluido = :ind_excluido, ');
       qry.SQL.Add('email = :email, nome = :nome, token_push = :token_push, ');
       qry.SQL.Add('plataforma = :plataforma ');
       qry.SQL.Add('where cod_usuario = :cod_usuario');
       qry.SQL.Add('returning cod_usuario');

       qry.ParamByName('ind_excluido').Value := 'S';
       qry.ParamByName('email').Value := 'usu�rio excluido';
       qry.ParamByName('nome').Value := 'usu�rio excluido';
       qry.ParamByName('token_push').Value := '';
       qry.ParamByName('plataforma').Value := '';
       qry.ParamByName('cod_usuario').Value := cod_usuario;
       qry.Active := true;

       Result := qry.ToJSONObject;

       qry.Active := false;
       qry.SQL.Clear;
       qry.SQL.Add('delete from tab_notificacao where cod_usuario = :cod_usuario');
       qry.ParamByName('cod_usuario').Value := cod_usuario;
       qry.ExecSQL;
       connection.Commit;
     except on ex:exception do
       begin
         connection.Rollback;
         raise Exception.Create(ex.Message);
       end;
     end;

   finally
     FreeAndNil(qry);
   end;
end;

function TdtmGlobal.emailjacadastrado(email: string; cod_usuario: integer): boolean;
var qry : TFDQuery;
begin
   try
     qry := TFDQuery.Create(nil);
     qry.Connection := connection;
     qry.Active := false;
     qry.SQL.Add('select cod_usuario from tab_usuario ');
     qry.SQL.Add('where email = :email  ');

     if (cod_usuario > 0) then begin
        qry.SQL.Add(' and cod_usuario <> :cod_usuario ');
        qry.ParamByName('cod_usuario').Value := cod_usuario;
     end;

     qry.ParamByName('email').Value   := email;
     qry.Active := true;

     result := (qry.RecordCount > 0);
   finally
     freeAndNil(qry);
   end;
end;

function TdtmGlobal.EditarSenha(cod_usuario: integer; senha: string): TJSONObject;
var qry : TFDQuery;
begin
   if (senha.trim.IsEmpty) then begin
      raise Exception.Create('Informe a nova senha do usu�rio!');
   end;

   if (senha.Length < 5) then begin
      raise Exception.Create('A senha deve ter no m�nimo 5 caracteres');
   end;

   try
     qry := TFDQuery.Create(nil);
     qry.Connection := connection;
     qry.Active := false;
     qry.SQL.Add('update tab_usuario set senha = :senha ');
     qry.SQL.Add('where cod_usuario = :cod_usuario ');
     qry.SQL.Add('returning cod_usuario ');

     qry.ParamByName('cod_usuario').Value  := cod_usuario;
     qry.ParamByName('senha').Value   := SaltPassword(senha); //Criptografa a senha;

     qry.Active := true;

     result := qry.ToJSONObject;
     {"cod_usuario": 1"}
   finally
     freeAndNil(qry);
   end;
end;

function TdtmGlobal.ListarNotificacoes(cod_usuario: integer): TJSONArray;
var qry : TFDQuery;
begin
   try
     qry := TFDQuery.Create(nil);
     qry.Connection := connection;
     qry.Active := false;
     qry.SQL.Add('select cod_notificacao, data_notificacao, titulo, texto ');
     qry.SQL.Add('from tab_notificacao ');
     qry.SQL.Add('where cod_usuario = :cod_usuario ');
     qry.SQL.Add('  and ind_lido = :ind_lido ');

     qry.ParamByName('cod_usuario').Value  := cod_usuario;
     qry.ParamByName('ind_lido').Value     := 'N';

     qry.Active := true;

     result := qry.ToJSONArray;

     //Ap�s carregar as notifica��es, elas s�o marcadas como lidas
     qry.Active := false;
     qry.SQL.clear;
     qry.SQL.Add('update tab_notificacao set ind_lido = ''S'' ');
     qry.SQL.Add('where cod_usuario = :cod_usuario ');
     qry.SQL.Add('  and ind_lido = :ind_lido ');

     qry.ParamByName('cod_usuario').Value  := cod_usuario;
     qry.ParamByName('ind_lido').Value     := 'N';

     qry.ExecSQL;

   finally
     freeAndNil(qry);
   end;
end;

function TdtmGlobal.ListarClientes(dt_ult_sincronizacao: string; pagina: integer): TJSONArray;
var qry : TFDQuery;
begin
   if (dt_ult_sincronizacao.IsEmpty) then begin
      raise Exception.Create('Par�metro dt_ult_sincronizacao n�o informado!');
   end;

   try
     qry := TFDQuery.Create(nil);
     qry.Connection := connection;
     qry.Active := false;
     qry.SQL.Add('select first :first skip :skip * ');
     qry.SQL.Add('from tab_cliente ');
     qry.SQL.Add('where data_ult_alteracao > :data_ult_alteracao');
     qry.SQL.Add('order by nome ');

     qry.ParamByName('data_ult_alteracao').Value  := dt_ult_sincronizacao;
     qry.ParamByName('first').Value               := QTD_REG_PAGINA_CLIENTE;
     qry.ParamByName('skip').Value                := (pagina * QTD_REG_PAGINA_CLIENTE) - QTD_REG_PAGINA_CLIENTE;
     qry.Active := true;

     result := qry.ToJSONArray;
   finally
     freeAndNil(qry);
   end;
end;

function TDtmGlobal.InserirEditarCliente(cod_usuario, cod_cliente_local: integer;
                                        cnpj_cpf, nome, fone, email, endereco, numero,
                                        complemento, bairro, cidade, uf, cep: string;
                                        latitude, longitude, limite_disponivel: double;
                                        cod_cliente_oficial: integer;
                                        dt_ult_sincronizacao: string): TJsonObject;
var
    qry: TFDQuery;
begin

    try
        qry := TFDQuery.Create(nil);
        qry.Connection := connection;

        with qry do
        begin
            Active := false;
            SQL.Clear;

            if cod_cliente_oficial = 0 then
            begin
                SQL.Add('insert into tab_cliente(cod_usuario, cnpj_cpf, nome, fone, email,');
                SQL.Add('endereco, numero, complemento, bairro, cidade, uf, ');
                SQL.Add('cep, latitude, longitude, limite_disponivel, data_ult_alteracao)');
                SQL.Add('values(:cod_usuario, :cnpj_cpf, :nome, :fone, :email,');
                SQL.Add(':endereco, :numero, :complemento, :bairro, :cidade, :uf, ');
                SQL.Add(':cep, :latitude, :longitude, :limite_disponivel, :data_ult_alteracao)');
                SQL.Add('returning cod_cliente');
                //SQL.Add('returning cod_cliente as cod_cliente_oficial');  //N�o funcionou com Apelido

                ParamByName('cod_usuario').Value := cod_usuario;
            end
            else
            begin
                SQL.Add('update tab_cliente set cnpj_cpf=:cnpj_cpf, nome=:nome, fone=:fone, ');
                SQL.Add('email=:email, endereco=:endereco, numero=:numero, complemento=:complemento, ');
                SQL.Add('bairro=:bairro, cidade=:cidade, uf=:uf, cep=:cep, latitude=:latitude, ');
                SQL.Add('longitude=:longitude, limite_disponivel=:limite_disponivel, ');
                SQL.Add('data_ult_alteracao=:data_ult_alteracao ');
                SQL.Add('where cod_cliente = :cod_cliente ');
                SQL.Add('returning cod_cliente');
                //SQL.Add('returning cod_cliente as cod_cliente_oficial');  //N�o funcionou com Apelido

                ParamByName('cod_cliente').Value := cod_cliente_oficial;
            end;

            ParamByName('cnpj_cpf').Value := cnpj_cpf;
            ParamByName('nome').Value := nome;
            ParamByName('fone').Value := fone;
            ParamByName('email').Value := email;
            ParamByName('endereco').Value := endereco;
            ParamByName('numero').Value := numero;
            ParamByName('complemento').Value := complemento;
            ParamByName('bairro').Value := bairro;
            ParamByName('cidade').Value := cidade;
            ParamByName('uf').Value :=  uf;
            ParamByName('cep').Value := cep;
            ParamByName('latitude').Value := latitude;
            ParamByName('longitude').Value := longitude;
            ParamByName('limite_disponivel').Value := limite_disponivel;
            ParamByName('data_ult_alteracao').Value := dt_ult_sincronizacao;
//            ExecSQL;
            Active := true;
        end;

        Result := qry.ToJSONObject;

    finally
        FreeAndNil(qry);
    end;
end;

function TDtmGlobal.ListarProdutos(dt_ult_sincronizacao: string; pagina: integer): TJsonArray;
var  qry: TFDQuery;
begin
    if (dt_ult_sincronizacao = '') then begin
        raise Exception.Create('Par�metro dt_ult_sincronizacao n�o informado');
    end;

    try
      qry := TFDQuery.Create(nil);
      qry.Connection := connection;

      qry.Active := false;
      qry.SQL.Clear;
      qry.SQL.Add('select first :first skip :skip cod_produto, descricao,');
      qry.SQL.Add('valor, qtd_estoque, cod_usuario, data_ult_alteracao');
      qry.SQL.Add('from tab_produto');
      qry.SQL.Add('where data_ult_alteracao > :data_ult_alteracao');
      qry.SQL.Add('order by cod_produto');

      qry.ParamByName('data_ult_alteracao').Value := dt_ult_sincronizacao;
      qry.ParamByName('first').Value := QTD_REG_PAGINA_PRODUTO;
      qry.ParamByName('skip').Value := (pagina * QTD_REG_PAGINA_PRODUTO) - QTD_REG_PAGINA_PRODUTO;

      qry.Active := true;
      Result := qry.ToJSONArray;
    finally
      FreeAndNil(qry);
    end;
end;

function TDtmGlobal.InserirEditarProduto(cod_usuario, cod_produto_local: integer; descricao: string; valor, qtd_estoque: double; cod_produto_oficial: integer; dt_ult_sincronizacao: string): TJsonObject;
var qry: TFDQuery;
begin
   try
     qry := TFDQuery.Create(nil);
     qry.Connection := connection;
     qry.Active := false;
     qry.SQL.Clear;

     if cod_produto_oficial = 0 then begin
        qry.SQL.Add('insert into tab_produto(descricao, valor, qtd_estoque, cod_usuario, data_ult_alteracao)');
        qry.SQL.Add('values(:descricao, :valor, :qtd_estoque, :cod_usuario, :data_ult_alteracao)');
        qry.SQL.Add('returning cod_produto ');
        //qry.SQL.Add('returning cod_produto as cod_produto_oficial'); //N�o funciona apelidando o Retorno FB 2.5
        qry.ParamByName('cod_usuario').Value := cod_usuario;
     end else begin
        qry.SQL.Add('update tab_produto set descricao=:descricao, valor=:valor,');
        qry.SQL.Add('qtd_estoque=:qtd_estoque, data_ult_alteracao=:data_ult_alteracao ');
        qry.SQL.Add('where cod_produto = :cod_produto ');
        qry.SQL.Add('returning cod_produto ');
        //qry.SQL.Add('returning cod_produto as cod_produto_oficial'); //N�o funciona apelidando o Retorno FB 2.5

        qry.ParamByName('cod_produto').Value := cod_produto_oficial;
     end;

     qry.ParamByName('descricao').Value := descricao;
     qry.ParamByName('valor').Value := valor;
     qry.ParamByName('qtd_estoque').Value := qtd_estoque;
     qry.ParamByName('data_ult_alteracao').Value := dt_ult_sincronizacao;
     qry.Active := true;

     Result := qry.ToJSONObject;

   finally
     FreeAndNil(qry);
   end;
end;

function TDtmGlobal.ListarFoto(cod_produto: integer): TMemoryStream;
var qry: TFDQuery;
    LStream: TStream;
begin
    if (cod_produto <= 0) then begin
        raise Exception.Create('Par�metro cod_produto n�o informado');
    end;

    try
      qry := TFDQuery.Create(nil);
      qry.Connection := connection;
      qry.Active := false;
      qry.SQL.Clear;
      qry.SQL.Add('select foto');
      qry.SQL.Add('from tab_produto');
      qry.SQL.Add('where cod_produto = :cod_produto');
      qry.ParamByName('cod_produto').Value := cod_produto;
      qry.Active := true;

      if qry.FieldByName('foto').AsString = '' then begin
         raise Exception.Create('O produto n�o possui uma foto cadastrada');
      end;

      //Carrega o Stream para poder retornar no Response da Requisi��o
      LStream := qry.CreateBlobStream(qry.FieldByName('foto'), TBlobStreamMode.bmRead);

      Result := TMemoryStream.Create;
      Result.LoadFromStream(LStream);

      FreeAndNil(LStream);

    finally
      FreeAndNil(qry);
    end;
end;

procedure TDtmGlobal.EditarFoto(cod_produto: integer; foto: TBitmap);
var qry: TFDQuery;
    LStream: TStream;
begin
    if (cod_produto <= 0) then begin
        raise Exception.Create('Par�metro cod_produto n�o informado');
    end;

    if (foto = nil) then begin
        raise Exception.Create('Par�metro foto n�o informado');
    end;

    try
      qry := TFDQuery.Create(nil);
      qry.Connection := connection;
      qry.Active := false;
      qry.SQL.Clear;
      qry.SQL.Add('update tab_produto set foto = :foto');
      qry.SQL.Add('where cod_produto = :cod_produto');

      qry.ParamByName('foto').Assign(foto);
      qry.ParamByName('cod_produto').Value := cod_produto;

      qry.ExecSQL;
    finally
      FreeAndNil(qry);
    end;
end;

function TDtmGlobal.ListarPedidos(dt_ult_sincronizacao: string; cod_usuario, pagina: integer): TJsonArray;
var qry: TFDQuery;
    pedidos: TJsonArray;
    i: integer;
    cod_pedido: integer;
begin
   if (dt_ult_sincronizacao = '') then
      raise Exception.Create('Par�metro dt_ult_sincronizacao n�o informado');

   try
     qry := TFDQuery.Create(nil);
     qry.Connection := connection;
     qry.Active := false;
     qry.SQL.Clear;
     qry.SQL.Add('select first :first skip :skip *');
     qry.SQL.Add('from tab_pedido');
     qry.SQL.Add('where data_ult_alteracao > :data_ult_alteracao');
     qry.SQL.Add('and cod_usuario = :cod_usuario');
     qry.SQL.Add('order by cod_pedido');

     qry.ParamByName('data_ult_alteracao').Value := dt_ult_sincronizacao;
     qry.ParamByName('cod_usuario').Value := cod_usuario;
     qry.ParamByName('first').Value := QTD_REG_PAGINA_PEDIDO;
     qry.ParamByName('skip').Value  := (pagina * QTD_REG_PAGINA_PEDIDO) - QTD_REG_PAGINA_PEDIDO;

     qry.Active := true;

     pedidos := qry.ToJSONArray;
     for i := 0 to pedidos.Size - 1 do begin
        //Captura o C�digo do pedido para consultar os itens
        cod_pedido := pedidos[i].GetValue<integer>('cod_pedido', 0);

        //Realiza a consulta dos itens vinculados ao Pedido e joga dentro do JSON
        (pedidos[i] as TJsonObject).AddPair('itens', ListarItensPedido(cod_pedido, qry));
     end;

     Result := pedidos;

   finally
     FreeAndNil(qry);
   end;
end;

function TDtmGlobal.ListarItensPedido(cod_pedido: integer; qry: TFDQuery): TJsonArray;
begin
   qry.Active := false;
   qry.SQL.Clear;
   qry.SQL.Add('select cod_item, cod_produto, qtd, valor_unitario, valor_total');
   qry.SQL.Add('from tab_pedido_item');
   qry.SQL.Add('where cod_pedido = :cod_pedido');
   qry.SQL.Add('order by cod_item');

   qry.ParamByName('cod_pedido').Value := cod_pedido;

   qry.Active := true;
   Result := qry.ToJSONArray;
end;

function TDtmGlobal.InserirEditarPedido(cod_usuario, cod_pedido_local, cod_cliente: integer; tipo_pedido, data_pedido, contato, obs: string;
                                        valor_total: double; cod_cond_pagto: integer; prazo_entrega, data_entrega: string; cod_pedido_oficial: integer;
                                        dt_ult_sincronizacao: string; itens: TJsonArray): TJsonObject;
var qry: TFDQuery;
    i  : integer;
begin
   try
     qry := TFDQuery.Create(nil);
     qry.Connection := connection;

     try
       connection.StartTransaction;
       qry.Active := false;
       qry.SQL.Clear;

       if cod_pedido_oficial = 0 then begin
          qry.SQL.Add('insert into tab_pedido(cod_cliente, cod_usuario, tipo_pedido, data_pedido, contato, obs,');
          qry.SQL.Add('valor_total, cod_cond_pagto, prazo_entrega, data_entrega, cod_pedido_local, data_ult_alteracao)');
          qry.SQL.Add('values(:cod_cliente, :cod_usuario, :tipo_pedido, :data_pedido, :contato, :obs,');
          qry.SQL.Add(':valor_total, :cod_cond_pagto, :prazo_entrega, :data_entrega, :cod_pedido_local, :data_ult_alteracao)');
          qry.SQL.Add('returning cod_pedido');

          qry.ParamByName('cod_usuario').Value := cod_usuario;
          qry.ParamByName('cod_pedido_local').Value := cod_pedido_local;
       end else begin
          qry.SQL.Add('update tab_pedido set cod_cliente=:cod_cliente, tipo_pedido=:tipo_pedido, ');
          qry.SQL.Add('data_pedido=:data_pedido, contato=:contato, obs=:obs, valor_total=:valor_total,');
          qry.SQL.Add('cod_cond_pagto=:cod_cond_pagto, prazo_entrega=:prazo_entrega, ');
          qry.SQL.Add('data_entrega=:data_entrega, data_ult_alteracao=:data_ult_alteracao');
          qry.SQL.Add('where cod_pedido = :cod_pedido');
          qry.SQL.Add('returning cod_pedido');

          qry.ParamByName('cod_pedido').Value := cod_pedido_oficial;
       end;

       qry.ParamByName('cod_cliente').Value := cod_cliente;
       qry.ParamByName('tipo_pedido').Value := tipo_pedido;
       qry.ParamByName('data_pedido').Value := data_pedido;
       qry.ParamByName('contato').Value := contato;
       qry.ParamByName('obs').Value := obs;
       qry.ParamByName('valor_total').Value := valor_total;
       qry.ParamByName('cod_cond_pagto').Value := cod_cond_pagto;
       qry.ParamByName('prazo_entrega').Value := prazo_entrega;

       if data_entrega <> '' then begin
          qry.ParamByName('data_entrega').Value := data_entrega
       end else begin
          qry.ParamByName('data_entrega').DataType := ftString;
          qry.ParamByName('data_entrega').Value := Unassigned;
       end;

       qry.ParamByName('data_ult_alteracao').Value := dt_ult_sincronizacao;
       qry.Active := true;

       Result := qry.ToJSONObject;
       cod_pedido_oficial := qry.FieldByName('cod_pedido').AsInteger;


       // Itens do pedido -----------------------------------------------------
       qry.Active := false;
       qry.SQL.Clear;
       qry.SQL.Add('delete from tab_pedido_item where cod_pedido = :cod_pedido');
       qry.ParamByName('cod_pedido').Value := cod_pedido_oficial;
       qry.ExecSQL;

       for i := 0 to itens.Size - 1 do begin
          qry.Active := false;
          qry.SQL.Clear;
          qry.SQL.Add('insert into tab_pedido_item(cod_pedido, cod_produto, qtd, valor_unitario, valor_total)');
          qry.SQL.Add('values(:cod_pedido, :cod_produto, :qtd, :valor_unitario, :valor_total)');
          qry.ParamByName('cod_pedido').Value := cod_pedido_oficial;
          qry.ParamByName('cod_produto').Value := itens[i].GetValue<integer>('cod_produto', 0);
          qry.ParamByName('qtd').Value := itens[i].GetValue<double>('qtd', 0);
          qry.ParamByName('valor_unitario').Value := itens[i].GetValue<double>('valor_unitario', 0);
          qry.ParamByName('valor_total').Value := itens[i].GetValue<double>('valor_total', 0);
          qry.ExecSQL;
       end;
       connection.Commit;

     except on ex:exception do
       begin
         connection.Rollback;
         raise Exception.Create(ex.Message);
       end;
     end;

   finally
      FreeAndNil(qry);
   end;
end;

function TDtmGlobal.ListarCondPagto: TJsonArray;
var  qry: TFDQuery;
begin
    try
      qry := TFDQuery.Create(nil);
      qry.Connection := connection;

      qry.Active := false;
      qry.SQL.Clear;
      qry.SQL.Add('select cod_cond_pagto, cond_pagto ');
      qry.SQL.Add('from tab_cond_pagto    ');
      qry.SQL.Add('where ind_excluido = ''N'' ');
      qry.SQL.Add('order by cod_cond_pagto');

      qry.Active := true;
      Result := qry.ToJSONArray;
    finally
      FreeAndNil(qry);
    end;
end;


end.
