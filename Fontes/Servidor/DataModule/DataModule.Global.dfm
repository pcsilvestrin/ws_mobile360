object dtmGlobal: TdtmGlobal
  OnCreate = DataModuleCreate
  Height = 480
  Width = 640
  object connection: TFDConnection
    Params.Strings = (
      'DriverID=FB'
      'User_Name=sysdba'
      'Password=masterkey')
    LoginPrompt = False
    BeforeConnect = connectionBeforeConnect
    Left = 72
    Top = 24
  end
  object FDPhysFBDriverLink: TFDPhysFBDriverLink
    Left = 200
    Top = 24
  end
end
