program ServidorHorse;

uses
  System.StartUpCopy,
  FMX.Forms,
  UntPrincipal in 'UntPrincipal.pas' {frmPrincipal},
  Controllers.Usuario in 'controller\Controllers.Usuario.pas',
  DataModule.Global in 'DataModule\DataModule.Global.pas' {dtmGlobal: TDataModule},
  uMD5 in 'units\uMD5.pas',
  Controllers.Auth in 'controller\Controllers.Auth.pas',
  Controllers.Notificacao in 'controller\Controllers.Notificacao.pas',
  Controllers.Cliente in 'controller\Controllers.Cliente.pas',
  Controllers.Produto in 'controller\Controllers.Produto.pas',
  Controllers.Pedido in 'controller\Controllers.Pedido.pas',
  Controllers.CondPagto in 'controller\Controllers.CondPagto.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TfrmPrincipal, frmPrincipal);
  Application.Run;
end.
