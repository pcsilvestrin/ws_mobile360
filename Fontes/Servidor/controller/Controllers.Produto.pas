unit Controllers.Produto;

interface

uses Horse,
     Horse.Jhonson,
     Horse.CORS,
     Horse.Upload,
     DataModule.Global,
     System.JSON,
     Controllers.Auth,
     Horse.JWT;

procedure RegistrarRotas;
procedure ListarProdutos(Req: THorseRequest; Res: THorseResponse);
procedure InserirEditarProduto(Req: THorseRequest; Res: THorseResponse; Next: TProc);
procedure ListarFoto(Req: THorseRequest; Res: THorseResponse; Next: TProc);
procedure EditarFoto(Req: THorseRequest; Res: THorseResponse; Next: TProc);

implementation

uses
  System.SysUtils, System.Classes, FMX.Graphics;

procedure RegistrarRotas;
begin
   THorse.AddCallback(
         HorseJWT(
           Controllers.Auth.SECRET,
           THorseJWTConfig.New.SessionClass(TMyClaims)
         )).Get('/produtos/sincronizacao', ListarProdutos);

   THorse.AddCallback(
         HorseJWT(
           Controllers.Auth.SECRET,
           THorseJWTConfig.New.SessionClass(TMyClaims)
         )).Post('/produtos/sincronizacao', InserirEditarProduto);

    THorse.AddCallback(
         HorseJWT(
           Controllers.Auth.SECRET,
           THorseJWTConfig.New.SessionClass(TMyClaims)
         )).Get('/produtos/foto/:cod_produto', ListarFoto);

   THorse.AddCallback(
         HorseJWT(
            Controllers.Auth.SECRET,
            THorseJWTConfig.New.SessionClass(TMyClaims)
         )).Put('/produtos/foto/:cod_produto', EditarFoto);
end;

procedure ListarProdutos(Req: THorseRequest; Res: THorseResponse);
var dmGlobal    : TDtmGlobal;
    dt_ult_sincronizacao: string;
    pagina      : integer;
    response    : TJSONObject;
begin
   try
     try
        dmGlobal := TDtmGlobal.Create(nil);

        try
          //Par�metro passado na rota
          dt_ult_sincronizacao := Req.Query['dt_ult_sincronizacao']; //yyyy-mm-dd hh:mm:ss
        except
          dt_ult_sincronizacao := '';
        end;

        try
          //Par�metro passado na rota
          pagina := Req.Query['pagina'].ToInteger;
        except
          pagina := 1;
        end;

        Res.Send<TJSONArray>(dmGlobal.ListarProdutos(dt_ult_sincronizacao, pagina)).Status(200);
     except on e: Exception do
       begin
         Res.Send(e.message).Status(500);
       end;
     end;
   finally
     freeAndNil(dmGlobal);
   end;
end;

procedure InserirEditarProduto(Req: THorseRequest; Res: THorseResponse; Next: TProc);
var
    DmGlobal: TDtmGlobal;
    cod_usuario: integer;
    body, json_ret: TJsonObject;
begin
    try
        try
            DmGlobal := TDtmGlobal.Create(Nil);

            cod_usuario := Get_Usuario_Request(Req);
            body := Req.Body<TJsonObject>;


            json_ret := DmGlobal.InserirEditarProduto(cod_usuario,
                                    body.GetValue<integer>('cod_produto_local', 0),
                                    body.GetValue<string>('descricao', ''),
                                    body.GetValue<double>('valor', 0),
                                    body.GetValue<double>('qtd_estoque', 0),
                                    body.GetValue<integer>('cod_produto_oficial', 0),
                                    body.GetValue<string>('dt_ult_sincronizacao', '')
                                    );

            json_ret.AddPair('cod_produto_local', TJsonNumber.Create(body.GetValue<integer>('cod_produto_local', 0)));

            {"cod_produto_local": 123, "cod_produto_oficial": 5654}
            Res.Send<TJsonObject>(json_ret).Status(200);

        except on ex:exception do
            Res.Send(ex.Message).Status(500);
        end;
    finally
        FreeAndNil(DmGlobal);
    end;
end;

procedure ListarFoto(Req: THorseRequest; Res: THorseResponse; Next: TProc);
var dmGlobal: TDtmGlobal;
    cod_produto: integer;
begin
   try
     try
       dmGlobal := TDtmGlobal.Create(Nil);

       //--> /produtos/foto/123
       try
         cod_produto := Req.Params.Items['cod_produto'].ToInteger;
       except
         cod_produto := 0;
       end;
       Res.Send<TStream>(dmGlobal.ListarFoto(cod_produto)).Status(200);
     except on ex:exception do
       Res.Send(ex.Message).Status(500);
     end;
    finally
        FreeAndNil(DmGlobal);
    end;
end;

procedure EditarFoto(Req: THorseRequest; Res: THorseResponse; Next: TProc);
var LUploadConfig: TUploadConfig;  //** uses: Horse.Upload
    cod_produto: integer;
    foto: TBitmap;
    DmGlobal: TDtmGlobal;
begin
   //--> /produtos/foto/123
   try
     cod_produto := Req.Params.Items['cod_produto'].ToInteger;
   except
     cod_produto := 0;
   end;

   LUploadConfig := TUploadConfig.Create(ExtractFilePath(ParamStr(0)) + 'Fotos');
   LUploadConfig.ForceDir      := True; //Se n�o existe o caminho, o componente ir� criar
   LUploadConfig.OverrideFiles := True; //Se existir o arquivo no caminho, o arquivo ser� atualizado

   LUploadConfig.UploadFileCallBack :=
    procedure(Sender: TObject; AFile: TUploadFileInfo)
    begin
      try
        DmGlobal := TDtmGlobal.Create(nil);
        foto := TBitmap.CreateFromFile(AFile.fullpath);
        dmGlobal.EditarFoto(cod_produto, foto);
        FreeAndNil(foto);
      finally
        FreeAndNil(DmGlobal);
      end;
    end;
    Res.Send<TUploadConfig>(LUploadConfig);
end;

end.
