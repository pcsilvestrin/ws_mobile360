unit Controllers.CondPagto;

interface

uses Horse,
     Horse.Jhonson,
     Horse.CORS,
     DataModule.Global,
     System.JSON,
     Controllers.Auth,
     Horse.JWT;

procedure RegistrarRotas;
procedure ListarCondPagto(Req: THorseRequest; Res: THorseResponse);

implementation

uses
  System.SysUtils, System.Classes, FMX.Graphics;

procedure RegistrarRotas;
begin
   THorse.AddCallback(
         HorseJWT(
           Controllers.Auth.SECRET,
           THorseJWTConfig.New.SessionClass(TMyClaims)
         )).Get('/cond-pagto', ListarCondPagto);

end;

procedure ListarCondPagto(Req: THorseRequest; Res: THorseResponse);
var dmGlobal    : TDtmGlobal;
begin
   try
     try
        dmGlobal := TDtmGlobal.Create(nil);
        Res.Send<TJSONArray>(dmGlobal.ListarCondPagto).Status(200);
     except on e: Exception do
       begin
         Res.Send(e.message).Status(500);
       end;
     end;
   finally
     freeAndNil(dmGlobal);
   end;
end;

end.
