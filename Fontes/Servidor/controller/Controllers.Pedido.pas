unit Controllers.Pedido;

interface

uses Horse,
     Horse.Jhonson,
     Horse.CORS,
     DataModule.Global,
     System.JSON,
     Controllers.Auth,
     Horse.JWT;

procedure RegistrarRotas;
procedure ListarPedidos(Req: THorseRequest; Res: THorseResponse);
procedure InserirEditarPedido(Req: THorseRequest; Res: THorseResponse; Next: TProc);

implementation

uses
  System.SysUtils, System.Classes, FMX.Graphics;

procedure RegistrarRotas;
begin
   THorse.AddCallback(
         HorseJWT(
           Controllers.Auth.SECRET,
           THorseJWTConfig.New.SessionClass(TMyClaims)
         )).Get('/pedidos/sincronizacao', ListarPedidos);

   THorse.AddCallback(
         HorseJWT(
           Controllers.Auth.SECRET,
           THorseJWTConfig.New.SessionClass(TMyClaims)
         )).Post('/pedidos/sincronizacao', InserirEditarPedido);
end;

procedure ListarPedidos(Req: THorseRequest; Res: THorseResponse);
var dmGlobal    : TDtmGlobal;
    dt_ult_sincronizacao: string;
    pagina      : integer;
    response    : TJSONObject;
    cod_usuario : integer;
begin
   try
     try
        dmGlobal := TDtmGlobal.Create(nil);

        //S� consulta os pedidos feitos pelo Usu�rio
        cod_usuario := Get_Usuario_Request(Req);

        try
          //Par�metro passado na rota
          dt_ult_sincronizacao := Req.Query['dt_ult_sincronizacao']; //yyyy-mm-dd hh:mm:ss
        except
          dt_ult_sincronizacao := '';
        end;

        try
          //Par�metro passado na rota
          pagina := Req.Query['pagina'].ToInteger;
        except
          pagina := 1;
        end;

        Res.Send<TJSONArray>(dmGlobal.ListarPedidos(dt_ult_sincronizacao, cod_usuario, pagina)).Status(200);
     except on e: Exception do
       begin
         Res.Send(e.message).Status(500);
       end;
     end;
   finally
     freeAndNil(dmGlobal);
   end;
end;

procedure InserirEditarPedido(Req: THorseRequest; Res: THorseResponse; Next: TProc);
var dmGlobal: TDtmGlobal;
    cod_usuario: integer;
    body, json_ret: TJsonObject;
    itens: TJsonArray;
begin
   try
     try
       dmGlobal := TDtmGlobal.Create(Nil);
       cod_usuario := Get_Usuario_Request(Req);
       body := Req.Body<TJsonObject>;

       itens := body.GetValue<TJsonArray>('itens');
       json_ret := dmGlobal.InserirEditarPedido(cod_usuario,
                              body.GetValue<integer>('cod_pedido_local', 0),
                              body.GetValue<integer>('cod_cliente', 0),
                              body.GetValue<string>('tipo_pedido', ''),
                              body.GetValue<string>('data_pedido', ''),
                              body.GetValue<string>('contato', ''),
                              body.GetValue<string>('obs', ''),
                              body.GetValue<double>('valor_total', 0),
                              body.GetValue<integer>('cod_cond_pagto', 0),
                              body.GetValue<string>('prazo_entrega', ''),
                              body.GetValue<string>('data_entrega', ''),
                              body.GetValue<integer>('cod_pedido_oficial', 0),
                              body.GetValue<string>('dt_ult_sincronizacao', ''),
                              itens
                           );

       json_ret.AddPair('cod_pedido_local', TJsonNumber.Create(body.GetValue<integer>('cod_pedido_local', 0)));

       {"cod_pedido_local": 123, "cod_pedido_oficial": 5654}
       Res.Send<TJsonObject>(json_ret).Status(200);

     except on ex:exception do
        Res.Send(ex.Message).Status(500);
     end;
   finally
     FreeAndNil(DmGlobal);
   end;
end;

end.
