unit Controllers.Usuario;

interface

uses Horse,
     Horse.Jhonson,
     Horse.CORS,
     DataModule.Global,
     System.JSON,
     Controllers.Auth,
     Horse.JWT;

procedure RegistrarRotas;
procedure InserirUsuarios(Req: THorseRequest; Res: THorseResponse);
procedure Login(Req: THorseRequest; Res: THorseResponse);
procedure Push(Req: THorseRequest; Res: THorseResponse);
procedure EditarUsuario(Req: THorseRequest; Res: THorseResponse);
procedure ExcluirUsuario(Req: THorseRequest; Res: THorseResponse);
procedure EditarSenha(Req: THorseRequest; Res: THorseResponse);
procedure ObterDataServidor(Req: THorseRequest; Res: THorseResponse);

implementation

uses
  System.SysUtils;

procedure RegistrarRotas;
begin
   THorse.post('/usuarios', InserirUsuarios);
   THorse.post('/usuarios/login', Login);

   THorse.AddCallback(
         HorseJWT(
           Controllers.Auth.SECRET,
           THorseJWTConfig.New.SessionClass(TMyClaims)
         )).post('/usuarios/push', Push); //Declarando rota protegida

   THorse.AddCallback(
         HorseJWT(
           Controllers.Auth.SECRET,
           THorseJWTConfig.New.SessionClass(TMyClaims)
         )).put('/usuarios', EditarUsuario); //Declarando rota protegida

   THorse.AddCallback(
         HorseJWT(
           Controllers.Auth.SECRET,
           THorseJWTConfig.New.SessionClass(TMyClaims)
         )).Delete('/usuarios/:cod_usuario', ExcluirUsuario); //Declarando rota protegida

   THorse.AddCallback(
         HorseJWT(
           Controllers.Auth.SECRET,
           THorseJWTConfig.New.SessionClass(TMyClaims)
         )).put('/usuarios/senha', EditarSenha);

   THorse.AddCallback(
         HorseJWT(
           Controllers.Auth.SECRET,
           THorseJWTConfig.New.SessionClass(TMyClaims)
         )).Get('/usuarios/horario', ObterDataServidor);
end;

procedure InserirUsuarios(Req: THorseRequest; Res: THorseResponse);
var dmGlobal : TDtmGlobal;
    nome, email, senha : string;
    body, response     : TJSONObject;
begin
   try
     try
        dmGlobal := TDtmGlobal.Create(nil);

        body := Req.Body<TJSONObject>; //Pega o objeto recebido por parametro
        nome := body.GetValue<string>('nome', ''); //captura o valor do campo 'nome' do Body, se n�o encontrar, passa vazio
        email:= body.GetValue<string>('email', ''); //captura o valor do campo 'email' do Body, se n�o encontrar, passa vazio
        senha:= body.GetValue<string>('senha', ''); //captura o valor do campo 'senha' do Body, se n�o encontrar, passa vazio

        response := dmGlobal.InserirUsuario(nome, email, senha);
        response.AddPair('nome', nome);
        response.AddPair('email', email);

        response.AddPair('token', Criar_Token(response.GetValue<integer>('cod_usuario', 0))); //Gera o Token JWT com o c�digo do usu�rio carregado no Response
        Res.Send<TJSONObject>(response).Status(201);

     except on e: Exception do
       begin
         Res.Send(e.message).Status(500);
       end;
     end;
   finally
     freeAndNil(dmGlobal);
   end;
end;

procedure Login(Req: THorseRequest; Res: THorseResponse);
var dmGlobal : TDtmGlobal;
    email, senha : string;
    body, response : TJSONObject;
begin
   try
     try
        dmGlobal := TDtmGlobal.Create(nil);

        body := Req.Body<TJSONObject>; //Pega o objeto recebido por parametro
        email:= body.GetValue<string>('email', ''); //captura o valor do campo 'email' do Body, se n�o encontrar, passa vazio
        senha:= body.GetValue<string>('senha', ''); //captura o valor do campo 'senha' do Body, se n�o encontrar, passa vazio

        response := dmGlobal.login(email, senha);
        if (response.Size = 0) then begin
           Res.Send('E-Mail ou Senha s�o inv�lidos').Status(401);
        end else begin
           response.AddPair('token', Criar_Token(response.GetValue<integer>('cod_usuario', 0))); //Gera o Token JWT com o c�digo do usu�rio carregado no Response
           Res.Send<TJSONObject>(response).Status(200);
        end;

     except on e: Exception do
       begin
         Res.Send(e.message).Status(500);
       end;
     end;
   finally
     freeAndNil(dmGlobal);
   end;
end;

procedure Push(Req: THorseRequest; Res: THorseResponse);
var dmGlobal : TDtmGlobal;
    token_push : string;
    cod_usuario: integer;
    body, response : TJSONObject;
begin
   try
     try
        dmGlobal := TDtmGlobal.Create(nil);

        //Captura o C�digo do usuario que est� no Token enviado na requisi��o
        cod_usuario := Get_Usuario_Request(req);

        body := Req.Body<TJSONObject>;
        token_push:= body.GetValue<string>('token_push', '');

        response := dmGlobal.push(cod_usuario, token_push);
        Res.Send<TJSONObject>(response).Status(200);

     except on e: Exception do
       begin
         Res.Send(e.message).Status(500);
       end;
     end;
   finally
     freeAndNil(dmGlobal);
   end;
end;

procedure EditarUsuario(Req: THorseRequest; Res: THorseResponse);
var dmGlobal    : TDtmGlobal;
    nome, email : string;
    cod_usuario : integer;
    body, response : TJSONObject;
begin
   try
     try
        dmGlobal := TDtmGlobal.Create(nil);

        //Captura o C�digo do usuario que est� no Token enviado na requisi��o
        cod_usuario := Get_Usuario_Request(req);

        body := Req.Body<TJSONObject>;
        nome := body.GetValue<string>('nome', '');
        email:= body.GetValue<string>('email', '');

        response := dmGlobal.EditarUsuario(cod_usuario, nome, email);
        Res.Send<TJSONObject>(response).Status(200);

     except on e: Exception do
       begin
         Res.Send(e.message).Status(500);
       end;
     end;
   finally
     freeAndNil(dmGlobal);
   end;
end;

procedure EditarSenha(Req: THorseRequest; Res: THorseResponse);
var dmGlobal    : TDtmGlobal;
    nova_senha  : string;
    cod_usuario : integer;
    body, response : TJSONObject;
begin
   try
     try
        dmGlobal := TDtmGlobal.Create(nil);

        //Captura o C�digo do usuario que est� no Token enviado na requisi��o
        cod_usuario := Get_Usuario_Request(req);

        body := Req.Body<TJSONObject>;
        nova_senha := body.GetValue<string>('senha', '');

        response := dmGlobal.EditarSenha(cod_usuario, nova_senha);
        Res.Send<TJSONObject>(response).Status(200);

     except on e: Exception do
       begin
         Res.Send(e.message).Status(500);
       end;
     end;
   finally
     freeAndNil(dmGlobal);
   end;
end;

procedure ExcluirUsuario(Req: THorseRequest; Res: THorseResponse);
var DmGlobal: TDtmGlobal;
    cod_usuario, cod_usuario_param: integer;
    json_ret: TJsonObject;
begin
    try
        try
            DmGlobal := TDtmGlobal.Create(Nil);

            cod_usuario := Get_Usuario_Request(Req);

            // localhost:9000/usuarios/2

            try
                cod_usuario_param := Req.Params.Items['cod_usuario'].ToInteger;
            except
                cod_usuario_param := 0;
            end;

            //S� exclui se o usu�rio passado por parametro � o mesmo autenticado
            if cod_usuario <> cod_usuario_param then
                raise Exception.Create('Opera��o n�o permitida');

            json_ret := DmGlobal.ExcluirUsuario(cod_usuario);

            Res.Send<TJsonObject>(json_ret).Status(200);

        except on ex:exception do
            Res.Send(ex.Message).Status(500);
        end;
    finally
        FreeAndNil(DmGlobal);
    end;
end;

procedure ObterDataServidor(Req: THorseRequest; Res: THorseResponse);
begin
   Res.Send(FormatDateTime('yyyy-mm-dd hh:nn:ss', now)).Status(200);
end;

end.
