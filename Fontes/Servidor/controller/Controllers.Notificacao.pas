unit Controllers.Notificacao;

interface

uses Horse,
     Horse.Jhonson,
     Horse.CORS,
     DataModule.Global,
     System.JSON,
     Controllers.Auth,
     Horse.JWT;

procedure RegistrarRotas;
procedure ListarNotificacoes(Req: THorseRequest; Res: THorseResponse);

implementation

uses
  System.SysUtils;

procedure RegistrarRotas;
begin
   THorse.AddCallback(
         HorseJWT(
           Controllers.Auth.SECRET,
           THorseJWTConfig.New.SessionClass(TMyClaims)
         )).Get('/notificacoes', ListarNotificacoes);
end;

procedure ListarNotificacoes(Req: THorseRequest; Res: THorseResponse);
var dmGlobal    : TDtmGlobal;
    cod_usuario : integer;
    response    : TJSONObject;
begin
   try
     try
        dmGlobal := TDtmGlobal.Create(nil);

        cod_usuario := Get_Usuario_Request(req);
        Res.Send<TJSONArray>(dmGlobal.ListarNotificacoes(cod_usuario)).Status(200);
     except on e: Exception do
       begin
         Res.Send(e.message).Status(500);
       end;
     end;
   finally
     freeAndNil(dmGlobal);
   end;
end;

end.
