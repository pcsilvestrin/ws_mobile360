unit UntProduto;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.StdCtrls,
  FMX.Controls.Presentation, FMX.Edit, FMX.Objects, FMX.ListView.Types,
  FMX.ListView.Appearances, FMX.ListView.Adapters.Base, FMX.ListView, uFancyDialog;

type
  TFrmProduto = class(TForm)
    rectToolbarProdutos: TRectangle;
    Label3: TLabel;
    btnAddProduto: TSpeedButton;
    Image4: TImage;
    rectBusca: TRectangle;
    edtBuscarProdutos: TEdit;
    btnBuscarProdutos: TSpeedButton;
    btnVoltar: TSpeedButton;
    Image1: TImage;
    lvProduto: TListView;
    imgEstoque: TImage;
    imgMoney: TImage;
    imgSemFoto: TImage;
    imgSemProdutos: TImage;
    imgIcoSincronizar: TImage;
    procedure FormShow(Sender: TObject);
    procedure btnBuscarProdutosClick(Sender: TObject);
    procedure lvProdutoPaint(Sender: TObject; Canvas: TCanvas;
      const ARect: TRectF);
    procedure lvProdutoUpdateObjects(const Sender: TObject;
      const AItem: TListViewItem);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnAddProdutoClick(Sender: TObject);
    procedure btnVoltarClick(Sender: TObject);
    procedure lvProdutoItemClick(const Sender: TObject;
      const AItem: TListViewItem);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    Fancy : TFancyDialog;
    procedure addProdutoListView(cod_produto_local, descricao, ind_sincronizar: string; valor, estoque: double; foto: TStream);
    procedure ListarProdutos(pagina: integer; busca: string;
      ind_clear: boolean);
    procedure ThreadProdutosTerminate(Sender: TObject);
    procedure LayoutListViewProdutos(AItem: TListViewItem);
    procedure RefreshListagem;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmProduto: TFrmProduto;

implementation

{$R *.fmx}

uses UntPrincipal, DataModule.Produto, Data.DB, uFunctions, UntProdutoCad,
  uConstantes;

procedure TFrmProduto.LayoutListViewProdutos(AItem: TListViewItem);
var txt : TListItemText;
    img : TListItemImage;
    posicao_y: Extended;
begin
   txt       := TListItemText(AItem.Objects.FindDrawable('txtDescricao'));
   txt.Width := lvProduto.Width - txt.PlaceOffset.X - txt.PlaceOffset.Y; //X e Y S�o as bordas do Componente
   txt.Height:= GetTextHeight(txt, txt.width, txt.text) + 3; // 3 - Valor de seguran�a para n�o cortar o Texto do Componente

   // Define o espa�amento dos elementos que est�o abaixo da Descri��o
   posicao_y := txt.PlaceOffSet.Y + txt.Height;

   TListItemText(AItem.Objects.FindDrawable('txtValor')).PlaceOffSet.Y    := posicao_y;
   TListItemText(AItem.Objects.FindDrawable('txtEstoque')).PlaceOffSet.Y  := posicao_y;
   TListItemImage(AItem.Objects.FindDrawable('imgValor')).PlaceOffSet.Y   := posicao_y;
   TListItemImage(AItem.Objects.FindDrawable('imgEstoque')).PlaceOffSet.Y := posicao_y;
   TListItemImage(AItem.Objects.FindDrawable('imgSincronizar')).PlaceOffSet.Y := posicao_y;

   AItem.Height := Trunc(posicao_y + 30);
end;

procedure TFrmProduto.addProdutoListView(cod_produto_local, descricao, ind_sincronizar: string; valor, estoque: double; foto: TStream);
var item: TListViewItem;
    txt : tListItemText;
    img : TListItemImage;
    bmp : TBitMap;
begin
   try
     item := lvProduto.Items.Add;
     item.height := 75;
     item.TagString := cod_produto_local;

     // Descricao
     txt := tListItemText(item.Objects.FindDrawable('txtDescricao'));
     txt.Text := descricao;

     // Valor
     txt := tListItemText(item.Objects.FindDrawable('txtValor'));
     txt.Text := 'R$ '+ FormatFloat('###,###,##0.00', valor);

     // Estoque
     txt := tListItemText(item.Objects.FindDrawable('txtEstoque'));
     txt.Text := FormatFloat('###,####0.0000', estoque);

     // Icone do Valor
     img := TListItemImage(item.Objects.FindDrawable('imgValor'));
     img.Bitmap := imgMoney.Bitmap;

     // Icone do Estoque
     img := TListItemImage(item.Objects.FindDrawable('imgEstoque'));
     img.Bitmap := imgEstoque.Bitmap;

     // Icone para Sincronizar
     if ind_sincronizar = 'S' then begin
        img := TListItemImage(item.Objects.FindDrawable('imgSincronizar'));
        img.Bitmap := imgIcoSincronizar.Bitmap;
     end;


     // Imagem do Produto
     img := TListItemImage(item.Objects.FindDrawable('imgFoto'));
     if (foto <> nil) then begin
        bmp := TBitmap.Create;
        bmp.LoadFromStream(foto);

        //Destr�i o objeto automaticamente
        img.OwnsBitmap := true;

        img.Bitmap := bmp;
     end else begin
        img.Bitmap := imgSemFoto.Bitmap;
     end;

     LayoutListViewProdutos(item);

   except on ex: exception do
     begin
       fancy.Show(TIconDialog.Error, 'Aviso', 'Erro ao inserir produto na lista: '+ex.Message, 'OK');
     end;
   end;
end;

procedure TFrmProduto.btnAddProdutoClick(Sender: TObject);
begin
   if not Assigned(FrmProdutoCad) then begin
      Application.CreateForm(TFrmProdutoCad, FrmProdutoCad);
   end;

   FrmProdutoCad.Modo := 'I';
   FrmProdutoCad.Cod_Produto := 0;
   FrmProdutoCad.ExecuteOnClose := RefreshListagem;
   FrmProdutoCad.show;
end;

procedure TFrmProduto.btnBuscarProdutosClick(Sender: TObject);
begin
   ListarProdutos(1, edtBuscarProdutos.Text, true);
end; procedure TFrmProduto.btnVoltarClick(Sender: TObject);
begin
   Close;
end;

procedure TFrmProduto.FormClose(Sender: TObject; var Action: TCloseAction);
begin
   Action := TCloseAction.caFree;
   FrmProduto := nil;
end;

procedure TFrmProduto.FormCreate(Sender: TObject);
begin
   Fancy := TFancyDialog.Create(FrmProduto);
end;

procedure TFrmProduto.FormDestroy(Sender: TObject);
begin
   Fancy.DisposeOf;
end;

procedure TFrmProduto.FormShow(Sender: TObject);
begin
   ListarProdutos(1, '', true);
end;

procedure TFrmProduto.ListarProdutos(pagina: integer; busca: string; ind_clear: boolean);
var t : TThread;
begin
   imgSemProdutos.visible := false;

   //Evitar processamento concorrente ...
   if lvProduto.TagString = 'S' then exit;

   //Em processamento
   lvProduto.TagString := 'S';

   lvProduto.BeginUpdate;
   if ind_clear then begin
      pagina := 1;
      lvProduto.ScrollTo(0); //Volta para o primeiro item da lista
      lvProduto.Items.Clear;
   end;

   {
    Tag: Cont�m a pagina atual solicitada ao servidor ...
    >= 1 : Faz o request para buscar mais dados
     - 1 : indica que n�o tem mais dados
   }
   //Salva a pagina atual a ser exibida
   lvProduto.Tag := pagina;

   //Requisi��o por mais dados
   t := TThread.CreateAnonymousThread(procedure
   begin
      dtmProduto.ListarProdutos(pagina, busca, '');
   end);

   t.OnTerminate := ThreadProdutosTerminate;
   t.Start;
end;

procedure TFrmProduto.lvProdutoItemClick(const Sender: TObject;
  const AItem: TListViewItem);
begin
   if not Assigned(FrmProdutoCad) then begin
      Application.CreateForm(TFrmProdutoCad, FrmProdutoCad);
   end;

   FrmProdutoCad.Modo := 'A';
   FrmProdutoCad.Cod_Produto := AItem.TagString.ToInteger;
   FrmProdutoCad.ExecuteOnClose := RefreshListagem;
   FrmProdutoCad.show;
end;

procedure TFrmProduto.lvProdutoPaint(Sender: TObject; Canvas: TCanvas; const ARect: TRectF);
begin
   //Verifica se a Rolagem atingiu o limite para uma nova Carga
   if (lvProduto.Items.Count >= QTD_REG_PAGINA_PRODUTOS) and (lvProduto.Tag >= 0) then begin

      //Monitora o 10� Item, quando a parte de baixo do elemento entrar na ListView, � executado uma nova consulta
      if (lvProduto.GetItemRect(lvProduto.Items.Count - 5).Bottom <= lvProduto.Height) then begin
         ListarProdutos(lvProduto.Tag + 1, edtBuscarProdutos.Text, false);
      end;

   end;
end;

procedure TFrmProduto.lvProdutoUpdateObjects(const Sender: TObject; const AItem: TListViewItem);
begin
   LayoutListViewProdutos(AItem);
end;

procedure TFrmProduto.ThreadProdutosTerminate(Sender: TObject);
var foto: TStream;
begin
   //N�o carregar mais Dados
   if (dtmProduto.qryCnsProduto.RecordCount < QTD_REG_PAGINA_PRODUTOS) then lvProduto.Tag := -1;

   while not dtmProduto.qryCnsProduto.eof do begin
      if (dtmProduto.qryCnsProduto.fieldByName('foto').asString <> emptyStr) then begin
         foto := dtmProduto.qryCnsProduto.createBlobStream(
            dtmProduto.qryCnsProduto.fieldByName('foto'),
            TBlobStreamMode.bmRead
         );
      end else begin
         foto := nil;
      end;

      addProdutoListView(
         dtmProduto.qryCnsProduto.fieldByName('cod_produto_local').asString,
         dtmProduto.qryCnsProduto.fieldByName('descricao').asString,
         dtmProduto.qryCnsProduto.fieldByName('ind_sincronizar').asString,
         dtmProduto.qryCnsProduto.fieldByName('valor').AsFloat,
         dtmProduto.qryCnsProduto.fieldByName('qtd_estoque').AsFloat,
         foto
      );

      if foto <> nil then foto.DisposeOf;
      dtmProduto.qryCnsProduto.next;
   end;

   lvProduto.EndUpdate;

   //Marca que o processo terminou
   lvProduto.TagString := '';

   imgSemProdutos.visible := (lvProduto.items.count = 0);

   //Se deu erro na Thread, � apresentado a mensagem
   if Sender is TThread then begin
      if Assigned(TThread(Sender).FatalException) then begin
         fancy.Show(TIconDialog.Error, 'Aviso', Exception(TThread(sender).FatalException).Message, 'OK');
         exit;
      end;
   end;
end;

procedure TFrmProduto.RefreshListagem;
begin
   ListarProdutos(1, edtBuscarProdutos.Text, true);
end;

end.
