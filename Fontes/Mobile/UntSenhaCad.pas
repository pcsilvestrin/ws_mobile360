unit UntSenhaCad;

interface

uses
  System.SysUtils, System.Types, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Graphics, FMX.Dialogs, FMX.Objects,
  FMX.StdCtrls, FMX.Controls.Presentation, FMX.Forms, System.UITypes,
  uFancyDialog, uFunctions, uLoading;

type
  TFrmSenhaCad = class(TForm)
    rectToolbarProdutos: TRectangle;
    lblTitulo: TLabel;
    btnSalvar: TSpeedButton;
    Image4: TImage;
    btnVoltar: TSpeedButton;
    Image1: TImage;
    rectSenha01: TRectangle;
    Senha01: TLabel;
    Image10: TImage;
    lblSenha01: TLabel;
    rectSenha02: TRectangle;
    Senha02: TLabel;
    Image3: TImage;
    lblSenha02: TLabel;
    procedure rectSenha01Click(Sender: TObject);
    procedure rectSenha02Click(Sender: TObject);
    procedure btnVoltarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure btnSalvarClick(Sender: TObject);
  private
    { Private declarations }
    fancy : TFancyDialog;
    procedure FormatarCampos(Sender: TObject);
    procedure ThreadSenhaCadOnTerminate(Sender: TObject);
  public
    { Public declarations }
  end;

var
  FrmSenhaCad: TFrmSenhaCad;

implementation

{$R *.fmx}

uses UntPrincipal, UntEdicao, DataModule.Usuario;

procedure TFrmSenhaCad.ThreadSenhaCadOnTerminate(Sender: TObject);
begin
   TLoading.Hide; //Fecha o icone de carregando

   if Sender is TThread then begin
      if (Assigned(TThread(sender).FatalException)) then begin
         fancy.Show(TIconDialog.Error, 'Ocorreu um erro', Exception(TThread(sender).FatalException).Message, 'OK');
         exit;
      end;
   end;

   close;
end;

procedure TFrmSenhaCad.btnSalvarClick(Sender: TObject);
var t: TThread;
begin
   if (lblSenha01.TagString <> lblSenha02.TagString) then begin
      fancy.Show(TIconDialog.Warning, 'Verifica��o', 'As senhas n�o conferem, digite novamente!', 'Ok');
      exit;
   end;

   TLoading.Show(FrmSenhaCad, '');

   t := TThread.CreateAnonymousThread(procedure
   begin
      dtmUsuario.AlterarSenha(lblSenha01.TagString);
      dtmUsuario.AlterarSenhaWS(lblSenha01.TagString);
   end);
   t.OnTerminate := ThreadSenhaCadOnTerminate;
   t.Start;
end;

procedure TFrmSenhaCad.btnVoltarClick(Sender: TObject);
begin
   close;
end;

procedure TFrmSenhaCad.FormClose(Sender: TObject; var Action: TCloseAction);
begin
   Action := TCloseAction.caFree;
   FrmSenhaCad := nil;
end;

procedure TFrmSenhaCad.FormCreate(Sender: TObject);
begin
   fancy := TFancyDialog.create(FrmSenhaCad);
end;

procedure TFrmSenhaCad.FormDestroy(Sender: TObject);
begin
   fancy.DisposeOf;
end;

procedure TFrmSenhaCad.FormatarCampos(Sender: TObject);
begin
    TLabel(Sender).tagString := TLabel(Sender).text;
    TLabel(Sender).text:= formatarSenha(TLabel(Sender).text)
end;

procedure TFrmSenhaCad.FormShow(Sender: TObject);
begin
   try
      dtmUsuario.ListarUsuarios;
      lblSenha01.Text       := formatarSenha(dtmUsuario.qryCnsUsuario.fieldByName('SENHA').AsString);
      lblSenha01.TagString  := dtmUsuario.qryCnsUsuario.fieldByName('SENHA').AsString;

      lblSenha02.Text       := lblSenha01.Text;
      lblSenha02.TagString  := lblSenha01.TagString;
   except on e: exception do
     begin
       fancy.Show(TIconDialog.Error, 'Erro', e.Message, 'Ok');
     end;
   end;
end;

procedure TFrmSenhaCad.rectSenha01Click(Sender: TObject);
begin
   FrmEdicao.Editar(
      lblSenha01,
      TTipoCampo.Senha,
      'Senha do Usu�rio',
      'Informe a Senha',
      '',
      true,
      50,
      FormatarCampos
   );
end;

procedure TFrmSenhaCad.rectSenha02Click(Sender: TObject);
begin
   FrmEdicao.Editar(
      lblSenha02,
      TTipoCampo.Senha,
      'Confirme a senha',
      'Informe o Nome',
      '',
      true,
      50,
      FormatarCampos
   );
end;

end.
