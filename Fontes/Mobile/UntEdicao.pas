unit UntEdicao;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.Objects,
  FMX.StdCtrls, FMX.Controls.Presentation, FMX.Edit, FMX.Calendar,
  FMX.Memo.Types, FMX.ScrollBox, FMX.Memo, FMX.Layouts, uFancyDialog;

type
  TTipoCampo = (Edit, Data, Senha, Memo, Valor, Inteiro);
  TExecuteOnClose = procedure(Sender: TObject) of Object;

  TFrmEdicao = class(TForm)
    rectToolbarProdutos: TRectangle;
    lblTitulo: TLabel;
    btnSalvar: TSpeedButton;
    Image4: TImage;
    btnVoltar: TSpeedButton;
    Image1: TImage;
    edtTexto: TEdit;
    edtSenha: TEdit;
    Calendar: TCalendar;
    memo: TMemo;
    StyleBook1: TStyleBook;
    lytValor: TLayout;
    lblValor: TLabel;
    Layout1: TLayout;
    Label2: TLabel;
    Layout2: TLayout;
    Label3: TLabel;
    Layout3: TLayout;
    Label4: TLabel;
    Layout4: TLayout;
    Label5: TLabel;
    Layout5: TLayout;
    Label6: TLabel;
    Layout6: TLayout;
    Label7: TLabel;
    Layout7: TLayout;
    Label8: TLabel;
    Layout8: TLayout;
    Label9: TLabel;
    Layout9: TLayout;
    Label10: TLabel;
    Layout10: TLayout;
    Label11: TLabel;
    Layout11: TLayout;
    Label12: TLabel;
    Layout12: TLayout;
    imgBackspace: TImage;
    procedure btnSalvarClick(Sender: TObject);
    procedure imgBackspaceClick(Sender: TObject);
    procedure Label2Click(Sender: TObject);
    procedure btnVoltarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    { Private declarations }
    Fancy : TFancyDialog;
    obrigatorio: boolean;
    objeto     : TObject;
    ProcExecuteOnClose: TExecuteOnClose;
    tipoCampo  : TTipoCampo;

    procedure TeclaNumero(lbl: TLabel);
    procedure TeclaBackspace;
  public
    { Public declarations }
    procedure Editar(
            obj: TObject;
            tipo_campo: TTipoCampo;
            titulo, textPrompt, texto_padrao: string;
            ind_obrigatorio: boolean;
            tam_maximo: integer;
            ExecuteOnClose: TExecuteOnClose = nil
    );
  end;

var
  FrmEdicao: TFrmEdicao;

implementation

{$R *.fmx}

{ TFrmEdicao }

procedure TFrmEdicao.btnSalvarClick(Sender: TObject);
var ret : String;
begin
   case tipoCampo of
     TTipoCampo.Edit : begin
        ret := edtTexto.Text;
     end;
     TTipoCampo.Data : begin
        ret := FormatDateTime('dd/mm/yyyy', Calendar.Date);
     end;
     TTipoCampo.Senha: begin
        ret := edtSenha.Text;
     end;
     TTipoCampo.Memo: begin
        ret := memo.Text;
     end;
     TTipoCampo.Valor: begin
        ret := lblValor.Text;
     end;
     TTipoCampo.Inteiro: begin
        ret := lblValor.Text;
     end;
   end;

   if (obrigatorio) and (trim(ret).isEmpty) then begin
      fancy.Show(TIconDialog.Warning, 'Esse campo � obrigat�rio!', 'OK');
      exit;
   end;

   if objeto is TLabel then begin
      TLabel(objeto).Text := ret;
   end;

   if (Assigned(ProcExecuteOnClose)) then begin
      ProcExecuteOnClose(objeto);
   end;

   close;
end;

procedure TFrmEdicao.btnVoltarClick(Sender: TObject);
begin
   close;
end;

procedure TFrmEdicao.Editar(obj: TObject; tipo_campo: TTipoCampo; titulo,
  textPrompt, texto_padrao: string;
  ind_obrigatorio: boolean;
  tam_maximo: integer;
  ExecuteOnClose: TExecuteOnClose);
var dia, mes, ano: integer;
begin
   lblTitulo.Text := titulo;
   objeto := obj;
   ProcExecuteOnClose := ExecuteOnClose;
   obrigatorio := ind_obrigatorio;
   tipoCampo   := tipo_campo;

   edtTexto.Visible := (tipoCampo = TTipoCampo.Edit);
   edtSenha.Visible := (tipoCampo = TTipoCampo.Senha);
   calendar.Visible := (tipoCampo = TTipoCampo.Data);
   memo.Visible     := (tipoCampo = TTipoCampo.Memo);
   lytValor.Visible := (tipoCampo in [TTipoCampo.Valor, TTipoCampo.Inteiro]);

   case tipoCampo of
     TTipoCampo.Edit: begin
        edtTexto.TextPrompt := textPrompt;
        edtTexto.MaxLength  := tam_maximo;
        edtTexto.Text       := texto_padrao;
     end;
     TTipoCampo.Data: begin
        if (trim(texto_padrao) <> emptyStr) then begin
           // Para evitar erro na convers�o da Data em Aparelhos diferentes
           dia := Copy(texto_padrao, 1, 2).ToInteger;
           mes := Copy(texto_padrao, 4, 2).ToInteger;
           ano := Copy(texto_padrao, 7, 4).ToInteger;

           Calendar.Date    := EncodeDate(ano, mes, dia);
        end else begin
           Calendar.Date    := date;
        end;
     end;
     TTipoCampo.Senha: begin
        edtSenha.TextPrompt := textPrompt;
        edtSenha.MaxLength  := tam_maximo;
        edtSenha.Text       := texto_padrao;
     end;
     TTipoCampo.Memo: begin
        memo.MaxLength  := tam_maximo;
        memo.Text       := texto_padrao;
     end;
     TTipoCampo.Valor: begin
        lblValor.Text       := texto_padrao;
     end;
     TTipoCampo.Inteiro: begin
        lblValor.Text       := texto_padrao;
     end;
   end;

   if (tipo_campo = TTipoCampo.Edit) then begin

   end;

   FrmEdicao.Show;
end;

procedure TFrmEdicao.FormCreate(Sender: TObject);
begin
   Fancy := TFancyDialog.Create(FrmEdicao);
end;

procedure TFrmEdicao.FormDestroy(Sender: TObject);
begin
   Fancy.DisposeOf;
end;

procedure TFrmEdicao.imgBackspaceClick(Sender: TObject);
begin
    TeclaBackspace;
end;

procedure TFrmEdicao.Label2Click(Sender: TObject);
begin
   TeclaNumero(TLabel(Sender));
end;

procedure TFrmEdicao.TeclaNumero(lbl: TLabel);
var
    valor: string;
begin
    valor := lblValor.Text;  // 9.500,00
    valor := valor.Replace('.', '');  // 9500,00
    valor := valor.Replace(',', '');  // 950000

    valor := valor + lbl.Text;

    if tipoCampo = TTipoCampo.Valor then
        lblValor.Text := FormatFloat('#,##0.00', valor.ToDouble / 100)
    else
        lblValor.Text := FormatFloat('#,##', valor.ToDouble);
end;

procedure TFrmEdicao.TeclaBackspace();
var
    valor: string;
begin
    valor := lblValor.Text;  // 9.500,00
    valor := valor.Replace('.', '');  // 9500,00
    valor := valor.Replace(',', '');  // 950000

    if Length(valor) > 1 then
        valor := Copy(valor, 1, length(valor) - 1)
    else
        valor := '0';

    if tipoCampo = TTipoCampo.Valor then
        lblValor.Text := FormatFloat('#,##0.00', valor.ToDouble / 100)
    else
        lblValor.Text := FormatFloat('#,##0', valor.ToDouble);
end;

end.
