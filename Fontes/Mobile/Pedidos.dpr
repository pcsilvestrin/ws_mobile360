program Pedidos;

uses
  System.StartUpCopy,
  FMX.Forms,
  UntInicial in 'UntInicial.pas' {frmInicial},
  UntLogin in 'UntLogin.pas' {FrmLogin},
  UntPrincipal in 'UntPrincipal.pas' {FrmPrincipal},
  DataModule.Global in 'DataModule\DataModule.Global.pas' {dtmGlobal: TDataModule},
  DataModule.Pedido in 'DataModule\DataModule.Pedido.pas' {dtmPedido: TDataModule},
  DataModule.Usuario in 'DataModule\DataModule.Usuario.pas' {dtmUsuario: TDataModule},
  DataModule.Notificacao in 'DataModule\DataModule.Notificacao.pas' {dtmNotificacao: TDataModule},
  uFunctions in 'Units\uFunctions.pas',
  UntProduto in 'UntProduto.pas' {FrmProduto},
  DataModule.Produto in 'DataModule\DataModule.Produto.pas' {dtmProduto: TDataModule},
  UntProdutoCad in 'UntProdutoCad.pas' {FrmProdutoCad},
  uActionSheet in 'Units\uActionSheet.pas',
  u99Permissions in 'Units\u99Permissions.pas',
  UntEdicao in 'UntEdicao.pas' {FrmEdicao},
  UntClienteCad in 'UntClienteCad.pas' {FrmClienteCad},
  uFancyDialog in 'Units\uFancyDialog.pas',
  uCombobox in 'Units\uCombobox.pas',
  uFormat in 'Units\uFormat.pas',
  UntSenhaCad in 'UntSenhaCad.pas' {FrmSenhaCad},
  DataModule.Cliente in 'DataModule\DataModule.Cliente.pas' {dtmCliente: TDataModule},
  UntSincronizacao in 'UntSincronizacao.pas' {FrmSincronizacao},
  UntPedidoCad in 'UntPedidoCad.pas' {FrmPedidoCad},
  UntPedidoItemCad in 'UntPedidoItemCad.pas' {FrmPedidoItemCad},
  UntClienteBusca in 'UntClienteBusca.pas' {FrmClienteBusca},
  UntProdutoBusca in 'UntProdutoBusca.pas' {FrmProdutoBusca},
  uConstantes in 'Units\uConstantes.pas',
  uSession in 'Units\uSession.pas',
  uLoading in 'Units\uLoading.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TdtmGlobal, dtmGlobal);
  Application.CreateForm(TfrmInicial, frmInicial);
  Application.CreateForm(TFrmEdicao, FrmEdicao);
  Application.Run;
end.
