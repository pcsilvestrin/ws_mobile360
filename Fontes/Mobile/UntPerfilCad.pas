unit UntPerfilCad;

interface

uses
  System.SysUtils, System.Types, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Graphics, FMX.Dialogs, FMX.Objects,
  FMX.StdCtrls, FMX.Controls.Presentation, FMX.Forms, System.UITypes,
  uFancyDialog, uLoading, uSession;

type
  TFrmPerfilCad = class(TForm)
    rectToolbarProdutos: TRectangle;
    lblTitulo: TLabel;
    btnSalvar: TSpeedButton;
    Image4: TImage;
    btnVoltar: TSpeedButton;
    Image1: TImage;
    rectNome: TRectangle;
    Label1: TLabel;
    Image10: TImage;
    lblNome: TLabel;
    rectEmail: TRectangle;
    Label3: TLabel;
    Image3: TImage;
    lblEmail: TLabel;
    procedure rectNomeClick(Sender: TObject);
    procedure rectEmailClick(Sender: TObject);
    procedure btnVoltarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure btnSalvarClick(Sender: TObject);
  private
    { Private declarations }
    fancy : TFancyDialog;
    procedure ThreadPerfilOnTerminate(Sender: TObject);
  public
    { Public declarations }
  end;

var
  FrmPerfilCad: TFrmPerfilCad;

implementation

{$R *.fmx}

uses UntPrincipal, UntEdicao, DataModule.Usuario;

procedure TFrmPerfilCad.ThreadPerfilOnTerminate(Sender: TObject);
begin
   TLoading.Hide; //Fecha o icone de carregando

   if Sender is TThread then begin
      if (Assigned(TThread(sender).FatalException)) then begin
         fancy.Show(TIconDialog.Error, 'Ocorreu um erro', Exception(TThread(sender).FatalException).Message, 'OK');
         exit;
      end;
   end;

   close;
end;


procedure TFrmPerfilCad.btnSalvarClick(Sender: TObject);
var t: TThread;
begin
   TLoading.Show(FrmPerfilCad, '');

   t := TThread.CreateAnonymousThread(procedure
   begin
      dtmUsuario.AlteraDadosContaWS(lblNome.Text, lblEmail.Text);
      dtmUsuario.EditarUsuario(lblNome.Text, lblEmail.Text);

      //Trata a sess�o do Usu�rio
      TSession.NOME        := lblNome.text;
      TSession.EMAIL       := lblEmail.text;
      ////////////
   end);
   t.OnTerminate := ThreadPerfilOnTerminate;
   t.Start;
end;

procedure TFrmPerfilCad.btnVoltarClick(Sender: TObject);
begin
   close;
end;

procedure TFrmPerfilCad.FormClose(Sender: TObject; var Action: TCloseAction);
begin
   Action := TCloseAction.caFree;
   FrmPerfilCad := nil;
end;

procedure TFrmPerfilCad.FormCreate(Sender: TObject);
begin
   fancy := TFancyDialog.create(FrmPerfilCad);
end;

procedure TFrmPerfilCad.FormDestroy(Sender: TObject);
begin
   fancy.DisposeOf;
end;

procedure TFrmPerfilCad.FormShow(Sender: TObject);
begin
   try
      dtmUsuario.ListarUsuarios;
      lblNome.Text  := dtmUsuario.qryCnsUsuario.fieldByName('NOME').AsString;
      lblEmail.Text := dtmUsuario.qryCnsUsuario.fieldByName('EMAIL').AsString;
   except on e: exception do
     begin
       fancy.Show(TIconDialog.Error, 'Erro', e.Message, 'Ok');
     end;
   end;
end;

procedure TFrmPerfilCad.rectEmailClick(Sender: TObject);
begin
   FrmEdicao.Editar(
      lblEmail,
      TTipoCampo.Edit,
      'E-Mail do Usu�rio',
      'Informe o E-Mail',
      lblEmail.text,
      true,
      100
   );
end;

procedure TFrmPerfilCad.rectNomeClick(Sender: TObject);
begin
   FrmEdicao.Editar(
      lblNome,
      TTipoCampo.Edit,
      'Nome do Usu�rio',
      'Informe o Nome',
      lblNome.text,
      true,
      100
   );
end;

end.
