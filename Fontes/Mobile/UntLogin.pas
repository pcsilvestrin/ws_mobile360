unit UntLogin;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.TabControl,
  FMX.Objects, FMX.Controls.Presentation, FMX.StdCtrls, FMX.Layouts, FMX.Edit,
  uLoading, uFancyDialog;

type
  TFrmLogin = class(TForm)
    TabControl: TTabControl;
    tabLogin: TTabItem;
    tabCriarConta: TTabItem;
    Rectangle1: TRectangle;
    Layout7: TLayout;
    Image4: TImage;
    Label7: TLabel;
    Layout1: TLayout;
    lblCriar: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    edtEmail: TEdit;
    edtSenha: TEdit;
    StyleBook1: TStyleBook;
    btnLogin: TSpeedButton;
    Rectangle2: TRectangle;
    Layout2: TLayout;
    Image1: TImage;
    Label4: TLabel;
    Layout3: TLayout;
    Label5: TLabel;
    Label6: TLabel;
    edtContaEmail: TEdit;
    edtContaSenha: TEdit;
    btnCriarConta: TSpeedButton;
    lblLogin: TLabel;
    Label9: TLabel;
    edtContaNome: TEdit;
    procedure lblCriarClick(Sender: TObject);
    procedure lblLoginClick(Sender: TObject);
    procedure btnLoginClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure btnCriarContaClick(Sender: TObject);
  private
    fancy : TFancyDialog;
    procedure ThreadLoginOnTerminate(Sender: TObject);
    procedure OpenFormPrincipal;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmLogin: TFrmLogin;

implementation

{$R *.fmx}

uses UntPrincipal, DataModule.Cliente, uSession, DataModule.Usuario;

procedure TFrmLogin.OpenFormPrincipal;
begin
   if not Assigned(FrmPrincipal) then begin
      Application.CreateForm(TFrmPrincipal, FrmPrincipal);
   end;

   //Trata a sess�o do Usu�rio
   TSession.COD_USUARIO := dtmUsuario.TabUsuario.fieldByName('cod_usuario').AsInteger;
   TSession.NOME        := dtmUsuario.TabUsuario.fieldByName('nome').AsString;
   TSession.EMAIL       := dtmUsuario.TabUsuario.fieldByName('email').AsString;
   TSession.TOKEN_JWT   := dtmUsuario.TabUsuario.fieldByName('token').AsString;
   ////////////

   Application.MainForm := FrmPrincipal;
   FrmPrincipal.Show;
   FrmLogin.close;
end;

procedure TFrmLogin.ThreadLoginOnTerminate(Sender: TObject);
begin
   TLoading.Hide; //Fecha o icone de carregando

   if Sender is TThread then begin
      if (Assigned(TThread(sender).FatalException)) then begin
         fancy.Show(TIconDialog.Error, 'Ocorreu um erro', Exception(TThread(sender).FatalException).Message, 'OK');
         exit;
      end;
   end;

   //Salva a data da ultima sincroniza��o
   dtmUsuario.SalvarConfig('DATA-ULT-SINC', '2000-01-01 08:00:00');

   OpenFormPrincipal;
end;

procedure TFrmLogin.btnCriarContaClick(Sender: TObject);
var t: TThread;
begin
   TLoading.Show(FrmLogin, '');

   t := TThread.CreateAnonymousThread(procedure
   begin
      dtmUsuario.NovaContaWS(edtContaNome.Text, edtContaEmail.Text, edtContaSenha.Text);

      dtmUsuario.Logout;
      dtmUsuario.ExcluirUsuario(dtmUsuario.TabUsuario.fieldByName('cod_usuario').AsInteger);

      dtmUsuario.InserirUsuario(
        dtmUsuario.TabUsuario.fieldByName('cod_usuario').AsInteger,
        dtmUsuario.TabUsuario.fieldByName('nome').AsString,
        dtmUsuario.TabUsuario.fieldByName('email').AsString,
        edtContaSenha.text,
        dtmUsuario.TabUsuario.fieldByName('token').AsString
      );

   end);
   t.OnTerminate := ThreadLoginOnTerminate;
   t.Start;
end;

procedure TFrmLogin.btnLoginClick(Sender: TObject);
var t: TThread;
begin
   TLoading.Show(FrmLogin, '');

   t := TThread.CreateAnonymousThread(procedure
   begin
      dtmUsuario.LoginWS(edtEmail.Text, edtSenha.Text);

      dtmUsuario.Logout;
      dtmUsuario.ExcluirUsuario(dtmUsuario.TabUsuario.fieldByName('cod_usuario').AsInteger);

      dtmUsuario.InserirUsuario(
        dtmUsuario.TabUsuario.fieldByName('cod_usuario').AsInteger,
        dtmUsuario.TabUsuario.fieldByName('nome').AsString,
        dtmUsuario.TabUsuario.fieldByName('email').AsString,
        edtSenha.text,
        dtmUsuario.TabUsuario.fieldByName('token').AsString
      );

   end);
   t.OnTerminate := ThreadLoginOnTerminate;
   t.Start;
end;

procedure TFrmLogin.FormClose(Sender: TObject; var Action: TCloseAction);
begin
   Action   := TCloseAction.caFree;
   FrmLogin := nil;
end;

procedure TFrmLogin.FormCreate(Sender: TObject);
begin
   fancy := TFancyDialog.Create(FrmLogin);
end;

procedure TFrmLogin.FormDestroy(Sender: TObject);
begin
   fancy.DisposeOf;
end;

procedure TFrmLogin.lblCriarClick(Sender: TObject);
begin
   TabControl.GotoVisibleTab(1);
end;

procedure TFrmLogin.lblLoginClick(Sender: TObject);
begin
   TabControl.GotoVisibleTab(0);
end;

end.
