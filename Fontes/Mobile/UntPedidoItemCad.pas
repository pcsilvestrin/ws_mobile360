unit UntPedidoItemCad;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.Objects,
  FMX.StdCtrls, FMX.Controls.Presentation, uFancyDialog;

type
  TExecuteOnClose = procedure of object;

  TFrmPedidoItemCad = class(TForm)
    rectToolbarProdutos: TRectangle;
    lblTitulo: TLabel;
    btnSalvar: TSpeedButton;
    Image4: TImage;
    btnVoltar: TSpeedButton;
    Image1: TImage;
    rectVlrUnit: TRectangle;
    Label3: TLabel;
    Image3: TImage;
    lblVlrUnit: TLabel;
    rectProduto: TRectangle;
    Label1: TLabel;
    Image10: TImage;
    lblProduto: TLabel;
    rectVlrTotal: TRectangle;
    Label2: TLabel;
    Image2: TImage;
    lblVlrTotal: TLabel;
    rectQtde: TRectangle;
    Label5: TLabel;
    Image5: TImage;
    lblQtde: TLabel;
    imgMenos: TImage;
    btnMenos: TSpeedButton;
    btnMais: TSpeedButton;
    imgMais: TImage;
    procedure rectProdutoClick(Sender: TObject);
    procedure rectQtdeClick(Sender: TObject);
    procedure rectVlrUnitClick(Sender: TObject);
    procedure rectVlrTotalClick(Sender: TObject);
    procedure btnVoltarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure btnSalvarClick(Sender: TObject);
    procedure btnMaisClick(Sender: TObject);
  private
    fancy : TFancyDialog;
    FCod_Item_Pedido: integer;
    FModo: String;
    FExecuteOnClose: TExecuteOnClose;
    procedure SelecionarProduto(cod_produto_local: integer; descricao: string;
      valor: double);
    procedure CalcularTotalItem;
    procedure calcularQtde(qtd: integer);
    procedure CalcularUnitarioItem;
    procedure ClickTotalItem(Sender: TObject);
    procedure ClickUnitarioItem(Sender: TObject);
    { Private declarations }
  public
    { Public declarations }
  published
    property Modo: String read FModo write FModo;
    property Cod_Item_pedido: integer read FCod_Item_Pedido write FCod_Item_Pedido;
    property ExecuteOnClose : TExecuteOnClose read FExecuteOnClose write FExecuteOnClose;
  end;

var
  FrmPedidoItemCad: TFrmPedidoItemCad;

implementation

{$R *.fmx}

uses UntPrincipal, UntEdicao, DataModule.Pedido, uFunctions, UntProdutoBusca;

procedure TFrmPedidoItemCad.btnMaisClick(Sender: TObject);
begin
   calcularQtde(TSpeedButton(sender).tag);
end;

procedure TFrmPedidoItemCad.calcularQtde(qtd: integer);
var iQtd: integer;
begin
   try
     iQtd := lblQtde.Text.ToInteger + qtd;
     if iQtd < 1 then iQtd := 1;
   except
     iQtd := 1;
   end;
   lblQtde.Text := iQtd.ToString;

   CalcularTotalItem;
end;

procedure TFrmPedidoItemCad.btnSalvarClick(Sender: TObject);
begin
   if (Trim(lblProduto.Text) = emptyStr) then begin
      fancy.Show(TIconDialog.Warning, 'Escolha um produto!', 'OK');
      exit;
   end;

   try
      if (Modo = 'I') then begin
         dtmPedido.IncluirItem(
            lblProduto.Tag,
            lblQtde.Text.ToInteger(),
            StringToFloat(lblVlrUnit.Text),
            StringToFloat(lblVlrTotal.Text)
         );
      end else begin
         dtmPedido.EditarItem(
            Cod_Item_pedido,
            lblProduto.Tag,
            lblQtde.Text.ToInteger(),
            StringToFloat(lblVlrUnit.Text),
            StringToFloat(lblVlrTotal.Text)
         );
      end;

      if (Assigned(ExecuteOnClose)) then ExecuteOnClose;

      close;
   except on e: exception do
     begin
        fancy.Show(TIconDialog.Error, 'Erro ao salvar dados do item: '+e.Message, 'OK');
     end;
   end;
end;

procedure TFrmPedidoItemCad.btnVoltarClick(Sender: TObject);
begin
   close;
end;

procedure TFrmPedidoItemCad.FormClose(Sender: TObject; var Action: TCloseAction);
begin
   Action := TCloseAction.caFree;
   FrmPedidoItemCad := nil;
end;

procedure TFrmPedidoItemCad.FormCreate(Sender: TObject);
begin
   fancy := TFancyDialog.Create(FrmPedidoItemCad);
end;

procedure TFrmPedidoItemCad.FormDestroy(Sender: TObject);
begin
   fancy.DisposeOf;
end;

procedure TFrmPedidoItemCad.FormShow(Sender: TObject);
begin
   lblTitulo.text   := 'Incluir Produto';
   try
     if (Modo = 'A') then begin
        dtmPedido.ListarPedidoItemId(Cod_Item_pedido);

        lblProduto.text  := dtmPedido.qryCnsItem.fieldByName('DESCRICAO').AsString;
        lblProduto.Tag   := dtmPedido.qryCnsItem.fieldByName('COD_PRODUTO_LOCAL').AsInteger;

        lblQtde.text     := FormatFloat('#,##0', dtmPedido.qryCnsItem.fieldByName('QTD').AsFloat);
        lblVlrUnit.text  := FormatFloat('###,###,##0.00', dtmPedido.qryCnsItem.fieldByName('VALOR_UNITARIO').AsFloat);
        lblVlrTotal.text := FormatFloat('###,###,##0.00', dtmPedido.qryCnsItem.fieldByName('VALOR_TOTAL').AsFloat);
        lblTitulo.text   := 'Editar Produto';
     end;
   except on e: exception do
     begin
        fancy.Show(TIconDialog.Error, 'Erro ao carregar dados do produto: '+#13+e.message, 'OK');
     end;
   end;
end;

procedure TFrmPedidoItemCad.SelecionarProduto(cod_produto_local: integer; descricao: string; valor: double);
begin
    lblProduto.Text := descricao;
    lblProduto.Tag  := cod_produto_local;
    lblVlrUnit.Text := FormatFloat('#,##0.00', valor);
    CalcularTotalItem;
end;

procedure TFrmPedidoItemCad.CalcularTotalItem;
var qtd, vl_unitario: double;
begin
    try
        qtd := lblQtde.Text.ToDouble;
        vl_unitario := StringToFloat(lblVlrUnit.Text);
    except
        qtd := 0;
        vl_unitario := 0;
    end;

    lblVlrTotal.Text := FormatFloat('#,##0.00', qtd * vl_unitario);
end;

procedure TFrmPedidoItemCad.CalcularUnitarioItem;
var qtd, vl_total: double;
begin
    try
        qtd := lblQtde.Text.ToDouble;
        vl_total := StringToFloat(lblVlrTotal.Text);
    except
        qtd := 0;
        vl_total := 0;
    end;

    if (qtd > 0.00) then begin
       lblVlrUnit.Text := FormatFloat('#,##0.00', vl_total / qtd);
    end else begin
       lblVlrUnit.Text := FormatFloat('#,##0.00', 0.00);
    end;
end;

procedure TFrmPedidoItemCad.rectProdutoClick(Sender: TObject);
begin
    if NOT Assigned(FrmProdutoBusca) then
        Application.CreateForm(TFrmProdutoBusca, FrmProdutoBusca);

    FrmProdutoBusca.ExecuteOnClick := SelecionarProduto;
    FrmProdutoBusca.Show;
end;

procedure TFrmPedidoItemCad.rectQtdeClick(Sender: TObject);
begin
   FrmEdicao.Editar(
      lblQtde,
      TTipoCampo.Inteiro,
      'Qtde. do Produto',
      'Informe a Quantidade',
      lblQtde.text,
      true,
      0
   );
end;

procedure TFrmPedidoItemCad.ClickTotalItem(Sender: TObject);
begin
   CalcularUnitarioItem;
end;

procedure TFrmPedidoItemCad.rectVlrTotalClick(Sender: TObject);
begin
   FrmEdicao.Editar(
      lblVlrTotal,
      TTipoCampo.Valor,
      'Valor Total',
      'Informe o Valor',
      lblVlrTotal.text,
      true,
      0,
      ClickTotalItem
   );
end;

procedure TFrmPedidoItemCad.ClickUnitarioItem(Sender: TObject);
begin
   CalcularTotalItem;
end;

procedure TFrmPedidoItemCad.rectVlrUnitClick(Sender: TObject);
begin
   FrmEdicao.Editar(
      lblVlrUnit,
      TTipoCampo.Valor,
      'Valor Unit�rio',
      'Informe o Valor',
      lblVlrUnit.text,
      true,
      0,
      ClickUnitarioItem
   );
end;

end.
