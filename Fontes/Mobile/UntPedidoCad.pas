unit UntPedidoCad;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, UntPrincipal,
  FMX.Objects, FMX.StdCtrls, FMX.Controls.Presentation, FMX.Layouts,
  FMX.TabControl, FMX.ListBox, FMX.ListView.Types, FMX.ListView.Appearances,
  FMX.ListView.Adapters.Base, FMX.ListView, uFancyDialog, uCombobox;

type
  TExecuteOnClose = procedure of object;

  TFrmPedidoCad = class(TForm)
    rectToolbarProdutos: TRectangle;
    lblTitulo: TLabel;
    btnSalvar: TSpeedButton;
    Image4: TImage;
    btnVoltar: TSpeedButton;
    Image1: TImage;
    lytAbas: TLayout;
    rectAbaPedido: TRectangle;
    rectAbaItem: TRectangle;
    lblAbaPedido: TLabel;
    lblAbaItem: TLabel;
    TabControl: TTabControl;
    tabPedido: TTabItem;
    tabItensPedido: TTabItem;
    ListBox1: TListBox;
    lbiCliente: TListBoxItem;
    lblCliente: TLabel;
    Image10: TImage;
    Line1: TLine;
    lbiTipo: TListBoxItem;
    Label4: TLabel;
    lblTipo: TLabel;
    Image2: TImage;
    Line8: TLine;
    lbiData: TListBoxItem;
    Label5: TLabel;
    lblData: TLabel;
    Image3: TImage;
    Line10: TLine;
    lbiContato: TListBoxItem;
    Label6: TLabel;
    lblContato: TLabel;
    Image5: TImage;
    Line6: TLine;
    lbiCondPgto: TListBoxItem;
    Label8: TLabel;
    lblCondPgto: TLabel;
    Image6: TImage;
    Line7: TLine;
    lbiPrazoEnt: TListBoxItem;
    Label10: TLabel;
    lblPrazoEntrega: TLabel;
    Image7: TImage;
    Line9: TLine;
    lbiObs: TListBoxItem;
    Label12: TLabel;
    lblObs: TLabel;
    Image8: TImage;
    Line5: TLine;
    lbiAllBottom: TListBoxItem;
    btnExcluir: TSpeedButton;
    Image15: TImage;
    lvtItem: TListView;
    imgIconeSemFoto: TImage;
    imgIconeMenos: TImage;
    imgIconeMais: TImage;
    imgIconeExcluir: TImage;
    rectTotal: TRectangle;
    btnInserir: TSpeedButton;
    Label3: TLabel;
    lblVlrTotal: TLabel;
    imgSemProduto: TImage;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure rectAbaItemClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btnVoltarClick(Sender: TObject);
    procedure btnInserirClick(Sender: TObject);
    procedure ListBox1ItemClick(const Sender: TCustomListBox;
      const Item: TListBoxItem);
    procedure FormShow(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure lvtItemItemClickEx(const Sender: TObject; ItemIndex: Integer;
      const LocalClickPos: TPointF; const ItemObject: TListItemDrawable);
    procedure btnSalvarClick(Sender: TObject);
    procedure btnExcluirClick(Sender: TObject);
  private
    FCod_Pedido: integer;
    FModo: String;
    fancy : TFancyDialog;
    combo : TCustomCombo;
    FExecuteOnClose: TExecuteOnClose;
    procedure AbrirAba(rect: TRectangle);
    procedure SelecionarCliente(cod_clliente_local: integer; nome: string);
    procedure addProdutoListView(cod_item: integer; descricao: string; qtde,
      vlrUnit, vlrTotal: double; foto: TStream);
    procedure LayoutListviewProduto(AItem: TListViewItem);
    procedure ListarItens;
    procedure CalcularTotalPedido;
    procedure CalculaQtdListView(Item: TListViewItem; qtde: integer);
    procedure ClickDeletePedido(Sender: TObject);
    {$IFDEF MSWINDOWS}
    procedure onComboClick(Sender: TObject);

    {$ELSE}
    procedure onComboClick(Sender: TObject; const Point: TPointF);
    {$ENDIF}
    { Private declarations }
  public
    { Public declarations }
  published
    property Modo: String read FModo write FModo;
    property Cod_Pedido     : integer read FCod_Pedido write FCod_Pedido;
    property ExecuteOnClose : TExecuteOnClose read FExecuteOnClose write FExecuteOnClose;
  end;

var
  FrmPedidoCad: TFrmPedidoCad;

implementation

{$R *.fmx}

uses UntPedidoItemCad, UntClienteBusca, DataModule.Pedido, Data.DB, uFunctions,
  UntEdicao, uSession;

procedure TFrmPedidoCad.ClickDeletePedido(Sender: TObject);
begin
    try
        dtmPedido.ExcluirPedido(Cod_Pedido);

        if Assigned(ExecuteOnClose) then
            ExecuteOnClose;

        close;
    except on ex:exception do
        fancy.Show(TIconDialog.Error, 'Aviso', ex.Message, 'OK');
    end;
end;

procedure TFrmPedidoCad.btnExcluirClick(Sender: TObject);
begin
    fancy.Show(TIconDialog.Question, 'Confirma��o', 'Confirma a exclus�o do pedido?',
               'Sim', ClickDeletePedido, 'N�o');
end;

procedure TFrmPedidoCad.btnInserirClick(Sender: TObject);
begin
   if not Assigned(FrmPedidoItemCad) then begin
      Application.CreateForm(TFrmPedidoItemCad, FrmPedidoItemCad);
   end;

   FrmPedidoItemCad.modo := 'I';
   FrmPedidoItemCad.cod_item_pedido := 0;
   FrmPedidoItemCad.ExecuteOnClose  := ListarItens;
   FrmPedidoItemCad.Show;
end;

procedure TFrmPedidoCad.btnSalvarClick(Sender: TObject);
begin
    if lblCliente.Text = '' then
    begin
        fancy.Show(TIconDialog.Warning, 'Aviso', 'Informe o cliente', 'OK');
        exit;
    end;
    if lblCondPgto.Text = '' then
    begin
        fancy.Show(TIconDialog.Warning, 'Aviso', 'Informe a condi��o de pagamento', 'OK');
        exit;
    end;

    try
        if Modo = 'I' then
            dtmPedido.InserirPedido(lblCliente.Tag, TSession.COD_USUARIO, lblCondPgto.Tag,
                                   StringToDate(lblData.Text), 0, lblContato.Text,
                                   lblObs.Text, lblPrazoEntrega.Text, 'S', StringToFloat(lblVlrTotal.Text), 0)
        else
            dtmPedido.EditarPedido(Cod_Pedido, lblCliente.Tag, lblCondPgto.Tag,
                                  StringToDate(lblData.Text), 0, lblContato.Text,
                                  lblObs.Text, lblPrazoEntrega.Text, 'S', StringToFloat(lblVlrTotal.Text));

        if Assigned(ExecuteOnClose) then
            ExecuteOnClose;

        close;

    except on ex:exception do
        fancy.Show(TIconDialog.Error, 'Erro', 'Erro ao salvar dados do cliente: ' + ex.Message, 'OK');
    end;
end;

procedure TFrmPedidoCad.btnVoltarClick(Sender: TObject);
begin
   close;
end;

procedure TFrmPedidoCad.FormClose(Sender: TObject; var Action: TCloseAction);
begin
   Action := TCloseAction.caFree;
   FrmPedidoCad := nil;
end;

procedure TFrmPedidoCad.FormCreate(Sender: TObject);
begin
   fancy    := TFancyDialog.create(FrmPedidoCad);
   TabControl.ActiveTab := tabPedido;

   //Montar lista de Cond. Pagamento            !
   dtmPedido.ListarCondPagto;

   combo := TCustomCombo.Create(FrmPedidoCad);
   combo.TitleMenuText := 'Condi��o de Pagamento';
   combo.BackgroundColor := $FFF2F2F8;
   combo.ItemBackgroundColor := $FFFFFFFF;
   combo.OnClick := onComboClick;

   while not dtmPedido.qryConsCondPagto.eof do begin
      combo.AddItem(dtmPedido.qryConsCondPagto.fieldByName('cod_cond_pagto').asString, dtmPedido.qryConsCondPagto.fieldByName('cond_pagto').asstring);
      dtmPedido.qryConsCondPagto.next;
   end;
end;

{$IFDEF MSWINDOWS}
procedure TFrmPedidoCad.onComboClick(Sender: TObject);
begin
    combo.HideMenu;
    lblCondPgto.Text := combo.DescrItem;
    lblCondPgto.tag  := combo.CodItem.ToInteger;
end;
{$ELSE}
procedure TFrmPedidoCad.onComboClick(Sender: TObject; const Point: TPointF);
begin
    combo.HideMenu;
    lblCondPgto.Text := combo.DescrItem;
    lblCondPgto.tag  := combo.CodItem.ToInteger;
end;
{$ENDIF}

procedure TFrmPedidoCad.FormDestroy(Sender: TObject);
begin
   fancy.DisposeOf;
   combo.DisposeOf;
end;

procedure TFrmPedidoCad.FormShow(Sender: TObject);
begin
   lblTitulo.text   := 'Incluir Pedido';
   try
     //Carrega a tabela Tempor�ria de Itens
     dtmPedido.CarregarTabelaTemp(Cod_pedido);

     btnExcluir.Visible := (Modo = 'A');
     lblData.Text       := FormatDateTime('dd/mm/yyyy', date);
     lblTipo.Text       := 'Or�amento';

     if (Modo = 'A') then begin
        dtmPedido.ListarPedidoId(Cod_Pedido);

        lblCliente.Text := dtmPedido.qryPedido.fieldByName('nome').asString;
        lblCliente.Tag  := dtmPedido.qryPedido.fieldByName('cod_cliente_local').asInteger;

        if dtmPedido.qryPedido.fieldByName('tipo_pedido').asString = 'P' then begin
           lblTipo.Text := 'Pedido';
        end;

        lblData.Text    := FormatDateTime('dd/mm/yyyy', dtmPedido.qryPedido.fieldByName('data_pedido').asDateTime);
        lblContato.Text := dtmPedido.qryPedido.fieldByName('contato').asString;

        lblCondPgto.Text:= dtmPedido.qryPedido.fieldByName('cond_pagto').asString;
        lblCondPgto.Tag := dtmPedido.qryPedido.fieldByName('cod_cond_pagto').asInteger;

        lblPrazoEntrega.Text := dtmPedido.qryPedido.fieldByName('prazo_entrega').asString;
        lblObs.Text     := dtmPedido.qryPedido.fieldByName('obs').asString;

        lblTitulo.text := 'Editar Pedido';
     end;

     // Carrega os itens
     ListarItens;

   except on e: exception do
     begin
        fancy.Show(TIconDialog.Error, 'Erro ao carregar Pedido: '+#13+e.message, 'OK');
     end;
   end;
end;

procedure TFrmPedidoCad.ListarItens;
var
    foto: TStream;
begin
    try
        dtmPedido.ListarPedidoItemId(0);
        lvtItem.BeginUpdate;
        lvtItem.Items.Clear;

        with dtmPedido.qryCnsItem do begin
            while NOT EOF do begin
               if fieldbyname('foto').asstring <> '' then
                  foto := CreateBlobStream(fieldbyname('foto'), TBlobStreamMode.bmRead)
               else
                  foto := nil;

               AddProdutoListview(FieldByName('cod_item').AsInteger,
                                  FieldByName('descricao').AsString,
                                  FieldByName('qtd').AsFloat,
                                  FieldByName('valor_unitario').AsFloat,
                                  FieldByName('valor_total').AsFloat,
                                  foto);

               Next;
            end;
        end;

        lvtItem.EndUpdate;
        imgSemProduto.Visible := dtmPedido.qryCnsItem.RecordCount = 0;

        // Calcular total do pedido
        CalcularTotalPedido;

    except on ex:exception do
        fancy.Show(TIconDialog.Error, 'Erro', 'Erro ao carregar itens do pedido: ' + ex.Message, 'OK');
    end;
end;

procedure TFrmPedidoCad.CalculaQtdListView(Item: TListViewItem; qtde: integer);
var
    qtd_atual, valor_unitario: double;
begin
    // Qtd...
    qtd_atual := TListItemText(Item.Objects.FindDrawable('txtQtde')).Text.ToInteger;
    qtd_atual := qtd_atual + Qtde;

    if qtd_atual < 1 then
        qtd_atual := 1;

    TListItemText(Item.Objects.FindDrawable('txtQtde')).Text := qtd_atual.tostring;


    // Unitario...
    valor_unitario := TListItemText(Item.Objects.FindDrawable('txtUnitario')).TagFloat;
    TListItemText(Item.Objects.FindDrawable('txtUnitario')).Text := FormatFloat('#,##0', qtd_atual) + ' x ' +
                                                                    FormatFloat('R$#,##0.00', valor_unitario);

    // Total...
    TListItemText(Item.Objects.FindDrawable('txtTotal')).Text := FormatFloat('R$#,##0.00', qtd_atual * valor_unitario);
end;

procedure TFrmPedidoCad.CalcularTotalPedido;
var
    total: double;
    i: integer;
begin
    total := 0;

    for i := 0 to lvtItem.Items.Count - 1 do
        total := total + StringToFloat(TListItemText(lvtItem.Items[i].Objects.FindDrawable('txtTotal')).Text);

    lblVlrTotal.Text := FormatFloat('R$#,##0.00', total);
end;

procedure TFrmPedidoCad.SelecionarCliente(cod_clliente_local: integer; nome: string);
begin
   lblCliente.Text := nome;
   lblCliente.Tag  := cod_clliente_local;
end;

procedure TFrmPedidoCad.ListBox1ItemClick(const Sender: TCustomListBox;
  const Item: TListBoxItem);
begin
   if Item = lbiCliente then begin
      if not Assigned(FrmClienteBusca) then begin
         Application.CreateForm(TFrmClienteBusca, FrmClienteBusca);
      end;
      FrmClienteBusca.ExecuteOnClick := SelecionarCliente;
      FrmClienteBusca.Show;
   end else if item = lbiData then begin
      FrmEdicao.Editar(lblData, TTipoCampo.Data, 'Data do Pedido', '', lblData.text, true, 0);
   end else if item = lbiContato then begin
      FrmEdicao.Editar(lblContato, TTipoCampo.Edit, 'Nome do Contato', 'Informe o Nome', lblContato.text, false, 100);
   end else if item = lbiCondPgto then begin
      combo.ShowMenu;
   end else if item = lbiPrazoEnt then begin
      FrmEdicao.Editar(lblPrazoEntrega, TTipoCampo.Edit, 'Prazo de Entrega', 'Informe o Prazo', lblPrazoEntrega.text, false, 50);
   end else if item = lbiObs then begin
      FrmEdicao.Editar(lblObs, TTipoCampo.Memo, 'Obs. do Pedido', '', lblObs.text, false, 500);
   end;
end;

procedure TFrmPedidoCad.lvtItemItemClickEx(const Sender: TObject;
  ItemIndex: Integer; const LocalClickPos: TPointF;
  const ItemObject: TListItemDrawable);
var
    qtd_atual: double;
begin
    if Assigned(ItemObject) then
    begin
        try
            if ItemObject.Name = 'imgMenos' then
            begin
                CalculaQtdListView(lvtItem.Items[ItemIndex], -1);

                qtd_atual := TListItemText(lvtItem.items[ItemIndex].Objects.FindDrawable('txtQtde')).Text.ToInteger;
                DtmPedido.AtuailzarQtdeItem(Trunc(ItemObject.TagFloat), qtd_atual);
                exit;
            end
            else
            if ItemObject.Name = 'imgMais' then
            begin
                CalculaQtdListView(lvtItem.Items[ItemIndex], 1);

                qtd_atual := TListItemText(lvtItem.items[ItemIndex].Objects.FindDrawable('txtQtde')).Text.ToInteger;
                DtmPedido.AtuailzarQtdeItem(Trunc(ItemObject.TagFloat), qtd_atual);
                exit;
            end
            else
            if ItemObject.Name = 'imgExcluir' then
            begin
                DtmPedido.ExcluirItem(Trunc(ItemObject.TagFloat));
                lvtItem.Items.Delete(ItemIndex);
                exit;
            end;

        finally
            CalcularTotalPedido;
        end;
    end;

    // Abre edicao do item...
    if NOT Assigned(FrmPedidoItemCad) then
        Application.CreateForm(TFrmPedidoItemCad, FrmPedidoItemCad);

    FrmPedidoItemCad.Modo := 'A';
    FrmPedidoItemCad.Cod_item_pedido:= lvtItem.Items[ItemIndex].Tag;
    FrmPedidoItemCad.ExecuteOnClose := ListarItens;
    FrmPedidoItemCad.Show;
end;

procedure TFrmPedidoCad.AbrirAba(rect: TRectangle);
begin
   rectAbaPedido.Fill.Color := $FFFFFFFF;
   rectAbaItem.Fill.Color   := $FFFFFFFF;
   lblAbaPedido.FontColor   := $FF8B8B8B;
   lblAbaItem.FontColor     := $FF8B8B8B;

   if (rect.Tag = 0) then begin
     rectAbaPedido.Fill.Color := $FF4162FF;
     lblAbaPedido.FontColor   := $FFFFFFFF;
   end else begin
     rectAbaItem.Fill.Color   := $FF4162FF;
     lblAbaItem.FontColor     := $FFFFFFFF;
   end;

   TabControl.GotoVisibleTab(rect.Tag);
end;

procedure TFrmPedidoCad.rectAbaItemClick(Sender: TObject);
begin
   AbrirAba(TRectangle(Sender));
end;

procedure TFrmPedidoCad.addProdutoListView(
    cod_item: integer;
    descricao: string;
    qtde, vlrUnit, vlrTotal: double;
    foto: TStream
);
var item: TListViewItem;
    txt : tListItemText;
    img : TListItemImage;
    bmp : TBitMap;
begin
   try
     item := lvtItem.Items.Add;
     item.height := 105;
     item.Tag := cod_item;

     // Descricao...
     txt := TListItemText(item.Objects.FindDrawable('txtDescricao'));
     txt.Text := descricao;

     // Qtd + Valor unit...
     txt := TListItemText(item.Objects.FindDrawable('txtUnitario'));
     txt.Text := FormatFloat('#,##0', qtde) + ' x ' + FormatFloat('R$#,##0.00', vlrUnit);
     txt.TagFloat := vlrUnit;

     // Valor total...
     txt := TListItemText(item.Objects.FindDrawable('txtTotal'));
     txt.Text := FormatFloat('R$#,##0.00', vlrTotal);

     // Qtd...
     txt := TListItemText(item.Objects.FindDrawable('txtQtde'));
     txt.Text := qtde.ToString;

     // Icone menos...
     img := TListItemImage(item.Objects.FindDrawable('imgMenos'));
     img.Bitmap := imgIconeMenos.Bitmap;
     img.TagFloat := cod_item;

     // Icone mais...
     img := TListItemImage(item.Objects.FindDrawable('imgMais'));
     img.Bitmap := imgIconeMais.Bitmap;
     img.TagFloat := cod_item;

     // Icone delete...
     img := TListItemImage(item.Objects.FindDrawable('imgExcluir'));
     img.Bitmap := imgIconeExcluir.Bitmap;
     img.TagFloat := cod_item;

     // Foto...
     img := TListItemImage(item.Objects.FindDrawable('imgFoto'));
     if foto <> nil then begin
        bmp := TBitmap.Create;
        bmp.LoadFromStream(foto);

        img.OwnsBitmap := true;
        img.Bitmap := bmp;
     end else begin
        img.Bitmap := imgIconeSemFoto.Bitmap;
     end;

     LayoutListviewProduto(item);

   except on ex: exception do
     begin
       fancy.Show(TIconDialog.Error, 'Aviso', 'Erro ao inserir produto na lista: '+ex.Message, 'OK');
     end;
   end;
end;

procedure TFrmPedidoCad.LayoutListviewProduto(AItem: TListViewItem);
var
    txt: TListItemText;
    img: TListItemImage;
    posicao_y: Extended;
begin
    {
    txt := TListItemText(AItem.Objects.FindDrawable('txtDescricao'));
    txt.Width := lvProduto.Width - 92;
    txt.Height := GetTextHeight(txt, txt.Width, txt.Text) + 3;

    posicao_y := txt.PlaceOffset.Y + txt.Height;


    TListItemText(AItem.Objects.FindDrawable('txtValor')).PlaceOffset.Y := posicao_y;
    TListItemText(AItem.Objects.FindDrawable('txtEstoque')).PlaceOffset.Y := posicao_y;
    TListItemImage(AItem.Objects.FindDrawable('imgValor')).PlaceOffset.Y := posicao_y;
    TListItemImage(AItem.Objects.FindDrawable('imgEstoque')).PlaceOffset.Y := posicao_y;


    AItem.Height := Trunc(posicao_y + 30);
    }
end;

end.
