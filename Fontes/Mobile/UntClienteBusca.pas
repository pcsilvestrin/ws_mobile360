unit UntClienteBusca;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.Objects,
  FMX.StdCtrls, FMX.Controls.Presentation, FMX.Edit, FMX.ListView.Types,
  FMX.ListView.Appearances, FMX.ListView.Adapters.Base, FMX.ListView, uFancyDialog;

type
  TExecuteOnClick = procedure(cod_clliente_local: integer; nome: string) of Object;

  TFrmClienteBusca = class(TForm)
    rectBusca: TRectangle;
    edtBuscarClientes: TEdit;
    btnBuscarClientes: TSpeedButton;
    rectToolbarProdutos: TRectangle;
    Label3: TLabel;
    btnAddProduto: TSpeedButton;
    btnVoltar: TSpeedButton;
    Image1: TImage;
    lvClientes: TListView;
    imgSemCliente: TImage;
    procedure btnVoltarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure lvClientesUpdateObjects(const Sender: TObject;
      const AItem: TListViewItem);
    procedure lvClientesPaint(Sender: TObject; Canvas: TCanvas;
      const ARect: TRectF);
    procedure FormShow(Sender: TObject);
    procedure btnBuscarClientesClick(Sender: TObject);
    procedure lvClientesItemClick(const Sender: TObject;
      const AItem: TListViewItem);
  private
    fancy : TFancyDialog;
    FExecuteOnClick: TExecuteOnClick;
    procedure addClienteListView(
                 cod_cliente_local: integer; nome, endereco, numero, bairro,
                 complemento, cidade, uf, cnpj_cpf: string);
    procedure LayoutListViewCliente(AItem: TListViewItem);
    procedure ListarClientes(pagina: integer; busca: string;
      ind_clear: boolean);
    procedure ThreadClientesTerminate(Sender: TObject);
    { Private declarations }
  public
    { Public declarations }
  published
    property ExecuteOnClick: TExecuteOnClick read FExecuteOnClick write FExecuteOnClick;
  end;

var
  FrmClienteBusca: TFrmClienteBusca;

implementation

{$R *.fmx}

uses UntPrincipal, uFunctions, DataModule.Cliente, uConstantes;

procedure TFrmClienteBusca.btnBuscarClientesClick(Sender: TObject);
begin
    ListarClientes(1, edtBuscarClientes.Text, true);
end;

procedure TFrmClienteBusca.btnVoltarClick(Sender: TObject);
begin
   close;
end;

procedure TFrmClienteBusca.FormClose(Sender: TObject; var Action: TCloseAction);
begin
   Action := TCloseAction.caFree;
   FrmClienteBusca := nil;
end;

procedure TFrmClienteBusca.FormCreate(Sender: TObject);
begin
   fancy := TFancyDialog.Create(FrmClienteBusca);
end;

procedure TFrmClienteBusca.FormDestroy(Sender: TObject);
begin
   fancy.DisposeOf;
end;

procedure TFrmClienteBusca.FormShow(Sender: TObject);
begin
    ListarClientes(1, '', true);
end;

procedure TFrmClienteBusca.ThreadClientesTerminate(Sender: TObject);
begin
   //N�o carregar mais Dados
   if (dtmCliente.qryCnsCliente.RecordCount < QTD_REG_PAGINA_CLIENTES) then lvClientes.Tag := -1;

   while not dtmCliente.qryCnsCliente.eof do begin
      addClienteListView(
         dtmCliente.qryCnsCliente.fieldByName('cod_cliente_local').AsInteger,
         dtmCliente.qryCnsCliente.fieldByName('nome').asString,
         dtmCliente.qryCnsCliente.fieldByName('endereco').asString,
         dtmCliente.qryCnsCliente.fieldByName('numero').asString,
         dtmCliente.qryCnsCliente.fieldByName('complemento').asString,
         dtmCliente.qryCnsCliente.fieldByName('bairro').asString,
         dtmCliente.qryCnsCliente.fieldByName('cidade').asString,
         dtmCliente.qryCnsCliente.fieldByName('uf').asString,
         dtmCliente.qryCnsCliente.fieldByName('cnpj_cpf').asString
      );
      dtmCliente.qryCnsCliente.next;
   end;

   lvClientes.EndUpdate;

   //Marca que o processo terminou
   lvClientes.TagString := '';

   //Caso n�o tenha registros, � apresentado a imagem ao usu�rio
   imgSemCliente.Visible := (lvClientes.items.Count = 0);

   //Se deu erro na Thread, � apresentado a mensagem
   if Sender is TThread then begin
      if Assigned(TThread(Sender).FatalException) then begin
         fancy.Show(TIconDialog.Error, 'Aviso', Exception(TThread(sender).FatalException).Message, 'OK');
         exit;
      end;
   end;
end;

procedure TFrmClienteBusca.ListarClientes(pagina: integer; busca: string; ind_clear: boolean);
var t : TThread;
begin
   imgSemCliente.Visible := false;

   //Evitar processamento concorrente ...
   if lvClientes.TagString = 'S' then exit;

   //Em processamento
   lvClientes.TagString := 'S';

   lvClientes.BeginUpdate;
   if ind_clear then begin
      pagina := 1;
      lvClientes.ScrollTo(0); //Volta para o primeiro item da lista
      lvClientes.Items.Clear;
   end;

   {
    Tag: Cont�m a pagina atual solicitada ao servidor ...
    >= 1 : Faz o request para buscar mais dados
     - 1 : indica que n�o tem mais dados
   }
   //Salva a pagina atual a ser exibida
   lvClientes.Tag := pagina;

   //Requisi��o por mais dados
   t := TThread.CreateAnonymousThread(procedure
   begin
      dtmCliente.ListarClientes(pagina, busca, '');
   end);

   t.OnTerminate := ThreadClientesTerminate;
   t.Start;
end;

procedure TFrmClienteBusca.addClienteListView(
                 cod_cliente_local: integer; nome, endereco, numero, bairro,
                 complemento, cidade, uf, cnpj_cpf: string);
var item: TListViewItem;
    txt : tListItemText;
    img : TListItemImage;
begin
   try
     item := lvClientes.Items.Add;
     item.Height := 90;
     item.TagString := nome;
     item.tag := cod_cliente_local;

     //Nome Cliente
     txt := tListItemText(item.Objects.FindDrawable('txtNome'));
     txt.Text := nome;

     //CNPJ / CPF
     txt := tListItemText(item.Objects.FindDrawable('txtCnpj'));
     txt.Text := 'CNPJ / CPF: '+cnpj_cpf;

     //Endere�o Completo ...
     txt := tListItemText(item.Objects.FindDrawable('txtEndereco'));
     txt.Text := endereco;

     if not numero.IsEmpty then begin
        txt.Text := txt.Text +', '+ numero;
     end;

     if not complemento.IsEmpty then begin
        txt.Text := txt.Text +', '+ complemento;
     end;

     if not bairro.IsEmpty then begin
        txt.Text := txt.Text +', '+ bairro;
     end;

     if not cidade.IsEmpty then begin
        txt.Text := txt.Text +', '+ cidade;
     end;

     if not uf.IsEmpty then begin
        txt.Text := txt.Text +', '+ uf;
     end;

     LayoutListViewCliente(item);

   except on ex: exception do
     begin
       fancy.Show(TIconDialog.Error, 'Aviso', 'Erro ao inserir clientes na lista: '+ex.Message, 'OK');
     end;
   end;
end;

procedure TFrmClienteBusca.LayoutListViewCliente(AItem: TListViewItem);
var txt: TListItemText;
begin
   txt := TListItemText(AItem.Objects.FindDrawable('txtEndereco'));
   txt.Width := lvClientes.Width - txt.PlaceOffset.X - txt.PlaceOffset.Y; //X e Y S�o as bordas do Componente
   txt.Height:= GetTextHeight(txt, txt.width, txt.text) + 5; // 5 - Valor de seguran�a para n�o cortar o Texto do Componente

   AItem.Height := Trunc(txt.PlaceOffset.Y + txt.Height);
end;


procedure TFrmClienteBusca.lvClientesItemClick(const Sender: TObject;
  const AItem: TListViewItem);
begin
   if Assigned(ExecuteOnClick) then begin
      ExecuteOnClick(AItem.Tag, AItem.TagString);
   end;
   close;
end;

procedure TFrmClienteBusca.lvClientesPaint(Sender: TObject; Canvas: TCanvas;
  const ARect: TRectF);
begin
   //Verifica se a Rolagem atingiu o limite para uma nova Carga
   if (lvClientes.Items.Count >= QTD_REG_PAGINA_CLIENTES) and (lvClientes.Tag >= 0) then begin

      //Monitora o 10� Item, quando a parte de baixo do elemento entrar na ListView, � executado uma nova consulta
      if (lvClientes.GetItemRect(lvClientes.Items.Count - 5).Bottom <= lvClientes.Height) then begin
         ListarClientes(lvClientes.Tag + 1, edtBuscarClientes.Text, false);
      end;

   end;
end;

procedure TFrmClienteBusca.lvClientesUpdateObjects(const Sender: TObject;
  const AItem: TListViewItem);
begin
   LayoutListViewCliente(AItem);
end;

end.
