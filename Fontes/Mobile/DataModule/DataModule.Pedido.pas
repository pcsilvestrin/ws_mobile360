unit DataModule.Pedido;

interface

uses
  System.SysUtils, System.Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Data.DB,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client;

type
  TdtmPedido = class(TDataModule)
    qryCnsPedido: TFDQuery;
    qryPedido: TFDQuery;
    qryCnsItem: TFDQuery;
    qryCnsItemTemp: TFDQuery;
    qryItem: TFDQuery;
    qryConsCondPagto: TFDQuery;
    TabCondPagto: TFDMemTable;
  private
    procedure SalvarItensPedido(cod_pedido_local: integer);
    { Private declarations }
  public
    { Public declarations }
    procedure ListarPedidoId(cod_Pedido: integer);
    procedure ListarPedidos(pagina: integer; busca: string);
    procedure ListarPedidoItemId(cod_item: integer);
    procedure CarregarTabelaTemp(cod_pedido_local: integer);
    procedure AtuailzarQtdeItem(cod_item: integer; qtde: double);
    procedure ExcluirItem(cod_item: integer);

    procedure IncluirItem(cod_produto_local: integer; qtd, valor_unitario, valor_total: double);
    procedure EditarItem(cod_item, cod_produto_local: integer; qtd,
      valor_unitario, valor_total: double);
    procedure ListarCondPagto;

    procedure EditarPedido(cod_pedido_local, cod_cliente_local,
      cod_cond_pagto: integer; data_pedido, data_entrega: TDate; contato, obs,
      prazo_entrega, ind_sincronizar: string; valor_total: double);
    procedure InserirPedido(cod_cliente_local, cod_usuario,
      cod_cond_pagto: integer; data_pedido, data_entrega: TDate; contato, obs,
      prazo_entrega, ind_sincronizar: string; valor_total: double;
      cod_pedido_oficial: integer);
    procedure ExcluirPedido(cod_pedido_local: integer);

    procedure ListarCondPagtoWS;
    procedure ExcluirCondPagamento;
    procedure InserirEditarCondPagamento(cod_condPagto: integer; cond_pagto: string);
  end;

var
  dtmPedido: TdtmPedido;

implementation

{%CLASSGROUP 'FMX.Controls.TControl'}

uses DataModule.Global, System.Variants, uConstantes,
  RESTRequest4D.Response.Contract, System.JSON, RESTRequest4D, uSession;

{$R *.dfm}

{ TdtmPedido }

procedure TdtmPedido.ListarPedidos(pagina: integer; busca: string);
begin
   qryCnsPedido.Close;
   qryCnsPedido.SQL.Clear;
   qryCnsPedido.SQL.Add('SELECT PEDIDO.*, CLIENTE.NOME     ');
   qryCnsPedido.SQL.Add('FROM TAB_PEDIDO PEDIDO            ');
   qryCnsPedido.SQL.Add('  JOIN TAB_CLIENTE CLIENTE ON CLIENTE.COD_CLIENTE_LOCAL = PEDIDO.COD_CLIENTE_LOCAL ');
   qryCnsPedido.SQL.Add('WHERE pedido.cod_pedido_local > 0 ');

   if (trim(busca) <> emptyStr) then begin
      qryCnsPedido.SQL.Add(' AND (CLIENTE.NOME LIKE :NOME OR PEDIDO.COD_PEDIDO_LOCAL = :COD_PEDIDO_LOCAL ');
      qryCnsPedido.SQL.Add(' OR PEDIDO.COD_PEDIDO_OFICIAL = :COD_PEDIDO_OFICIAL)');

      qryCnsPedido.ParamByName('NOME').Value := '%'+busca+'%';

      try
        qryCnsPedido.ParamByName('COD_PEDIDO_LOCAL').Value   := StrToInt(busca);
      except
        qryCnsPedido.ParamByName('COD_PEDIDO_LOCAL').Value   := 0;
      end;


      try
        qryCnsPedido.ParamByName('COD_PEDIDO_OFICIAL').Value   := StrToInt(busca);
      except
        qryCnsPedido.ParamByName('COD_PEDIDO_OFICIAL').Value   := 0;
      end;
   end;

   qryCnsPedido.SQL.Add('ORDER BY PEDIDO.COD_PEDIDO_LOCAL DESC ');

   if (pagina > 0) then begin
      qryCnsPedido.SQL.Add('LIMIT :PAGINA, :QTD_REG');
      qryCnsPedido.ParamByName('PAGINA').Value := (pagina - 1) * QTD_REG_PAGINA_PEDIDOS;
      qryCnsPedido.ParamByName('QTD_REG').Value:= QTD_REG_PAGINA_PEDIDOS;
   end;

   qryCnsPedido.Open;
end;

procedure TdtmPedido.ListarPedidoId(cod_Pedido: integer);
begin
   qryPedido.Active := false;
   qryPedido.SQL.Clear;
   qryPedido.SQL.Add('SELECT PEDIDO.*, CLIENTE.NOME, COND.COND_PAGTO ');
   qryPedido.SQL.Add('FROM TAB_PEDIDO PEDIDO   ');
   qryPedido.SQL.Add('  JOIN TAB_CLIENTE CLIENTE ON CLIENTE.COD_CLIENTE_LOCAL = PEDIDO.COD_CLIENTE_LOCAL ');
   qryPedido.SQL.Add('  JOIN TAB_COND_PAGTO COND ON COND.COD_COND_PAGTO = PEDIDO.COD_COND_PAGTO ');
   qryPedido.SQL.Add('WHERE PEDIDO.COD_PEDIDO_LOCAL = :COD_PEDIDO_LOCAL ');
   qryPedido.ParamByName('COD_PEDIDO_LOCAL').Value := cod_pedido;
   qryPedido.Active := true;
end;

procedure TdtmPedido.ListarPedidoItemId(cod_item: integer);
begin
   qryCnsItem.Active := false;
   qryCnsItem.SQL.Clear;
   qryCnsItem.SQL.Add('SELECT ITEM_TEMP.*, PRODUTO.FOTO, PRODUTO.DESCRICAO');
   qryCnsItem.SQL.Add('FROM TAB_PEDIDO_ITEM_TEMP ITEM_TEMP   ');
   qryCnsItem.SQL.Add('  JOIN TAB_PRODUTO PRODUTO ON PRODUTO.COD_PRODUTO_LOCAL = ITEM_TEMP.COD_PRODUTO_LOCAL ');
   qryCnsItem.SQL.Add('WHERE ITEM_TEMP.COD_ITEM > 0  ');

   if (cod_item > 0) then begin
      qryCnsItem.SQL.Add(' AND ITEM_TEMP.COD_ITEM = :COD_ITEM');
      qryCnsItem.ParamByName('COD_ITEM').Value := cod_item;
   end;

   qryCnsItem.SQL.Add('ORDER BY ITEM_TEMP.COD_ITEM DESC');
   qryCnsItem.Active := true;
end;

procedure TDtmPedido.CarregarTabelaTemp(cod_pedido_local: integer);
begin
    qryCnsItemTemp.Active := false;
    qryCnsItemTemp.SQL.Clear;
    qryCnsItemTemp.SQL.Add('delete from tab_pedido_item_temp');
    qryCnsItemTemp.ExecSQL;

    qryCnsItemTemp.Active := false;
    qryCnsItemTemp.SQL.Clear;
    qryCnsItemTemp.SQL.Add('insert into tab_pedido_item_temp (cod_produto_local, qtd, valor_unitario, valor_total)');
    qryCnsItemTemp.SQL.Add('select cod_produto_local, qtd, valor_unitario, valor_total');
    qryCnsItemTemp.SQL.Add('from tab_pedido_item');
    qryCnsItemTemp.SQL.Add('where cod_pedido_local = :cod_pedido_local');
    qryCnsItemTemp.ParamByName('cod_pedido_local').Value := cod_pedido_local;
    qryCnsItemTemp.ExecSQL;
end;

procedure TDtmPedido.AtuailzarQtdeItem(cod_item: integer; qtde: double);
begin
    qryCnsItemTemp.Active := false;
    qryCnsItemTemp.SQL.Clear;
    qryCnsItemTemp.SQL.Add('update tab_pedido_item_temp');
    qryCnsItemTemp.SQL.Add('set qtd = qtd + :qtd, ');
    qryCnsItemTemp.SQL.Add('    valor_total = (qtd + :qtd) * valor_unitario');
    qryCnsItemTemp.SQL.Add('where cod_item = :cod_item');
    qryCnsItemTemp.ParamByName('qtd').Value := qtde;
    qryCnsItemTemp.ParamByName('cod_item').Value := cod_item;
    qryCnsItemTemp.ExecSQL;
end;

procedure TDtmPedido.ExcluirItem(cod_item: integer);
begin
    qryItem.Active := false;
    qryItem.SQL.Clear;
    qryItem.SQL.Add('delete from tab_pedido_item_temp');

    if cod_item > 0 then
    begin
        qryItem.SQL.Add('where cod_item = :cod_item');
        qryItem.ParamByName('cod_item').Value := cod_item;
    end;

    qryItem.ExecSQL;
end;

procedure TDtmPedido.IncluirItem(cod_produto_local: integer; qtd, valor_unitario, valor_total: double);
begin
    qryItem.Active := false;
    qryItem.SQL.Clear;
    qryItem.SQL.Add('insert into tab_pedido_item_temp(cod_produto_local, qtd, valor_unitario, valor_total)');
    qryItem.SQL.Add('values(:cod_produto_local, :qtd, :valor_unitario, :valor_total)');
    qryItem.ParamByName('cod_produto_local').Value := cod_produto_local;
    qryItem.ParamByName('qtd').Value := qtd;
    qryItem.ParamByName('valor_unitario').Value := valor_unitario;
    qryItem.ParamByName('valor_total').Value := valor_total;
    qryItem.ExecSQL;
end;

procedure TDtmPedido.EditarItem(cod_item: integer;
                               cod_produto_local: integer;
                               qtd, valor_unitario, valor_total: double);
begin
    qryItem.Active := false;
    qryItem.SQL.Clear;
    qryItem.SQL.Add('update tab_pedido_item_temp set cod_produto_local=:cod_produto_local, ');
    qryItem.SQL.Add('qtd=:qtd, valor_unitario=:valor_unitario, valor_total=:valor_total');
    qryItem.SQL.Add('where cod_item = :cod_item');
    qryItem.ParamByName('cod_produto_local').Value := cod_produto_local;
    qryItem.ParamByName('qtd').Value := qtd;
    qryItem.ParamByName('valor_unitario').Value := valor_unitario;
    qryItem.ParamByName('valor_total').Value := valor_total;
    qryItem.ParamByName('cod_item').Value := cod_item;
    qryItem.ExecSQL;
end;

procedure TDtmPedido.ListarCondPagto;
begin
    qryConsCondPagto.Active := false;
    qryConsCondPagto.SQL.Clear;
    qryConsCondPagto.SQL.Add('select *');
    qryConsCondPagto.SQL.Add('from tab_cond_pagto ');
    qryConsCondPagto.SQL.Add('where ind_excluido = ''N'' ');
    qryConsCondPagto.SQL.Add('order by cond_pagto');
    qryConsCondPagto.Active := true;
end;

procedure TDtmPedido.SalvarItensPedido(cod_pedido_local: integer);
begin
    qryItem.Active := false;
    qryItem.SQL.Clear;
    qryItem.SQL.Add('delete from tab_pedido_item');
    qryItem.SQL.Add('where cod_pedido_local = :cod_pedido_local');
    qryItem.ParamByName('cod_pedido_local').Value := cod_pedido_local;
    qryItem.ExecSQL;

    qryItem.Active := false;
    qryItem.SQL.Clear;
    qryItem.SQL.Add('insert into tab_pedido_item (cod_pedido_local, cod_produto_local, qtd, valor_unitario, valor_total)');
    qryItem.SQL.Add('select :cod_pedido_local, cod_produto_local, qtd, valor_unitario, valor_total');
    qryItem.SQL.Add('from tab_pedido_item_temp');
    qryItem.ParamByName('cod_pedido_local').Value := cod_pedido_local;
    qryItem.ExecSQL;
end;

procedure TDtmPedido.InserirPedido(cod_cliente_local, cod_usuario, cod_cond_pagto: integer;
                                  data_pedido, data_entrega: TDate;
                                  contato, obs, prazo_entrega, ind_sincronizar: string;
                                  valor_total: double;
                                  cod_pedido_oficial: integer);
begin
    try
        dtmGlobal.connection.StartTransaction;

        qryPedido.Active := false;
        qryPedido.SQL.Clear;
        qryPedido.SQL.Add('insert into tab_pedido(cod_cliente_local, cod_usuario, tipo_pedido, data_pedido,');
        qryPedido.SQL.Add('contato, obs, ind_sincronizar, valor_total, cod_cond_pagto, prazo_entrega, ');
        qryPedido.SQL.Add('data_entrega, cod_pedido_oficial)');
        qryPedido.SQL.Add('values(:cod_cliente_local, :cod_usuario, :tipo_pedido, :data_pedido,');
        qryPedido.SQL.Add(':contato, :obs, :ind_sincronizar, :valor_total, :cod_cond_pagto, :prazo_entrega, ');
        qryPedido.SQL.Add(':data_entrega, :cod_pedido_oficial);');
        qryPedido.SQL.Add('select last_insert_rowid() as cod_pedido_local'); //Pega o id do ultimo pedido gravado

        qryPedido.ParamByName('cod_cliente_local').Value := cod_cliente_local;
        qryPedido.ParamByName('cod_usuario').Value := cod_usuario;
        qryPedido.ParamByName('tipo_pedido').Value := 'O';
        qryPedido.ParamByName('data_pedido').Value := FormatDateTime('yyyy-mm-dd', data_pedido);
        qryPedido.ParamByName('contato').Value := contato;
        qryPedido.ParamByName('obs').Value := obs;
        qryPedido.ParamByName('ind_sincronizar').Value := ind_sincronizar;
        qryPedido.ParamByName('valor_total').Value := valor_total;
        qryPedido.ParamByName('cod_cond_pagto').Value := cod_cond_pagto;
        qryPedido.ParamByName('prazo_entrega').Value := prazo_entrega;
        qryPedido.ParamByName('cod_pedido_oficial').Value := cod_pedido_oficial;

        if data_entrega = 0 then
        begin
            qryPedido.ParamByName('data_entrega').DataType := ftString;
            qryPedido.ParamByName('data_entrega').Value    := UnAssigned;
        end
        else
            qryPedido.ParamByName('data_entrega').Value := FormatDateTime('yyyy-mm-dd', data_entrega);

        qryPedido.Active := true;


        // Itens...
        SalvarItensPedido(qryPedido.fieldbyname('cod_pedido_local').AsInteger);

        dtmGlobal.connection.Commit;

    except on ex:exception do
        begin
            dtmGlobal.connection.Rollback;
            raise Exception.Create('Erro ao inserir pedido: ' + ex.Message);
        end;
    end;
end;

procedure TDtmPedido.EditarPedido(cod_pedido_local, cod_cliente_local, cod_cond_pagto: integer;
                                 data_pedido, data_entrega: TDate;
                                 contato, obs, prazo_entrega, ind_sincronizar: string;
                                 valor_total: double);
begin
    try
        dtmGlobal.connection.StartTransaction;

        qryPedido.Active := false;
        qryPedido.SQL.Clear;
        qryPedido.SQL.Add('update tab_pedido set cod_cliente_local=:cod_cliente_local, data_pedido=:data_pedido,');
        qryPedido.SQL.Add('contato=:contato, obs=:obs, ind_sincronizar=:ind_sincronizar, valor_total=:valor_total,');
        qryPedido.SQL.Add('cod_cond_pagto=:cod_cond_pagto, prazo_entrega=:prazo_entrega, data_entrega=:data_entrega');
        qryPedido.SQL.Add('where cod_pedido_local=:cod_pedido_local');

        qryPedido.ParamByName('cod_cliente_local').Value := cod_cliente_local;
        qryPedido.ParamByName('data_pedido').Value := FormatDateTime('yyyy-mm-dd', data_pedido);
        qryPedido.ParamByName('contato').Value := contato;
        qryPedido.ParamByName('obs').Value := obs;
        qryPedido.ParamByName('ind_sincronizar').Value := ind_sincronizar;
        qryPedido.ParamByName('valor_total').Value := valor_total;
        qryPedido.ParamByName('cod_cond_pagto').Value := cod_cond_pagto;
        qryPedido.ParamByName('prazo_entrega').Value := prazo_entrega;

        if data_entrega = 0 then
        begin
            qryPedido.ParamByName('data_entrega').DataType := ftString;
            qryPedido.ParamByName('data_entrega').Value := UnAssigned;
        end
        else
            qryPedido.ParamByName('data_entrega').Value := FormatDateTime('yyyy-mm-dd', data_entrega);

        qryPedido.ParamByName('cod_pedido_local').Value := cod_pedido_local;

        qryPedido.ExecSQL;

        // Itens...
        SalvarItensPedido(cod_pedido_local);

        dtmGlobal.connection.Commit;

    except on ex:exception do
        begin
            dtmGlobal.connection.Rollback;
            raise Exception.Create('Erro ao editar pedido: ' + ex.Message);
        end;
    end;
end;

procedure TDtmPedido.ExcluirPedido(cod_pedido_local: integer);
begin
    try
        dtmGlobal.connection.StartTransaction;

        qryItem.Active := false;
        qryItem.SQL.Clear;
        qryItem.SQL.Add('delete from tab_pedido_item');
        qryItem.SQL.Add('where cod_pedido_local = :cod_pedido_local');
        qryItem.ParamByName('cod_pedido_local').Value := cod_pedido_local;
        qryItem.ExecSQL;

        qryItem.Active := false;
        qryItem.SQL.Clear;
        qryItem.SQL.Add('delete from tab_pedido');
        qryItem.SQL.Add('where cod_pedido_local = :cod_pedido_local');
        qryItem.ParamByName('cod_pedido_local').Value := cod_pedido_local;
        qryItem.ExecSQL;

        dtmGlobal.connection.Commit;
    except on ex:exception do
        begin
            dtmGlobal.connection.Rollback;
            raise Exception.Create('Erro ao excluir pedido: ' + ex.Message);
        end;
    end;
end;

procedure TDtmPedido.ExcluirCondPagamento;
var qryCondPagto : TFDQuery;
begin
   try
     qryCondPagto  := TFDQuery.Create(nil);
     qryCondPagto.Connection := dtmGlobal.connection;

     qryCondPagto.Active := false;
     qryCondPagto.SQL.Clear;
     qryCondPagto.SQL.Add('UPDATE TAB_COND_PAGTO SET IND_EXCLUIDO = ''S'' ');
     qryCondPagto.ExecSQL();

   finally
     qryCondPagto.close;
     freeAndNil(qryCondPagto);
   end;
end;

procedure TDtmPedido.InserirEditarCondPagamento(cod_condPagto: integer; cond_pagto: string);
var qryCondPagto : TFDQuery;
begin
   try
     qryCondPagto  := TFDQuery.Create(nil);
     qryCondPagto.Connection := dtmGlobal.connection;

     qryCondPagto.Active := false;
     qryCondPagto.SQL.Clear;
     qryCondPagto.SQL.Add('INSERT OR REPLACE INTO TAB_COND_PAGTO (COD_COND_PAGTO, COND_PAGTO, IND_EXCLUIDO) ');
     qryCondPagto.SQL.Add('VALUES (:COD_COND_PAGTO, :COND_PAGTO, :IND_EXCLUIDO) ');

     qryCondPagto.ParamByName('COD_COND_PAGTO').Value  := cod_condPagto;
     qryCondPagto.ParamByName('COND_PAGTO').Value      := cond_pagto;
     qryCondPagto.ParamByName('IND_EXCLUIDO').Value    := 'N';

     qryCondPagto.ExecSQL();
   finally
     qryCondPagto.close;
     freeAndNil(qryCondPagto);
   end;
end;

procedure TDtmPedido.ListarCondPagtoWS;
var resp: IResponse;
begin
   try
     //Limpa os campos da tabela, para que possa ser populada novamente
     TabCondPagto.FieldDefs.Clear;

     resp := TRequest.New.BaseURL(BASE_URL)
              .Resource('cond-pagto')
              .TokenBearer(TSession.TOKEN_JWT) //Passa o Token na Requisi��o para Autentica��o
              .Accept('application/json')
              .DataSetAdapter(TabCondPagto) //As informa��es retornadas pelo Servidor s�o inseridas neste Objeto
              .Get;

     if resp.StatusCode <> 200 then begin
        raise Exception.Create(resp.Content);
     end;
   finally

   end;
end;

end.
