object dtmProduto: TdtmProduto
  Height = 413
  Width = 402
  object qryCnsProduto: TFDQuery
    Connection = dtmGlobal.connection
    Left = 104
    Top = 56
  end
  object qryProduto: TFDQuery
    Connection = dtmGlobal.connection
    Left = 224
    Top = 56
  end
  object TabProduto: TFDMemTable
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    Left = 112
    Top = 152
  end
  object qryProdutoSinc: TFDQuery
    Connection = dtmGlobal.connection
    Left = 216
    Top = 152
  end
  object qryFoto: TFDQuery
    Connection = dtmGlobal.connection
    Left = 224
    Top = 224
  end
end
