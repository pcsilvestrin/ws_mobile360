unit DataModule.Usuario;

interface

uses
  System.SysUtils, System.Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Data.DB,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, RESTRequest4D, System.JSON,
  uSession;

type
  TdtmUsuario = class(TDataModule)
    qryUsuario: TFDQuery;
    qryCnsUsuario: TFDQuery;
    TabUsuario: TFDMemTable;
    qryConfig: TFDQuery;
  private
    { Private declarations }
  public
    { Public declarations }
    procedure Logout;
    procedure DesativarOnboarding;
    procedure EditarUsuario(nome, email: String);
    procedure ListarUsuarios;
    procedure InserirUsuario(cod_usuario: integer; nome, email, senha, token_jwt: string);
    procedure ExcluirUsuario(cod_usuario: integer);
    procedure AlterarSenha(senha: String);

    procedure LoginWS(email, senha: string);
    procedure NovaContaWS(nome, email, senha: string);
    procedure AlteraDadosContaWS(nome, email: string);
    procedure AlterarSenhaWS(senha: string);
    function ObterDataServidorWS: string;

    function BuscarConfig(campo: String): string;
    procedure SalvarConfig(campo, valor: String);
  end;

var
  dtmUsuario: TdtmUsuario;

const
  QTDE_REG_PAGINA = 15;

implementation

{%CLASSGROUP 'FMX.Controls.TControl'}

uses uConstantes;

{$R *.dfm}

{ TdmUsuario }

procedure TdtmUsuario.Logout;
begin
   qryUsuario.Active := false;
   qryUsuario.SQL.Clear;
   qryUsuario.SQL.Add('UPDATE TAB_USUARIO             ');
   qryUsuario.SQL.Add('   SET IND_LOGIN = :IND_LOGIN, ');
   qryUsuario.SQL.Add('       IND_ONBOARDING = :IND_ONBOARDING');

   qryUsuario.ParamByName('IND_LOGIN').Value := 'N';
   qryUsuario.ParamByName('IND_ONBOARDING').Value := 'N';  //Desabilita as informa��es iniciais
   qryUsuario.ExecSQL;

   qryUsuario.Active := false;
   qryUsuario.SQL.Clear;
   qryUsuario.SQL.Add('DELETE FROM TAB_PEDIDO_ITEM ');
   qryUsuario.ExecSQL;

   qryUsuario.Active := false;
   qryUsuario.SQL.Clear;
   qryUsuario.SQL.Add('DELETE FROM TAB_PEDIDO ');
   qryUsuario.ExecSQL;

   qryUsuario.Active := false;
   qryUsuario.SQL.Clear;
   qryUsuario.SQL.Add('DELETE FROM TAB_NOTIFICACAO ');
   qryUsuario.ExecSQL;

   qryUsuario.Active := false;
   qryUsuario.SQL.Clear;
   qryUsuario.SQL.Add('DELETE FROM TAB_PRODUTO ');
   qryUsuario.ExecSQL;

   qryUsuario.Active := false;
   qryUsuario.SQL.Clear;
   qryUsuario.SQL.Add('DELETE FROM TAB_CLIENTE ');
   qryUsuario.ExecSQL;

   qryUsuario.Active := false;
   qryUsuario.SQL.Clear;
   qryUsuario.SQL.Add('DELETE FROM TAB_COND_PAGTO ');
   qryUsuario.ExecSQL;

   qryUsuario.Active := false;
   qryUsuario.SQL.Clear;
   qryUsuario.SQL.Add('DELETE FROM TAB_CONFIG ');
   qryUsuario.ExecSQL;
end;

procedure TdtmUsuario.AlterarSenha(senha: String);
begin
   qryUsuario.Active := false;
   qryUsuario.SQL.Clear;
   qryUsuario.SQL.Add('UPDATE TAB_USUARIO    ');
   qryUsuario.SQL.Add('   SET SENHA = :SENHA ');

   qryUsuario.ParamByName('SENHA').Value := senha;
   qryUsuario.ExecSQL;
end;

procedure TdtmUsuario.DesativarOnboarding;
begin
   qryUsuario.Active := false;
   qryUsuario.SQL.Clear;
   qryUsuario.SQL.Add('UPDATE TAB_USUARIO    ');
   qryUsuario.SQL.Add('   SET IND_ONBOARDING = :IND_ONBOARDING ');

   qryUsuario.ParamByName('IND_ONBOARDING').Value := 'N';
   qryUsuario.ExecSQL;
end;

procedure TdtmUsuario.ListarUsuarios;
begin
   qryCnsUsuario.Active := false;
   qryCnsUsuario.SQL.Clear;
   qryCnsUsuario.SQL.Add('SELECT U.* FROM TAB_USUARIO U ');
   qryCnsUsuario.Active := true;
end;

procedure TdtmUsuario.InserirUsuario(cod_usuario: integer; nome, email, senha, token_jwt: string);
begin
   qryUsuario.Active := false;
   qryUsuario.SQL.Clear;
   qryUsuario.SQL.Add('INSERT INTO TAB_USUARIO (COD_USUARIO, NOME, EMAIL, SENHA, TOKEN_JWT, IND_LOGIN, IND_ONBOARDING) ');
   qryUsuario.SQL.Add('VALUES (:COD_USUARIO, :NOME, :EMAIL, :SENHA, :TOKEN_JWT, :IND_LOGIN, :IND_ONBOARDING) ');
   qryUsuario.ParamByName('COD_USUARIO').Value    := cod_usuario;
   qryUsuario.ParamByName('NOME').Value           := nome;
   qryUsuario.ParamByName('EMAIL').Value          := email;
   qryUsuario.ParamByName('SENHA').Value          := senha;
   qryUsuario.ParamByName('TOKEN_JWT').Value      := token_jwt;
   qryUsuario.ParamByName('IND_LOGIN').Value      := 'S';
   qryUsuario.ParamByName('IND_ONBOARDING').Value := 'N';
   qryUsuario.ExecSQL;
end;

procedure TdtmUsuario.ExcluirUsuario(cod_usuario: integer);
begin
   qryUsuario.Active := false;
   qryUsuario.SQL.Clear;
   qryUsuario.SQL.Add('DELETE FROM TAB_USUARIO WHERE COD_USUARIO = :COD_USUARIO ');
   qryUsuario.ParamByName('COD_USUARIO').Value := cod_usuario;
   qryUsuario.ExecSQL;
end;

procedure TdtmUsuario.EditarUsuario(nome, email: String);
begin
   qryUsuario.Active := false;
   qryUsuario.SQL.Clear;
   qryUsuario.SQL.Add('UPDATE TAB_USUARIO      ');
   qryUsuario.SQL.Add('   SET NOME  = :NOME,   ');
   qryUsuario.SQL.Add('       EMAIL = :EMAIL   ');

   qryUsuario.ParamByName('NOME').Value   := nome;
   qryUsuario.ParamByName('EMAIL').Value  := email;
   qryUsuario.ExecSQL;
end;

procedure TdtmUsuario.LoginWS(email, senha: string);
var resp: IResponse;
    json: TJSONObject;
begin
   try
     //Limpa os campos da tabela, para que possa ser populada novamente
     TabUsuario.FieldDefs.Clear;

     json := TJSONObject.Create;
     json.AddPair('email', email);
     json.AddPair('senha', senha);

     resp := TRequest.New.BaseURL(BASE_URL)
              .Resource('usuarios/login')
              .AddBody(json.ToJSON())
              .Accept('application/json')
              .DataSetAdapter(TabUsuario) //As informa��es retornadas pelo Servidor s�o inseridas neste Objeto
              .Post;

     if resp.StatusCode <> 200 then begin
        raise Exception.Create(resp.Content);
     end;
   finally
     json.DisposeOf;
   end;
end;

procedure TdtmUsuario.NovaContaWS(nome, email, senha: string);
var resp: IResponse;
    json: TJSONObject;
begin
   try
     //Limpa os campos da tabela, para que possa ser populada novamente
     TabUsuario.FieldDefs.Clear;

     json := TJSONObject.Create;
     json.AddPair('nome', nome);
     json.AddPair('email', email);
     json.AddPair('senha', senha);

     resp := TRequest.New.BaseURL(BASE_URL)
              .Resource('usuarios')
              .AddBody(json.ToJSON())
              .Accept('application/json')
              .DataSetAdapter(TabUsuario) //As informa��es retornadas pelo Servidor s�o inseridas neste Objeto
              .Post;

     if resp.StatusCode <> 201 then begin
        raise Exception.Create(resp.Content);
     end;
   finally
     json.DisposeOf;
   end;
end;

procedure TdtmUsuario.AlteraDadosContaWS(nome, email: string);
var resp: IResponse;
    json: TJSONObject;
begin
   try
     //Limpa os campos da tabela, para que possa ser populada novamente
     TabUsuario.FieldDefs.Clear;

     json := TJSONObject.Create;
     json.AddPair('nome', nome);
     json.AddPair('email', email);

     resp := TRequest.New.BaseURL(BASE_URL)
              .Resource('usuarios')
              .TokenBearer(TSession.TOKEN_JWT) //Passa o Token na Requisi��o para Autentica��o
              .AddBody(json.ToJSON())
              .Accept('application/json')
              .DataSetAdapter(TabUsuario) //As informa��es retornadas pelo Servidor s�o inseridas neste Objeto
              .Put;

     if resp.StatusCode <> 200 then begin
        raise Exception.Create(resp.Content);
     end;
   finally
     json.DisposeOf;
   end;
end;

procedure TdtmUsuario.AlterarSenhaWS(senha: string);
var resp: IResponse;
    json: TJSONObject;
begin
   try
     //Limpa os campos da tabela, para que possa ser populada novamente
     TabUsuario.FieldDefs.Clear;

     json := TJSONObject.Create;
     json.AddPair('senha', senha);

     resp := TRequest.New.BaseURL(BASE_URL)
              .Resource('usuarios/senha')
              .TokenBearer(TSession.TOKEN_JWT) //Passa o Token na Requisi��o para Autentica��o
              .AddBody(json.ToJSON())
              .Accept('application/json')
              .Put;

     if resp.StatusCode <> 200 then begin
        raise Exception.Create(resp.Content);
     end;
   finally
     json.DisposeOf;
   end;
end;

procedure TdtmUsuario.SalvarConfig(campo, valor: String);
begin
   qryConfig.Active := false;
   qryConfig.SQL.Clear;
   qryConfig.SQL.Add('insert or replace into tab_config (campo, valor) values (:campo, :valor) ');
   qryConfig.ParamByName('campo').Value := campo;
   qryConfig.ParamByName('valor').Value := valor;
   qryConfig.ExecSQL;
end;

function TdtmUsuario.BuscarConfig(campo: String): string;
begin
   qryConfig.Active := false;
   qryConfig.SQL.Clear;
   qryConfig.SQL.Add('select valor   ');
   qryConfig.SQL.Add('from tab_config ');
   qryConfig.SQL.Add('where campo = :campo ');
   qryConfig.ParamByName('campo').Value := campo;
   qryConfig.Active := true;

   result := qryConfig.fieldByName('valor').asString;
end;

function TdtmUsuario.ObterDataServidorWS: string;
var resp: IResponse;
begin
   try
     resp := TRequest.New.BaseURL(BASE_URL)
              .Resource('usuarios/horario')
              .TokenBearer(TSession.TOKEN_JWT) //Passa o Token na Requisi��o para Autentica��o
              .Accept('application/json')
              .Get;

     if resp.StatusCode <> 200 then begin
        raise Exception.Create(resp.Content);
     end else begin
        //Este m�todo j� retorna a data em formato de texto
        result := resp.Content;  //2022-11-11 17:20:00
     end;
   finally
   end;
end;

end.
