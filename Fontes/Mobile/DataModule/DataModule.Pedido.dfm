object dtmPedido: TdtmPedido
  Height = 480
  Width = 640
  object qryCnsPedido: TFDQuery
    Connection = dtmGlobal.connection
    Left = 104
    Top = 56
  end
  object qryPedido: TFDQuery
    Connection = dtmGlobal.connection
    Left = 208
    Top = 56
  end
  object qryCnsItem: TFDQuery
    Connection = dtmGlobal.connection
    Left = 288
    Top = 56
  end
  object qryCnsItemTemp: TFDQuery
    Connection = dtmGlobal.connection
    Left = 368
    Top = 56
  end
  object qryItem: TFDQuery
    Connection = dtmGlobal.connection
    Left = 104
    Top = 128
  end
  object qryConsCondPagto: TFDQuery
    Connection = dtmGlobal.connection
    Left = 208
    Top = 128
  end
  object TabCondPagto: TFDMemTable
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    Left = 144
    Top = 224
  end
end
