unit DataModule.Notificacao;

interface

uses
  System.SysUtils, System.Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Data.DB,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, uSession;

type
  TdtmNotificacao = class(TDataModule)
    qryCnsNotificacao: TFDQuery;
    qryNotificacao: TFDQuery;
    TabNotificacoes: TFDMemTable;
  private
    { Private declarations }
  public
    { Public declarations }
    procedure InserirNotificacao(cod_notificacao: integer; dt_notificacao, titulo, texto: string);
    procedure MarcarNotificacaoLida(cod_notificacao: integer; bLida: boolean);
    procedure ExcluirNotificacao(cod_notificacao: integer);
    procedure ListarNotificacoes(pagina: integer);

    procedure ListarNotificacoesWS;
  end;

var
  dtmNotificacao: TdtmNotificacao;

implementation

{%CLASSGROUP 'FMX.Controls.TControl'}

uses uConstantes, RESTRequest4D, System.JSON, DataModule.Global;

{$R *.dfm}

procedure TdtmNotificacao.ListarNotificacoes(pagina: integer);
begin
   qryCnsNotificacao.Close;
   qryCnsNotificacao.SQL.Clear;
   qryCnsNotificacao.SQL.Add('SELECT N.*     ');
   qryCnsNotificacao.SQL.Add('FROM TAB_NOTIFICACAO N      ');
   qryCnsNotificacao.SQL.Add('WHERE N.COD_NOTIFICACAO > 0 ');

   qryCnsNotificacao.SQL.Add('ORDER BY N.COD_NOTIFICACAO DESC ');

   if (pagina > 0) then begin
      qryCnsNotificacao.SQL.Add('LIMIT :PAGINA, :QTD_REG');
      qryCnsNotificacao.ParamByName('PAGINA').Value := (pagina - 1) * QTD_REG_PAGINA_NOTIFICACOES;
      qryCnsNotificacao.ParamByName('QTD_REG').Value:= QTD_REG_PAGINA_NOTIFICACOES;
   end;

   qryCnsNotificacao.Open;
end;

procedure TdtmNotificacao.MarcarNotificacaoLida(cod_notificacao: integer; bLida: boolean);
begin
   qryNotificacao.Active := false;
   qryNotificacao.SQL.Clear;
   qryNotificacao.SQL.Add('UPDATE TAB_NOTIFICACAO SET IND_LIDO = :IND_LIDO  ');
   qryNotificacao.SQL.Add('WHERE COD_NOTIFICACAO = :COD_NOTIFICACAO ');

   if (bLida) then begin
      qryNotificacao.ParamByName('IND_LIDO').Value := 'S';
   end else begin
      qryNotificacao.ParamByName('IND_LIDO').Value := 'N';
   end;

   qryNotificacao.ParamByName('COD_NOTIFICACAO').Value:= cod_notificacao;
   qryNotificacao.ExecSQL();
end;

procedure TdtmNotificacao.ExcluirNotificacao(cod_notificacao: integer);
begin
   qryNotificacao.Active := false;
   qryNotificacao.SQL.Clear;
   qryNotificacao.SQL.Add('DELETE FROM TAB_NOTIFICACAO ');
   qryNotificacao.SQL.Add('WHERE COD_NOTIFICACAO = :COD_NOTIFICACAO ');
   qryNotificacao.ParamByName('COD_NOTIFICACAO').Value:= cod_notificacao;
   qryNotificacao.ExecSQL();
end;

procedure TdtmNotificacao.InserirNotificacao(cod_notificacao: integer; dt_notificacao, titulo, texto: string);
var qryNotificacaoCad : TFDQuery;
begin
   try
     qryNotificacaoCad  := TFDQuery.Create(nil);
     qryNotificacaoCad.Connection := dtmGlobal.connection;

     qryNotificacaoCad.Active := false;
     qryNotificacaoCad.SQL.Clear;
     qryNotificacaoCad.SQL.Add('INSERT INTO TAB_NOTIFICACAO (COD_NOTIFICACAO, DATA_NOTIFICACAO, TITULO, TEXTO, IND_LIDO) ');
     qryNotificacaoCad.SQL.Add('VALUES (:COD_NOTIFICACAO, :DATA_NOTIFICACAO, :TITULO, :TEXTO, :IND_LIDO) ');

     qryNotificacaoCad.ParamByName('COD_NOTIFICACAO').Value  := cod_notificacao;
     qryNotificacaoCad.ParamByName('DATA_NOTIFICACAO').Value := dt_notificacao;
     qryNotificacaoCad.ParamByName('TITULO').Value  := titulo;
     qryNotificacaoCad.ParamByName('TEXTO').Value   := texto;
     qryNotificacaoCad.ParamByName('IND_LIDO').Value:= 'N';
     qryNotificacaoCad.ExecSQL();
   finally
     qryNotificacaoCad.close;
     freeAndNil(qryNotificacaoCad);
   end;
end;

procedure TdtmNotificacao.ListarNotificacoesWS;
var resp: IResponse;
begin
   try
     //Limpa os campos da tabela, para que possa ser populada novamente
     TabNotificacoes.FieldDefs.Clear;

     resp := TRequest.New.BaseURL(BASE_URL)
              .Resource('notificacoes')
              .TokenBearer(TSession.TOKEN_JWT) //Passa o Token na Requisição para Autenticação
              .DataSetAdapter(TabNotificacoes)
              .Accept('application/json')
              .Get;

     if resp.StatusCode <> 200 then begin
        raise Exception.Create(resp.Content);
     end;
   finally

   end;
end;

end.
