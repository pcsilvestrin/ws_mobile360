unit DataModule.Global;

interface

uses
  System.SysUtils, System.Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.SQLite,
  FireDAC.Phys.SQLiteDef, FireDAC.Stan.ExprFuncs,
  FireDAC.Phys.SQLiteWrapper.Stat, FireDAC.FMXUI.Wait, Data.DB,
  FireDAC.Comp.Client, DataSet.Serialize.Config, System.IOUtils,
  FireDAC.Stan.Param, FireDAC.DatS, FireDAC.DApt.Intf, FireDAC.DApt,
  FireDAC.Comp.DataSet, FireDAC.VCLUI.Wait;

type
  TdtmGlobal = class(TDataModule)
    connection: TFDConnection;
    qrySinc: TFDQuery;
    procedure DataModuleCreate(Sender: TObject);
    procedure connectionBeforeConnect(Sender: TObject);
    procedure connectionAfterConnect(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    function EstatisticaSinc(tabela: string): integer;
  end;

var
  dtmGlobal: TdtmGlobal;

implementation

{%CLASSGROUP 'FMX.Controls.TControl'}

{$R *.dfm}

procedure TdtmGlobal.connectionAfterConnect(Sender: TObject);
var sql : string;
begin
   sql := '';
   sql := sql +' CREATE TABLE IF NOT EXISTS TAB_USUARIO ( ';
   sql := sql +'     COD_USUARIO    INTEGER NOT NULL PRIMARY KEY, ';
   sql := sql +'     NOME           VARCHAR (100),  ';
   sql := sql +'     EMAIL          VARCHAR (100),  ';
   sql := sql +'     SENHA          VARCHAR (50),   ';
   sql := sql +'     TOKEN_PUSH     VARCHAR (200),  ';
   sql := sql +'     TOKEN_JWT      VARCHAR (1000), ';
   sql := sql +'     IND_LOGIN      CHAR (1),       ';
   sql := sql +'     IND_ONBOARDING CHAR (1)        ';
   sql := sql +' );  ';
   connection.ExecSQL(sql);

   sql := '';
   sql := sql +' CREATE TABLE IF NOT EXISTS TAB_CLIENTE (  ';
   sql := sql +'     COD_CLIENTE_LOCAL   INTEGER  NOT NULL PRIMARY KEY AUTOINCREMENT,  ';
   sql := sql +'     CNPJ_CPF            VARCHAR (20),   ';
   sql := sql +'     NOME                VARCHAR (100),  ';
   sql := sql +'     FONE                VARCHAR (20),   ';
   sql := sql +'     EMAIL               VARCHAR (100),  ';
   sql := sql +'     ENDERECO            VARCHAR (500),  ';
   sql := sql +'     NUMERO              VARCHAR (50),   ';
   sql := sql +'     COMPLEMENTO         VARCHAR (50),   ';
   sql := sql +'     BAIRRO              VARCHAR (50),   ';
   sql := sql +'     CIDADE              VARCHAR (50),   ';
   sql := sql +'     UF                  VARCHAR (2),    ';
   sql := sql +'     CEP                 VARCHAR (10),   ';
   sql := sql +'     IND_SINCRONIZAR     CHAR (1),       ';
   sql := sql +'     LATITUDE            DECIMAL (5, 6), ';
   sql := sql +'     LONGITUDE           DECIMAL (5, 6), ';
   sql := sql +'     LIMITE_DISPONIVEL   DECIMAL (12, 2),';
   sql := sql +'     COD_CLIENTE_OFICIAL INTEGER         ';
   //sql := sql +'     DT_ULT_SINCRONIZACAO VARCHAR(50)    ';
   sql := sql +' );  ';
   connection.ExecSQL(sql);

   sql := '';
   sql := sql +' CREATE TABLE IF NOT EXISTS TAB_PRODUTO (  ';
   sql := sql +'     COD_PRODUTO_LOCAL   INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, ';
   sql := sql +'     DESCRICAO           VARCHAR (200),   ';
   sql := sql +'     VALOR               DECIMAL (12, 2), ';
   sql := sql +'     FOTO                BLOB,            ';
   sql := sql +'     IND_SINCRONIZAR     CHAR (1),        ';
   sql := sql +'     QTD_ESTOQUE         DECIMAL (12, 2), ';
   sql := sql +'     COD_PRODUTO_OFICIAL INTEGER          ';
   sql := sql +' );  ';
   connection.ExecSQL(sql);

   sql := '';
   sql := sql +' CREATE TABLE IF NOT EXISTS TAB_COND_PAGTO (    ';
   sql := sql +'     COD_COND_PAGTO INTEGER NOT NULL PRIMARY KEY, ';
   sql := sql +'     COND_PAGTO     VARCHAR (100), ';
   sql := sql +'     IND_EXCLUIDO   CHAR(1)        ';
   sql := sql +' ); ';
   connection.ExecSQL(sql);

   sql := '';
   sql := sql +' CREATE TABLE IF NOT EXISTS TAB_CONFIG ( ';
   sql := sql +'     CAMPO VARCHAR (50)  PRIMARY KEY NOT NULL, ';
   sql := sql +'     VALOR VARCHAR (200)   ';
   sql := sql +' ); ';
   connection.ExecSQL(sql);

   sql := '';
   sql := sql +' CREATE TABLE IF NOT EXISTS TAB_NOTIFICACAO (     ';
   sql := sql +'     COD_NOTIFICACAO INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, ';
   sql := sql +'     DATA_NOTIFICACAO DATETIME,     ';
   sql := sql +'     TITULO          VARCHAR (100), ';
   sql := sql +'     TEXTO           VARCHAR (500), ';
   sql := sql +'     IND_LIDO        CHAR (1)       ';
   sql := sql +' ); ';
   connection.ExecSQL(sql);

   sql := '';
   sql := sql +' CREATE TABLE IF NOT EXISTS TAB_PEDIDO (        ';
   sql := sql +'     COD_PEDIDO_LOCAL   INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, ';
   sql := sql +'     COD_CLIENTE_LOCAL  INTEGER,  ';
   sql := sql +'     COD_USUARIO        INTEGER,  ';
   sql := sql +'     TIPO_PEDIDO        CHAR (1), ';
   sql := sql +'     DATA_PEDIDO        DATETIME, ';
   sql := sql +'     CONTATO            VARCHAR (100), ';
   sql := sql +'     OBS                VARCHAR (500), ';
   sql := sql +'     COD_PEDIDO_OFICIAL INTEGER,  ';
   sql := sql +'     IND_SINCRONIZAR    CHAR (1), ';
   sql := sql +'     VALOR_TOTAL        DECIMAL (12, 2),';
   sql := sql +'     COD_COND_PAGTO     INTEGER,  ';
   sql := sql +'     PRAZO_ENTREGA      VARCHAR (50),   ';
   sql := sql +'     DATA_ENTREGA       DATETIME, ';

   sql := sql +' 	FOREIGN KEY (COD_USUARIO) REFERENCES TAB_USUARIO(COD_USUARIO), ';
   sql := sql +'  FOREIGN KEY (COD_CLIENTE_LOCAL) REFERENCES TAB_CLIENTE(COD_CLIENTE_LOCAL), ';
   sql := sql +'  FOREIGN KEY (COD_COND_PAGTO) REFERENCES TAB_COND_PAGTO(COD_COND_PAGTO) ';
   sql := sql +' ); ';
   connection.ExecSQL(sql);

   sql := '';
   sql := sql +' CREATE TABLE IF NOT EXISTS TAB_PEDIDO_ITEM (  ';
   sql := sql +'     COD_ITEM         INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, ';
   sql := sql +'     COD_PEDIDO_LOCAL INTEGER         NOT NULL, ';
   sql := sql +'     COD_PRODUTO_LOCAL INTEGER,  ';
   sql := sql +'     QTD              INTEGER,   ';
   sql := sql +'     VALOR_UNITARIO   DECIMAL (12, 2), ';
   sql := sql +'     VALOR_TOTAL      DECIMAL (12, 2), ';

   sql := sql +'     FOREIGN KEY (COD_PEDIDO_LOCAL) REFERENCES TAB_PEDIDO(COD_PEDIDO_LOCAL),   ';
   sql := sql +'     FOREIGN KEY (COD_PRODUTO_LOCAL) REFERENCES TAB_PRODUTO(COD_PRODUTO_LOCAL) ';
   sql := sql +' ); ';
   connection.ExecSQL(sql);

   sql := '';
   sql := sql +' CREATE TABLE IF NOT EXISTS TAB_PEDIDO_ITEM_TEMP (   ';
   sql := sql +'     COD_ITEM         INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, ';
   //sql := sql +'     COD_PEDIDO_LOCAL INTEGER         NOT NULL, ';
   sql := sql +'     COD_PRODUTO_LOCAL INTEGER,        ';
   sql := sql +'     QTD              INTEGER,         ';
   sql := sql +'     VALOR_UNITARIO   DECIMAL (12, 2), ';
   sql := sql +'     VALOR_TOTAL      DECIMAL (12, 2)  ';
   sql := sql +' ); ';
   connection.ExecSQL(sql);

   //connection.ExecSQL(' ALTER TABLE TAB_CLIENTE ADD  DT_ULT_SINCRONIZACAO VARCHAR(50) ');

    // Cadastra cond pagto para testes...
    {connection.ExecSQL('insert or replace into tab_cond_pagto(cod_cond_pagto, cond_pagto) values(1, ''A Vista'');');
    connection.ExecSQL('insert or replace into tab_cond_pagto(cod_cond_pagto, cond_pagto) values(2, ''07 Dias'');');
    connection.ExecSQL('insert or replace into tab_cond_pagto(cod_cond_pagto, cond_pagto) values(3, ''10 Dias'');');
    connection.ExecSQL('insert or replace into tab_cond_pagto(cod_cond_pagto, cond_pagto) values(4, ''15 Dias'');');
    connection.ExecSQL('insert or replace into tab_cond_pagto(cod_cond_pagto, cond_pagto) values(5, ''30 Dias'');');
    connection.ExecSQL('insert or replace into tab_cond_pagto(cod_cond_pagto, cond_pagto) values(6, ''60 Dias'');');
    connection.ExecSQL('insert or replace into tab_cond_pagto(cod_cond_pagto, cond_pagto) values(7, ''30/60 Dias'');');}

end;

procedure TdtmGlobal.connectionBeforeConnect(Sender: TObject);
begin
   connection.DriverName := 'SQLite';
   {$IFDEF MSWINDOWS}
   connection.Params.Values['Database'] := System.SysUtils.GetCurrentDir +'\pedidos.db';
   {$ELSE}
   connection.Params.Values['Database'] := TPath.Combine(TPath.GetDocumentsPath, 'pedidos.db') ;
   {$ENDIF}
end;

procedure TdtmGlobal.DataModuleCreate(Sender: TObject);
begin
   //Utilizado para definir o tipo da formata��o do JSON, no caso, as "vari�veis/name" ser�o tratadas em minusculo
   TDataSetSerializeConfig.GetInstance.CaseNameDefinition := cndLower;

   //Define o separador das decimais
   TDataSetSerializeConfig.GetInstance.Import.DecimalSeparator := '.';

   connection.Connected := true;
end;

function TdtmGlobal.EstatisticaSinc(tabela: string): integer;
begin
   qrySinc.Active := false;
   qrySinc.SQL.Clear;
   qrySinc.SQL.Add('select count(*) as qtd from '+tabela);
   qrySinc.SQL.Add('where ind_sincronizar = ''S''');
   qrySinc.Active := true;

   result := qrySinc.fieldByName('qtd').AsInteger;
end;

end.
