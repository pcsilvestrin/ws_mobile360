object dtmCliente: TdtmCliente
  Height = 544
  Width = 416
  object qryCnsCliente: TFDQuery
    Connection = dtmGlobal.connection
    Left = 72
    Top = 40
  end
  object qryCliente: TFDQuery
    Connection = dtmGlobal.connection
    Left = 168
    Top = 40
  end
  object TabCliente: TFDMemTable
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    Left = 72
    Top = 120
  end
  object qryClienteSinc: TFDQuery
    Connection = dtmGlobal.connection
    Left = 248
    Top = 40
  end
end
