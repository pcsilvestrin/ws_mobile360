object dtmUsuario: TdtmUsuario
  Height = 544
  Width = 416
  object qryUsuario: TFDQuery
    Connection = dtmGlobal.connection
    Left = 168
    Top = 96
  end
  object qryCnsUsuario: TFDQuery
    Connection = dtmGlobal.connection
    Left = 80
    Top = 96
  end
  object TabUsuario: TFDMemTable
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    Left = 80
    Top = 200
  end
  object qryConfig: TFDQuery
    Connection = dtmGlobal.connection
    Left = 248
    Top = 96
  end
end
