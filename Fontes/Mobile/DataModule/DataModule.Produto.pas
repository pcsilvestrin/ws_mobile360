unit DataModule.Produto;

interface

uses
  System.SysUtils, System.Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Data.DB,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, FMX.Graphics;

type
  TdtmProduto = class(TDataModule)
    qryCnsProduto: TFDQuery;
    qryProduto: TFDQuery;
    TabProduto: TFDMemTable;
    qryProdutoSinc: TFDQuery;
    qryFoto: TFDQuery;
  private
    { Private declarations }
  public
    { Public declarations }
    function ListarProdutoId(cod_produto_local, cod_produto_oficial: integer): boolean;
    procedure ListarProdutos(pagina: integer; busca, ind_sincronizacao: string);

    procedure InserirProdutoId(
    descricao, ind_sincronizar: string; valor, qtd_estoque: double; foto: TBitmap;
    cod_produto_oficial : integer);

    procedure EditarProdutoId(
    cod_produto_local: integer; descricao, ind_sincronizar: string;
    valor, qtd_estoque: double; foto: TBitmap);
    procedure ExcluirProdutoId(cod_produto_local: integer);

    procedure ListarProdutosWS(dt_ult_sinc: string; pagina: integer);

    procedure InserirProdutoWS(
   cod_produto_local, cod_produto_oficial: integer; descricao: string;
   valor, qtd_estoque: double; dt_ult_sincronizacao : string
);

    procedure MarcarProdutoSincronizado(cod_produto_local, cod_produto_oficial: integer);
    procedure EditarFoto(cod_produto_oficial: integer; foto: TBitmap);
    procedure EditarFotoWS(cod_produto_oficial: integer; arq: string);
  end;

var
  dtmProduto: TdtmProduto;

implementation

{%CLASSGROUP 'FMX.Controls.TControl'}

uses DataModule.Global, uConstantes, RESTRequest4D, System.JSON, uSession,
  System.Variants;

{$R *.dfm}

procedure TdtmProduto.ListarProdutos(pagina: integer; busca, ind_sincronizacao: string);
begin
   qryCnsProduto.Close;
   qryCnsProduto.SQL.Clear;
   qryCnsProduto.SQL.Add('SELECT P.*     ');
   qryCnsProduto.SQL.Add('FROM TAB_PRODUTO P        ');
   qryCnsProduto.SQL.Add('WHERE P.COD_PRODUTO_LOCAL > 0 ');

   if (trim(busca) <> emptyStr) then begin
      qryCnsProduto.SQL.Add(' AND P.DESCRICAO LIKE :DESCRICAO ');
      qryCnsProduto.ParamByName('DESCRICAO').Value := '%'+busca+'%';
   end;

   if (trim(ind_sincronizacao) <> emptyStr) then begin
      qryCnsProduto.SQL.Add(' AND P.IND_SINCRONIZAR = :IND_SINCRONIZAR ');
      qryCnsProduto.ParamByName('IND_SINCRONIZAR').Value := ind_sincronizacao;
   end;

   qryCnsProduto.SQL.Add('ORDER BY P.DESCRICAO');

   if (pagina > 0) then begin
      qryCnsProduto.SQL.Add('LIMIT :PAGINA, :QTD_REG');
      qryCnsProduto.ParamByName('PAGINA').Value := (pagina - 1) * QTD_REG_PAGINA_PRODUTOS;
      qryCnsProduto.ParamByName('QTD_REG').Value:= QTD_REG_PAGINA_PRODUTOS;
   end;

   qryCnsProduto.Open;
end;

function TdtmProduto.ListarProdutoId(cod_produto_local, cod_produto_oficial: integer): boolean;
begin
   qryProduto.Close;
   qryProduto.SQL.Clear;
   qryProduto.SQL.Add('SELECT P.*     ');
   qryProduto.SQL.Add('FROM TAB_PRODUTO P        ');
   qryProduto.SQL.Add('WHERE P.COD_PRODUTO_LOCAL > 0 ');

   if (cod_produto_local > 0) then begin
      qryProduto.SQL.Add(' AND P.COD_PRODUTO_LOCAL = :COD_PRODUTO_LOCAL ');
      qryProduto.ParamByName('COD_PRODUTO_LOCAL').Value := cod_produto_local;
   end;

   if (cod_produto_oficial > 0) then begin
      qryProduto.SQL.Add(' AND P.COD_PRODUTO_OFICIAL = :COD_PRODUTO_OFICIAL ');
      qryProduto.ParamByName('COD_PRODUTO_OFICIAL').Value := cod_produto_oficial;
   end;

   qryProduto.SQL.Add('ORDER BY P.DESCRICAO');
   qryProduto.Active := true;

   result := (qryProduto.RecordCount > 0);
end;

procedure TdtmProduto.InserirProdutoId(
    descricao, ind_sincronizar: string; valor, qtd_estoque: double; foto: TBitmap;
    cod_produto_oficial : integer
);
begin
   qryProduto.Active := false;
   qryProduto.SQL.Clear;
   qryProduto.SQL.Add('INSERT INTO TAB_PRODUTO ( DESCRICAO, VALOR, FOTO, QTD_ESTOQUE, IND_SINCRONIZAR, COD_PRODUTO_OFICIAL ) ');
   qryProduto.SQL.Add('VALUES ( :DESCRICAO, :VALOR, :FOTO, :QTD_ESTOQUE, :IND_SINCRONIZAR, :COD_PRODUTO_OFICIAL ) ');
   qryProduto.ParamByName('DESCRICAO').Value   := descricao;
   qryProduto.ParamByName('VALOR').Value       := valor;

   qryProduto.ParamByName('FOTO').DataType := ftString;
   qryProduto.ParamByName('FOTO').Value    := Unassigned;
   if (foto <> nil) then begin
      qryProduto.ParamByName('FOTO').Assign(foto);
   end;

   qryProduto.ParamByName('QTD_ESTOQUE').Value := qtd_estoque;
   qryProduto.ParamByName('IND_SINCRONIZAR').Value := ind_sincronizar;
   qryProduto.ParamByName('COD_PRODUTO_OFICIAL').Value := cod_produto_oficial;
   qryProduto.ExecSQL;
end;

procedure TdtmProduto.EditarProdutoId(
    cod_produto_local: integer; descricao, ind_sincronizar: string;
    valor, qtd_estoque: double; foto: TBitmap
);
begin
   qryProduto.Active := false;
   qryProduto.SQL.Clear;
   qryProduto.SQL.Add('UPDATE TAB_PRODUTO      ');
   qryProduto.SQL.Add('   SET DESCRICAO = :DESCRICAO, ');
   qryProduto.SQL.Add('       VALOR = :VALOR,  ');
   qryProduto.SQL.Add('       QTD_ESTOQUE = :QTD_ESTOQUE, ');
   qryProduto.SQL.Add('       IND_SINCRONIZAR = :IND_SINCRONIZAR ');

   if (foto <> nil) then begin
      qryProduto.SQL.Add('   , FOTO  = :FOTO   ');
      qryProduto.ParamByName('FOTO').Assign(foto);
   end;

   qryProduto.SQL.Add('WHERE COD_PRODUTO_LOCAL = :COD_PRODUTO_LOCAL ');

   qryProduto.ParamByName('COD_PRODUTO_LOCAL').Value := cod_produto_local;
   qryProduto.ParamByName('DESCRICAO').Value   := descricao;
   qryProduto.ParamByName('VALOR').Value       := valor;
   qryProduto.ParamByName('QTD_ESTOQUE').Value := qtd_estoque;
   qryProduto.ParamByName('IND_SINCRONIZAR').Value := ind_sincronizar;
   qryProduto.ExecSQL;
end;

procedure TdtmProduto.ExcluirProdutoId(cod_produto_local: integer);
begin
   //Verifica se o Item est� vinculado a algum pedido
   qryProduto.Active := false;
   qryProduto.SQL.Clear;
   qryProduto.SQL.Add('SELECT * FROM TAB_PEDIDO_ITEM ');
   qryProduto.SQL.Add('WHERE COD_PRODUTO_LOCAL = :COD_PRODUTO_LOCAL ');
   qryProduto.ParamByName('COD_PRODUTO_LOCAL').Value := cod_produto_local;
   qryProduto.Active := true;

   if (not qryProduto.IsEmpty) then begin
     raise Exception.Create('O produto j� est� sendo usado em um pedido e n�o pode ser exclu�do!');
   end;


   //Faz a exclus�o do item
   qryProduto.Active := false;
   qryProduto.SQL.Clear;
   qryProduto.SQL.Add('DELETE FROM TAB_PRODUTO ');
   qryProduto.SQL.Add('WHERE COD_PRODUTO_LOCAL = :COD_PRODUTO_LOCAL ');

   qryProduto.ParamByName('COD_PRODUTO_LOCAL').Value := cod_produto_local;
   qryProduto.ExecSQL;
end;

procedure TdtmProduto.ListarProdutosWS(dt_ult_sinc: string; pagina: integer);
var resp: IResponse;
begin
   //Limpa os campos da tabela, para que possa ser populada novamente
   TabProduto.FieldDefs.Clear;

   resp := TRequest.New.BaseURL(BASE_URL)
            .Resource('produtos/sincronizacao')
            .AddParam('dt_ult_sincronizacao', dt_ult_sinc)
            .AddParam('pagina', pagina.ToString)
            .TokenBearer(TSession.TOKEN_JWT) //Passa o Token na Requisi��o para Autentica��o
            .Accept('application/json')
            .DataSetAdapter(TabProduto) //As informa��es retornadas pelo Servidor s�o inseridas neste Objeto
            .Get;

   if resp.StatusCode <> 200 then begin
      raise Exception.Create(resp.Content);
   end;
end;

procedure TdtmProduto.EditarFotoWS(cod_produto_oficial: integer; arq: string);
var resp: IResponse;
begin
   resp := TRequest.New.BaseURL(BASE_URL)
            .Resource('produtos/foto')
            .ResourceSuffix(cod_produto_oficial.ToString) //Ex. http://..../produtos/foto/1
            .TokenBearer(TSession.TOKEN_JWT)
            .AddParam('files', arq, pkFILE)  //Carrega o arquivo local para a requisi��o
            .Put;

   if resp.StatusCode <> 200 then begin
      raise Exception.Create(resp.Content);
   end;
end;

procedure TdtmProduto.InserirProdutoWS(
   cod_produto_local, cod_produto_oficial: integer; descricao: string;
   valor, qtd_estoque: double; dt_ult_sincronizacao : string
);
var resp : IResponse;
    json : TJSONObject;
begin
   try
     //Limpa os campos da tabela, para que possa ser populada novamente
     TabProduto.FieldDefs.Clear;


     json := TJSONObject.Create;
     json.AddPair('cod_produto_local', TJSONNumber.Create(cod_produto_local));
     json.AddPair('descricao', descricao);
     json.AddPair('valor', TJSONNumber.Create(valor));
     json.AddPair('qtd_estoque', TJSONNumber.Create(qtd_estoque));
     json.AddPair('cod_produto_oficial', TJSONNumber.Create(cod_produto_oficial));
     json.AddPair('dt_ult_sincronizacao', dt_ult_sincronizacao);

     resp := TRequest.New.BaseURL(BASE_URL)
              .Resource('produtos/sincronizacao')
              .AddBody(json.ToJSON())
              .TokenBearer(TSession.TOKEN_JWT) //Passa o Token na Requisi��o para Autentica��o
              .Accept('application/json')
              .DataSetAdapter(TabProduto) //As informa��es retornadas pelo Servidor s�o inseridas neste Objeto
              .Post;

     if resp.StatusCode <> 200 then begin
        raise Exception.Create(resp.Content);
     end;
   finally
     json.DisposeOf;
   end;
end;

procedure TdtmProduto.MarcarProdutoSincronizado(cod_produto_local, cod_produto_oficial: integer);
begin
   qryProdutoSinc.Active := false;
   qryProdutoSinc.SQL.Clear;
   qryProdutoSinc.SQL.Add('UPDATE TAB_PRODUTO       ');
   qryProdutoSinc.SQL.Add('   SET COD_PRODUTO_OFICIAL = :COD_PRODUTO_OFICIAL, ');
   qryProdutoSinc.SQL.Add('       IND_SINCRONIZAR     = :IND_SINCRONIZAR      ');
   qryProdutoSinc.SQL.Add('WHERE COD_PRODUTO_LOCAL    = :COD_PRODUTO_LOCAL  ');

   qryProdutoSinc.ParamByName('COD_PRODUTO_LOCAL').Value   := cod_produto_local;
   qryProdutoSinc.ParamByName('COD_PRODUTO_OFICIAL').Value := cod_produto_oficial;
   qryProdutoSinc.ParamByName('IND_SINCRONIZAR').Value     := 'N';

   qryProdutoSinc.ExecSQL;
end;

procedure TdtmProduto.EditarFoto(cod_produto_oficial: integer; foto: TBitmap);
begin
   qryFoto.Active := false;
   qryFoto.SQL.Clear;
   qryFoto.SQL.Add('UPDATE TAB_PRODUTO SET FOTO = :FOTO ');
   qryFoto.SQL.Add('WHERE COD_PRODUTO_OFICIAL = :COD_PRODUTO_OFICIAL ');
   qryFoto.ParamByName('COD_PRODUTO_OFICIAL').Value := cod_produto_oficial;
   qryFoto.ParamByName('FOTO').Assign(foto);
   qryFoto.ExecSQL;
end;


end.
