unit DataModule.Cliente;

interface

uses
  System.SysUtils, System.Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Data.DB,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client;

type
  TdtmCliente = class(TDataModule)
    qryCnsCliente: TFDQuery;
    qryCliente: TFDQuery;
    TabCliente: TFDMemTable;
    qryClienteSinc: TFDQuery;
  private
    { Private declarations }
  public
    { Public declarations }
    procedure ListarClientes(pagina: integer; busca, ind_sincronizacao: string);
    function ListarClienteId(cod_cliente_local, cod_cliente_oficial: integer): boolean;
    procedure InserirClienteId(
     cpf_cnpj, nome, fone, email, endereco, numero, complemento, bairro, cidade, uf, cep: string;
     limite_disponivel, latitude, longitude: double; ind_sincronizar: string; cod_cliente_oficial: integer
    );
    procedure EditarClienteId(
     cod_cliente_local: integer;   cpf_cnpj, nome, fone, email, endereco, numero, complemento, bairro, cidade, uf, cep, ind_sincronizar: string;
     limite_disponivel, latitude, longitude: double
    );
    procedure ExcluirClienteId(cod_cliente_local: integer);

    procedure InserirClienteWS(
     cod_cliente_oficial, cod_cliente_local : integer; cnpj_cpf, nome, fone, email, endereco, numero, complemento, bairro,
     cidade, uf, cep: string; latitude, longitude, limite_disponivel : double; dt_ult_sincronizacao : string
    );
    procedure ListarClientesWS(dt_ult_sinc: string; pagina: integer);
    procedure MarcarClienteSincronizado(cod_cliente_local, cod_cliente_oficial: integer);
  end;

var
  dtmCliente: TdtmCliente;

implementation

{%CLASSGROUP 'FMX.Controls.TControl'}

uses uConstantes, RESTRequest4D, System.JSON, uSession;

{$R *.dfm}

{ TdmCliente }

procedure TdtmCliente.ListarClientes(pagina: integer; busca, ind_sincronizacao: string);
begin
   qryCnsCliente.Close;
   qryCnsCliente.SQL.Clear;
   qryCnsCliente.SQL.Add('SELECT C.*     ');
   qryCnsCliente.SQL.Add('FROM TAB_CLIENTE C        ');
   qryCnsCliente.SQL.Add('WHERE C.COD_CLIENTE_LOCAL > 0 ');

   if (trim(busca) <> emptyStr) then begin
      qryCnsCliente.SQL.Add(' AND C.NOME LIKE :NOME ');
      qryCnsCliente.ParamByName('NOME').Value := '%'+busca+'%';
   end;

   if (trim(ind_sincronizacao) <> emptyStr) then begin
      qryCnsCliente.SQL.Add(' AND C.IND_SINCRONIZAR = :IND_SINCRONIZAR ');
      qryCnsCliente.ParamByName('IND_SINCRONIZAR').Value := ind_sincronizacao;
   end;

   qryCnsCliente.SQL.Add('ORDER BY C.NOME');

   if (pagina > 0) then begin
      qryCnsCliente.SQL.Add('LIMIT :PAGINA, :QTD_REG');
      qryCnsCliente.ParamByName('PAGINA').Value := (pagina - 1) * QTD_REG_PAGINA_CLIENTES;
      qryCnsCliente.ParamByName('QTD_REG').Value:= QTD_REG_PAGINA_CLIENTES;
   end;

   qryCnsCliente.Open;
end;

function TdtmCliente.ListarClienteId(cod_cliente_local, cod_cliente_oficial: integer): boolean;
begin
   qryCliente.Close;
   qryCliente.SQL.Clear;
   qryCliente.SQL.Add('SELECT P.*     ');
   qryCliente.SQL.Add('FROM TAB_CLIENTE P        ');
   qryCliente.SQL.Add('WHERE P.COD_CLIENTE_LOCAL > 0 ');

   if (cod_cliente_local > 0) then begin
      qryCliente.SQL.Add(' AND P.COD_CLIENTE_LOCAL = :COD_CLIENTE_LOCAL ');
      qryCliente.ParamByName('COD_CLIENTE_LOCAL').Value := cod_cliente_local;
   end;

   if (cod_cliente_oficial > 0) then begin
      qryCliente.SQL.Add(' AND P.COD_CLIENTE_OFICIAL = :COD_CLIENTE_OFICIAL ');
      qryCliente.ParamByName('COD_CLIENTE_OFICIAL').Value := cod_cliente_oficial;
   end;

   qryCliente.Active := true;

   result := (qryCliente.RecordCount > 0);
end;

procedure TdtmCliente.InserirClienteId(
  cpf_cnpj, nome, fone, email, endereco, numero, complemento, bairro, cidade, uf, cep: string;
  limite_disponivel, latitude, longitude: double; ind_sincronizar: string; cod_cliente_oficial: integer
);
begin
   qryCliente.Active := false;
   qryCliente.SQL.Clear;
   qryCliente.SQL.Add('INSERT INTO TAB_CLIENTE ( ');
   qryCliente.SQL.Add('   CNPJ_CPF, NOME, FONE, EMAIL, ENDERECO, NUMERO, COMPLEMENTO, BAIRRO, ');
   qryCliente.SQL.Add('   CIDADE, UF, CEP, LIMITE_DISPONIVEL, LATITUDE, LONGITUDE, IND_SINCRONIZAR, ');
   qryCliente.SQL.Add('   COD_CLIENTE_OFICIAL    ');
   qryCliente.SQL.Add(')        ');
   qryCliente.SQL.Add('VALUES ( ');
   qryCliente.SQL.Add('   :CNPJ_CPF, :NOME, :FONE, :EMAIL, :ENDERECO, :NUMERO, :COMPLEMENTO, :BAIRRO, ');
   qryCliente.SQL.Add('   :CIDADE, :UF, :CEP, :LIMITE_DISPONIVEL, :LATITUDE, :LONGITUDE, :IND_SINCRONIZAR, ');
   qryCliente.SQL.Add('   :COD_CLIENTE_OFICIAL    ');
   qryCliente.SQL.Add(')        ');

   qryCliente.ParamByName('CNPJ_CPF').Value := cpf_cnpj;
   qryCliente.ParamByName('NOME').Value     := nome;
   qryCliente.ParamByName('FONE').Value     := fone;
   qryCliente.ParamByName('EMAIL').Value    := email;
   qryCliente.ParamByName('ENDERECO').Value := endereco;
   qryCliente.ParamByName('NUMERO').Value   := numero;
   qryCliente.ParamByName('COMPLEMENTO').Value   := complemento;
   qryCliente.ParamByName('BAIRRO').Value   := bairro;
   qryCliente.ParamByName('CIDADE').Value   := cidade;
   qryCliente.ParamByName('UF').Value       := uf;
   qryCliente.ParamByName('CEP').Value      := cep;
   qryCliente.ParamByName('LIMITE_DISPONIVEL').Value := limite_disponivel;
   qryCliente.ParamByName('LATITUDE').Value  := latitude;
   qryCliente.ParamByName('LONGITUDE').Value := longitude;
   qryCliente.ParamByName('IND_SINCRONIZAR').Value := ind_sincronizar;
   qryCliente.ParamByName('COD_CLIENTE_OFICIAL').Value := cod_cliente_oficial;

   qryCliente.ExecSQL;
end;

procedure TdtmCliente.EditarClienteId(
    cod_cliente_local: integer;   cpf_cnpj, nome, fone, email, endereco, numero, complemento, bairro, cidade, uf, cep, ind_sincronizar: string;
    limite_disponivel, latitude, longitude: double
);
begin
   qryCliente.Active := false;
   qryCliente.SQL.Clear;
   qryCliente.SQL.Add('UPDATE TAB_CLIENTE       ');
   qryCliente.SQL.Add('   SET CNPJ_CPF = :CNPJ_CPF, ');
   qryCliente.SQL.Add('       NOME   = :NOME,   ');
   qryCliente.SQL.Add('       FONE   = :FONE,   ');
   qryCliente.SQL.Add('       EMAIL  = :EMAIL,  ');
   qryCliente.SQL.Add('       ENDERECO = :ENDERECO, ');
   qryCliente.SQL.Add('       NUMERO = :NUMERO, ');
   qryCliente.SQL.Add('       COMPLEMENTO = :COMPLEMENTO, ');
   qryCliente.SQL.Add('       BAIRRO = :BAIRRO, ');
   qryCliente.SQL.Add('       CIDADE = :CIDADE, ');
   qryCliente.SQL.Add('       UF     = :UF,     ');
   qryCliente.SQL.Add('       CEP    = :CEP,    ');
   qryCliente.SQL.Add('       LIMITE_DISPONIVEL = :LIMITE_DISPONIVEL,');
   qryCliente.SQL.Add('       IND_SINCRONIZAR = :IND_SINCRONIZAR,    ');
   qryCliente.SQL.Add('       LATITUDE  = :LATITUDE,    ');
   qryCliente.SQL.Add('       LONGITUDE = :LONGITUDE    ');
   qryCliente.SQL.Add('WHERE COD_CLIENTE_LOCAL = :COD_CLIENTE_LOCAL  ');

   qryCliente.ParamByName('COD_CLIENTE_LOCAL').Value := cod_cliente_local;
   qryCliente.ParamByName('CNPJ_CPF').Value := cpf_cnpj;
   qryCliente.ParamByName('NOME').Value     := nome;
   qryCliente.ParamByName('FONE').Value     := fone;
   qryCliente.ParamByName('EMAIL').Value    := email;
   qryCliente.ParamByName('ENDERECO').Value := endereco;
   qryCliente.ParamByName('NUMERO').Value   := numero;
   qryCliente.ParamByName('COMPLEMENTO').Value   := complemento;
   qryCliente.ParamByName('BAIRRO').Value   := bairro;
   qryCliente.ParamByName('CIDADE').Value   := cidade;
   qryCliente.ParamByName('UF').Value       := uf;
   qryCliente.ParamByName('CEP').Value      := cep;
   qryCliente.ParamByName('LIMITE_DISPONIVEL').Value := limite_disponivel;
   qryCliente.ParamByName('IND_SINCRONIZAR').Value := ind_sincronizar;
   qryCliente.ParamByName('LATITUDE').Value  := latitude;
   qryCliente.ParamByName('LONGITUDE').Value := longitude;

   qryCliente.ExecSQL;
end;

procedure TdtmCliente.MarcarClienteSincronizado(cod_cliente_local, cod_cliente_oficial: integer);
begin
   qryClienteSinc.Active := false;
   qryClienteSinc.SQL.Clear;
   qryClienteSinc.SQL.Add('UPDATE TAB_CLIENTE       ');
   qryClienteSinc.SQL.Add('   SET COD_CLIENTE_OFICIAL = :COD_CLIENTE_OFICIAL, ');
   qryClienteSinc.SQL.Add('       IND_SINCRONIZAR     = :IND_SINCRONIZAR      ');
   qryClienteSinc.SQL.Add('WHERE COD_CLIENTE_LOCAL = :COD_CLIENTE_LOCAL  ');

   qryClienteSinc.ParamByName('COD_CLIENTE_LOCAL').Value   := cod_cliente_local;
   qryClienteSinc.ParamByName('COD_CLIENTE_OFICIAL').Value := cod_cliente_oficial;
   qryClienteSinc.ParamByName('IND_SINCRONIZAR').Value     := 'N';

   qryClienteSinc.ExecSQL;
end;

procedure TdtmCliente.ExcluirClienteId(cod_cliente_local: integer);
begin
   //Verifica se o Cliente est� vinculado a algum pedido
   qryCliente.Active := false;
   qryCliente.SQL.Clear;
   qryCliente.SQL.Add('SELECT * FROM TAB_PEDIDO ');
   qryCliente.SQL.Add('WHERE COD_CLIENTE_LOCAL = :COD_CLIENTE_LOCAL ');
   qryCliente.ParamByName('COD_CLIENTE_LOCAL').Value := cod_cliente_local;
   qryCliente.Active := true;

   if (not qryCliente.IsEmpty) then begin
     raise Exception.Create('O cliente j� est� vinculado a um pedido e n�o pode ser exclu�do!');
   end;


   //Faz a exclus�o do Cliente
   qryCliente.Active := false;
   qryCliente.SQL.Clear;
   qryCliente.SQL.Add('DELETE FROM TAB_CLIENTE ');
   qryCliente.SQL.Add('WHERE COD_CLIENTE_LOCAL = :COD_CLIENTE_LOCAL ');

   qryCliente.ParamByName('COD_CLIENTE_LOCAL').Value := cod_cliente_local;
   qryCliente.ExecSQL;
end;

procedure TdtmCliente.ListarClientesWS(dt_ult_sinc: string; pagina: integer);
var resp: IResponse;
begin
   //Limpa os campos da tabela, para que possa ser populada novamente
   TabCliente.FieldDefs.Clear;

   resp := TRequest.New.BaseURL(BASE_URL)
            .Resource('clientes/sincronizacao')
            .AddParam('dt_ult_sincronizacao', dt_ult_sinc)
            .AddParam('pagina', pagina.ToString)
            .TokenBearer(TSession.TOKEN_JWT) //Passa o Token na Requisi��o para Autentica��o
            .Accept('application/json')
            .DataSetAdapter(TabCliente) //As informa��es retornadas pelo Servidor s�o inseridas neste Objeto
            .Get;

   if resp.StatusCode <> 200 then begin
      raise Exception.Create(resp.Content);
   end;
end;

procedure TdtmCliente.InserirClienteWS(
   cod_cliente_oficial, cod_cliente_local : integer; cnpj_cpf, nome, fone, email, endereco, numero, complemento, bairro,
   cidade, uf, cep: string; latitude, longitude, limite_disponivel : double; dt_ult_sincronizacao : string
);
var resp : IResponse;
    json : TJSONObject;
begin
   try
     //Limpa os campos da tabela, para que possa ser populada novamente
     TabCliente.FieldDefs.Clear;

     json := TJSONObject.Create;
     json.AddPair('cod_cliente_local', TJSONNumber.Create(cod_cliente_local));
     json.AddPair('cnpj_cpf', cnpj_cpf);
     json.AddPair('nome', nome);
     json.AddPair('fone', fone);
     json.AddPair('email', email);
     json.AddPair('endereco', endereco);
     json.AddPair('numero', numero);
     json.AddPair('complemento', complemento);
     json.AddPair('bairro', bairro);
     json.AddPair('cidade', cidade);
     json.AddPair('uf', uf);
     json.AddPair('cep', cep);
     json.AddPair('latitude', TJSONNumber.Create(latitude));
     json.AddPair('longitude', TJSONNumber.Create(longitude));
     json.AddPair('limite_disponivel', TJSONNumber.Create(limite_disponivel));
     json.AddPair('cod_cliente_oficial', TJSONNumber.Create(cod_cliente_oficial));
     json.AddPair('dt_ult_sincronizacao', dt_ult_sincronizacao);


     resp := TRequest.New.BaseURL(BASE_URL)
              .Resource('clientes/sincronizacao')
              .AddBody(json.ToJSON())
              .TokenBearer(TSession.TOKEN_JWT) //Passa o Token na Requisi��o para Autentica��o
              .Accept('application/json')
              .DataSetAdapter(TabCliente) //As informa��es retornadas pelo Servidor s�o inseridas neste Objeto
              .Post;

     if resp.StatusCode <> 200 then begin
        raise Exception.Create(resp.Content);
     end;
   finally
     json.DisposeOf;
   end;
end;

end.
