object dtmNotificacao: TdtmNotificacao
  Height = 480
  Width = 445
  object qryCnsNotificacao: TFDQuery
    Connection = dtmGlobal.connection
    Left = 104
    Top = 56
  end
  object qryNotificacao: TFDQuery
    Connection = dtmGlobal.connection
    Left = 224
    Top = 72
  end
  object TabNotificacoes: TFDMemTable
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    Left = 80
    Top = 200
  end
end
