unit UntPrincipal;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.Objects,
  FMX.TabControl, FMX.Controls.Presentation, FMX.StdCtrls, FMX.Layouts, FMX.Edit,
  FMX.ListView.Types, FMX.ListView.Appearances, FMX.ListView.Adapters.Base,
  FMX.ListView, FMX.ListBox, uFunctions, FMX.Memo.Types, FMX.ScrollBox, FMX.Memo,
  FMX.Styles.Objects, uFancyDialog, uActionSheet, uLoading;

type
  TFrmPrincipal = class(TForm)
    rectAbas: TRectangle;
    imgAbaDashboard: TImage;
    imgAbaPedidos: TImage;
    imgAbaNotifica��es: TImage;
    imgAbaClientes: TImage;
    imgAbaMais: TImage;
    TabControl: TTabControl;
    tabDashboard: TTabItem;
    tabPedidos: TTabItem;
    tabClientes: TTabItem;
    tabNotificacoes: TTabItem;
    tabMais: TTabItem;
    cNotificacao: TCircle;
    rectToolbarDashboard: TRectangle;
    Label1: TLabel;
    rectToolbarPedidos: TRectangle;
    Label2: TLabel;
    rectToolbarClientes: TRectangle;
    Label3: TLabel;
    rectToolbarNotificacoes: TRectangle;
    Label4: TLabel;
    rectToolbarMais: TRectangle;
    Label5: TLabel;
    Layout1: TLayout;
    Label6: TLabel;
    Layout2: TLayout;
    VertScrollBox1: TVertScrollBox;
    Label7: TLabel;
    Layout3: TLayout;
    Image1: TImage;
    Image2: TImage;
    rectBuscaPedido: TRectangle;
    StyleBook1: TStyleBook;
    edtBuscarPedidos: TEdit;
    btnBuscarPedidos: TSpeedButton;
    lvPedido: TListView;
    btnAddPedido: TSpeedButton;
    Image3: TImage;
    Rectangle1: TRectangle;
    edtBuscarClientes: TEdit;
    btnBuscarClientes: TSpeedButton;
    btnAddCliente: TSpeedButton;
    Image4: TImage;
    lvClientes: TListView;
    lvNotificacoes: TListView;
    ListBox1: TListBox;
    lbiProdutos: TListBoxItem;
    lbiPerfil: TListBoxItem;
    lbiSenha: TListBoxItem;
    lbiSincronizar: TListBoxItem;
    lbiLogout: TListBoxItem;
    Image5: TImage;
    Label8: TLabel;
    Image6: TImage;
    Image7: TImage;
    Image8: TImage;
    Image9: TImage;
    Image10: TImage;
    Image11: TImage;
    Label9: TLabel;
    Image12: TImage;
    Label10: TLabel;
    Image13: TImage;
    Label11: TLabel;
    Image14: TImage;
    Label12: TLabel;
    Line1: TLine;
    Line2: TLine;
    Line3: TLine;
    Line4: TLine;
    Line5: TLine;
    imgIconeCliente: TImage;
    imgIconeData: TImage;
    imgIconeSincronizar: TImage;
    imgIconeEndereco: TImage;
    imgIconeFone: TImage;
    imgIconeMenu: TImage;
    Image16: TImage;
    imgSemPedido: TImage;
    imgSemCliente: TImage;
    imgSemNotificacoes: TImage;
    procedure imgAbaDashboardClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnBuscarPedidosClick(Sender: TObject);
    procedure lvPedidoPaint(Sender: TObject; Canvas: TCanvas;
      const ARect: TRectF);
    procedure btnBuscarClientesClick(Sender: TObject);
    procedure lvClientesPaint(Sender: TObject; Canvas: TCanvas;
      const ARect: TRectF);
    procedure lvNotificacoesPaint(Sender: TObject; Canvas: TCanvas;
      const ARect: TRectF);
    procedure lvNotificacoesUpdateObjects(const Sender: TObject;
      const AItem: TListViewItem);
    procedure lvClientesUpdateObjects(const Sender: TObject;
      const AItem: TListViewItem);
    procedure lbiProdutosClick(Sender: TObject);
    procedure btnAddClienteClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure lvClientesItemClick(const Sender: TObject;
      const AItem: TListViewItem);
    procedure lvNotificacoesItemClickEx(const Sender: TObject;
      ItemIndex: Integer; const LocalClickPos: TPointF;
      const ItemObject: TListItemDrawable);
    procedure lbiPerfilClick(Sender: TObject);
    procedure lbiSenhaClick(Sender: TObject);
    procedure lbiLogoutClick(Sender: TObject);
    procedure lbiSincronizarClick(Sender: TObject);
    procedure btnAddPedidoClick(Sender: TObject);
    procedure lvPedidoItemClick(const Sender: TObject;
      const AItem: TListViewItem);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    thread_notificacao: TThread;
    fancy : TFancyDialog;
    menu_notificacao: TActionSheet;
    procedure AbrirAba(img: TImage);
    procedure addPedidoListView(pedido_local, pedido_oficial, cliente, dt_pedido, ind_sincronizar: string; valor: double);
    procedure addClienteListView(cod_cliente_local, nome, endereco, numero, bairro, complemento, cidade, uf, fone, ind_sincronizar: string);
    procedure addNotificaoListView(cod_notificacao, dt, titulo, texto, ind_lido: string);
    procedure ListarPedidos(pagina: integer; busca: string; ind_clear: boolean);
    procedure ListarClientes(pagina: integer; busca: string; ind_clear: boolean);
    procedure ListarNotificacoes(pagina: integer; ind_clear: boolean);
    procedure ThreadPedidosTerminate(Sender: TObject);
    procedure ThreadClientesTerminate(Sender: TObject);
    procedure ThreadNotificacoesTerminate(Sender: TObject);
    procedure LayoutListViewNotificacao(AItem: TListViewItem);
    procedure LayoutListViewCliente(AItem: TListViewItem);
    procedure RefreshListagemCliente;
    procedure ClickNotificacaoExcluir(Sender: TObject);
    procedure ClickNotificacaoLida(Sender: TObject);
    procedure ClickNotificacaoNaoLida(Sender: TObject);
    procedure RefreshListagemPedido;
    procedure StartThreadNotificacao;
    procedure SincronizarCondPagto;
    procedure ThreadCondPagtoTerminate(Sender: TObject);
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmPrincipal: TFrmPrincipal;

implementation

uses
  DataModule.Pedido, DataModule.Cliente, DataModule.Notificacao, UntProduto,
  UntProdutoCad, UntClienteCad, UntPerfilCad, UntSenhaCad, DataModule.Usuario,
  UntLogin, DataModule.Produto, UntSincronizacao, UntPedidoCad, uConstantes,
  DataModule.Global, uSession;

{$R *.fmx}

procedure TFrmPrincipal.addPedidoListView(
                 pedido_local, pedido_oficial, cliente,
                 dt_pedido, ind_sincronizar: string; valor: double );
var item: TListViewItem;
    txt : tListItemText;
    img : TListItemImage;
begin
   try
     item := lvPedido.Items.Add;
     item.TagString := pedido_local;

     // Pedido
     txt := tListItemText(item.Objects.FindDrawable('txtPedido'));
     if (trim(pedido_oficial) <> emptyStr) then begin
        txt.Text := 'Pedido #' + pedido_oficial;
     end else begin
        txt.Text := 'Or�amento #' + pedido_local;
     end;

     // Cliente
     txt := tListItemText(item.Objects.FindDrawable('txtCliente'));
     txt.Text := cliente;

     // Data Pedido
     txt := tListItemText(item.Objects.FindDrawable('txtData'));
     txt.Text := dt_pedido;

     // Valor do Pedido
     txt := tListItemText(item.Objects.FindDrawable('txtValor'));
     txt.Text := 'R$ '+ FormatFloat('###,###,##0.00', valor);

     // Icone Cliente
     img := TListItemImage(item.Objects.FindDrawable('imgCliente'));
     img.Bitmap := imgIconeCliente.Bitmap;

     // Icone Data
     img := TListItemImage(item.Objects.FindDrawable('imgData'));
     img.Bitmap := imgIconeData.Bitmap;

     // Icone Sincroniza��o
     if ind_sincronizar = 'S' then begin
        img := TListItemImage(item.Objects.FindDrawable('imgSincronizar'));
        img.Bitmap := imgIconeSincronizar.Bitmap;
     end;

   except on ex: exception do
     begin
       fancy.Show(TIconDialog.Error, 'Aviso', 'Erro ao inserir pedido na lista: '+ex.Message, 'OK');
     end;
   end;
end;


procedure TFrmPrincipal.addClienteListView(
                 cod_cliente_local, nome, endereco, numero, bairro,
                 complemento, cidade, uf, fone, ind_sincronizar: string);
var item: TListViewItem;
    txt : tListItemText;
    img : TListItemImage;
begin
   try
     item := lvClientes.Items.Add;
     item.Height := 90;
     item.TagString := cod_cliente_local;

     //Nome Cliente
     txt := tListItemText(item.Objects.FindDrawable('txtNome'));
     txt.Text := nome;

     //Endere�o Completo ...
     txt := tListItemText(item.Objects.FindDrawable('txtEndereco'));
     txt.Text := endereco;

     if not numero.IsEmpty then begin
        txt.Text := txt.Text +', '+ numero;
     end;

     if not complemento.IsEmpty then begin
        txt.Text := txt.Text +', '+ complemento;
     end;

     if not bairro.IsEmpty then begin
        txt.Text := txt.Text +', '+ bairro;
     end;

     if not cidade.IsEmpty then begin
        txt.Text := txt.Text +', '+ cidade;
     end;

     if not uf.IsEmpty then begin
        txt.Text := txt.Text +', '+ uf;
     end;

     //Fone ...
     txt := tListItemText(item.Objects.FindDrawable('txtFone'));
     txt.Text := fone;

     // Icone Endere�o
     img := TListItemImage(item.Objects.FindDrawable('imgEndereco'));
     img.Bitmap := imgIconeEndereco.Bitmap;

     // Icone Fone
     img := TListItemImage(item.Objects.FindDrawable('imgFone'));
     img.Bitmap := imgIconeFone.Bitmap;

     // Icone Sincroniza��o
     if ind_sincronizar = 'S' then begin
        img := TListItemImage(item.Objects.FindDrawable('imgSincronizar'));
        img.Bitmap := imgIconeSincronizar.Bitmap;
     end;

     LayoutListViewCliente(item);

   except on ex: exception do
     begin
       fancy.Show(TIconDialog.Error, 'Aviso', 'Erro ao inserir clientes na lista: '+ex.Message, 'OK');
     end;
   end;
end;

procedure TFrmPrincipal.addNotificaoListView(cod_notificacao, dt, titulo, texto, ind_lido: string);
var item: TListViewItem;
    txt : tListItemText;
    img : TListItemImage;
begin
   try
     item := lvNotificacoes.Items.Add;
     item.Height := 90;
     item.TagString := cod_notificacao;

     //T�tulo
     txt := tListItemText(item.Objects.FindDrawable('txtTitulo'));
     txt.Text := titulo;
     txt.TagString := ind_lido;

     //Data da Notifica��o
     txt := tListItemText(item.Objects.FindDrawable('txtData'));
     txt.Text := dt;

     //Mensagem
     txt := tListItemText(item.Objects.FindDrawable('txtMessage'));
     txt.Text := texto;

     //Icone Data
     img := TListItemImage(item.Objects.FindDrawable('imgData'));
     img.Bitmap := imgIconeData.Bitmap;

     //Icone Menu
     img := TListItemImage(item.Objects.FindDrawable('imgMenu'));
     img.Bitmap := imgIconeMenu.Bitmap;
     img.TagString := cod_notificacao;

     LayoutListViewNotificacao(item);

   except on ex: exception do
     begin
       fancy.Show(TIconDialog.Error, 'Aviso', 'Erro ao inserir notifica��es na lista: '+ex.Message, 'OK');
     end;
   end;
end;


procedure TFrmPrincipal.RefreshListagemCliente;
begin
   ListarClientes(1, edtBuscarClientes.Text, true);
end;

procedure TFrmPrincipal.btnAddClienteClick(Sender: TObject);
begin
   if not Assigned(FrmClienteCad) then begin
      Application.CreateForm(TFrmClienteCad, FrmClienteCad);
   end;

   FrmClienteCad.modo := 'I';
   FrmClienteCad.Cod_Cliente    := 0;
   FrmClienteCad.ExecuteOnClose := RefreshListagemCliente;
   FrmClienteCad.show;
end;

procedure TFrmPrincipal.btnAddPedidoClick(Sender: TObject);
begin
   if not Assigned(FrmPedidoCad) then begin
      Application.CreateForm(TFrmPedidoCad, FrmPedidoCad);
   end;

   FrmPedidoCad.Cod_Pedido := 0;
   FrmPedidoCad.Modo       := 'I';
   FrmPedidoCad.ExecuteOnClose := RefreshListagemPedido;
   FrmPedidoCad.show;
end;

procedure TFrmPrincipal.btnBuscarClientesClick(Sender: TObject);
begin
   ListarClientes(1, edtBuscarClientes.Text, true);
end;

procedure TFrmPrincipal.btnBuscarPedidosClick(Sender: TObject);
begin
   ListarPedidos(1, edtBuscarPedidos.Text, true);
end;

procedure TFrmPrincipal.AbrirAba(img: TImage);
begin
   imgAbaDashboard.Opacity    := 0.5;
   imgAbaPedidos.Opacity      := 0.5;
   imgAbaClientes.Opacity     := 0.5;
   imgAbaNotifica��es.Opacity := 0.5;
   imgAbaMais.Opacity         := 0.5;

   img.Opacity := 1.00;

   TabControl.GotoVisibleTab(img.Tag);

   //Aba Clientes
  if (img = imgAbaClientes) then begin
    ListarClientes(1, '', true);
  end;

   //Aba Notifica��es
  if (img = imgAbaNotifica��es) then begin
    ListarNotificacoes(1, true);
  end;
end;

procedure TFrmPrincipal.ClickNotificacaoExcluir(Sender: TObject);
begin
   menu_notificacao.HideMenu;
   dtmNotificacao.ExcluirNotificacao(menu_notificacao.TagString.ToInteger());
   ListarNotificacoes(1, true);
end;

procedure TFrmPrincipal.ClickNotificacaoLida(Sender: TObject);
begin
   menu_notificacao.HideMenu;
   dtmNotificacao.MarcarNotificacaoLida(menu_notificacao.TagString.ToInteger(), true);
   ListarNotificacoes(1, true);
end;


procedure TFrmPrincipal.ClickNotificacaoNaoLida(Sender: TObject);
begin
   menu_notificacao.HideMenu;
   dtmNotificacao.MarcarNotificacaoLida(menu_notificacao.TagString.ToInteger(), false);
   ListarNotificacoes(1, true);
end;

procedure TFrmPrincipal.FormClose(Sender: TObject; var Action: TCloseAction);
begin
   thread_notificacao.terminate;
   thread_notificacao.disposeOf;
end;

procedure TFrmPrincipal.FormCreate(Sender: TObject);
begin
   //Cria todos os m�dulos de dados
   if not Assigned(dtmUsuario) then
      Application.CreateForm(TdtmUsuario, dtmUsuario);
   if not Assigned(dtmCliente) then
      Application.CreateForm(TdtmCliente, dtmCliente);
   if not Assigned(dtmProduto) then
      Application.CreateForm(TdtmProduto, dtmProduto);
   if not Assigned(dtmNotificacao) then
      Application.CreateForm(TdtmNotificacao, dtmNotificacao);
   if not Assigned(dtmPedido) then
      Application.CreateForm(TdtmPedido, dtmPedido);


   fancy := TFancyDialog.Create(FrmPrincipal);

   menu_notificacao := TActionSheet.Create(FrmPrincipal);
   menu_notificacao.TitleFontSize := 12;
   menu_notificacao.TitleMenuText := 'O que deseja fazer?';
   menu_notificacao.TitleFontColor:= $FFA3A3A3;

   menu_notificacao.CancelMenuText := 'Cancelar';
   menu_notificacao.CancelFontSize := 15;
   menu_notificacao.CancelFontColor:= $FF4162FF;

   menu_notificacao.BackgroundOpacity := 0.5;
   menu_notificacao.MenuColor := $FFFFFFFF;

   menu_notificacao.AddItem('', 'Excluir', ClickNotificacaoExcluir, $FFDA4F3F, 15);
   menu_notificacao.AddItem('', 'Marcar como Lida', ClickNotificacaoLida);
   menu_notificacao.AddItem('', 'Marcar como n�o Lida', ClickNotificacaoNaoLida);
end;

procedure TFrmPrincipal.FormDestroy(Sender: TObject);
begin
   fancy.DisposeOf;
   menu_notificacao.DisposeOf;
end;

procedure TFrmPrincipal.ThreadCondPagtoTerminate(Sender: TObject);
begin
   TLoading.Hide;
   ListarPedidos(1, '', true);
end;

procedure TFrmPrincipal.SincronizarCondPagto;
var t: TThread;
begin
   TLoading.Show(FrmPrincipal, '');

   t := TThread.CreateAnonymousThread(procedure
   begin
      dtmPedido.ListarCondPagtoWS;
      dtmPedido.ExcluirCondPagamento;
      while not dtmPedido.TabCondPagto.eof do begin
         dtmPedido.InserirEditarCondPagamento(
            dtmPedido.TabCondPagto.fieldByName('cod_cond_pagto').asInteger,
            dtmPedido.TabCondPagto.fieldByName('cond_pagto').asString
         );
         dtmPedido.TabCondPagto.next;
      end;
   end);

   t.OnTerminate := ThreadCondPagtoTerminate;
   t.start;
end;

procedure TFrmPrincipal.FormShow(Sender: TObject);
begin
   AbrirAba(imgAbaDashboard);

   SincronizarCondPagto;
end;

procedure TFrmPrincipal.imgAbaDashboardClick(Sender: TObject);
begin
   AbrirAba(TImage(Sender));
end;

procedure TFrmPrincipal.ListarClientes(pagina: integer; busca: string; ind_clear: boolean);
var t : TThread;
begin
   imgSemCliente.Visible := false;

   //Evitar processamento concorrente ...
   if lvClientes.TagString = 'S' then exit;

   //Em processamento
   lvClientes.TagString := 'S';

   lvClientes.BeginUpdate;
   if ind_clear then begin
      pagina := 1;
      lvClientes.ScrollTo(0); //Volta para o primeiro item da lista
      lvClientes.Items.Clear;
   end;

   {
    Tag: Cont�m a pagina atual solicitada ao servidor ...
    >= 1 : Faz o request para buscar mais dados
     - 1 : indica que n�o tem mais dados
   }
   //Salva a pagina atual a ser exibida
   lvClientes.Tag := pagina;

   //Requisi��o por mais dados
   t := TThread.CreateAnonymousThread(procedure
   begin
      dtmCliente.ListarClientes(pagina, busca, '');
   end);

   t.OnTerminate := ThreadClientesTerminate;
   t.Start;
end;

procedure TFrmPrincipal.ListarNotificacoes(pagina: integer; ind_clear: boolean);
var t : TThread;
begin
   imgSemNotificacoes.Visible := false;

   //Evitar processamento concorrente ...
   if lvNotificacoes.TagString = 'S' then exit;

   //Em processamento
   lvNotificacoes.TagString := 'S';

   lvNotificacoes.BeginUpdate;
   if ind_clear then begin
      pagina := 1;
      lvNotificacoes.ScrollTo(0); //Volta para o primeiro item da lista
      lvNotificacoes.Items.Clear;
   end;

   {
    Tag: Cont�m a pagina atual solicitada ao servidor ...
    >= 1 : Faz o request para buscar mais dados
     - 1 : indica que n�o tem mais dados
   }
   //Salva a pagina atual a ser exibida
   lvNotificacoes.Tag := pagina;

   //Requisi��o por mais dados
   t := TThread.CreateAnonymousThread(procedure
   begin
      dtmNotificacao.ListarNotificacoes(pagina);
   end);

   t.OnTerminate := ThreadNotificacoesTerminate;
   t.Start;
end;

procedure TFrmPrincipal.ListarPedidos(pagina: integer; busca: string; ind_clear: boolean);
var t : TThread;
begin
   imgSemPedido.Visible := false;

   //Evitar processamento concorrente ...
   if lvPedido.TagString = 'S' then exit;

   //Em processamento
   lvPedido.TagString := 'S';

   lvPedido.BeginUpdate;
   if ind_clear then begin
      pagina := 1;
      lvPedido.ScrollTo(0); //Volta para o primeiro item da lista
      lvPedido.Items.Clear;
   end;

   {
    Tag: Cont�m a pagina atual solicitada ao servidor ...
    >= 1 : Faz o request para buscar mais dados
     - 1 : indica que n�o tem mais dados
   }
   //Salva a pagina atual a ser exibida
   lvPedido.Tag := pagina;

   //Requisi��o por mais dados
   t := TThread.CreateAnonymousThread(procedure
   begin
      dtmPedido.ListarPedidos(pagina, busca);
   end);

   t.OnTerminate := ThreadPedidosTerminate;
   t.Start;
end;

procedure TFrmPrincipal.lvClientesItemClick(const Sender: TObject;
  const AItem: TListViewItem);
begin
   if not Assigned(FrmClienteCad) then begin
      Application.CreateForm(TFrmClienteCad, FrmClienteCad);
   end;

   FrmClienteCad.modo := 'A';
   FrmClienteCad.Cod_Cliente    := AItem.tagString.ToInteger;
   FrmClienteCad.ExecuteOnClose := RefreshListagemCliente;
   FrmClienteCad.show;
end;

procedure TFrmPrincipal.lvClientesPaint(Sender: TObject; Canvas: TCanvas;
  const ARect: TRectF);
begin
   //Verifica se a Rolagem atingiu o limite para uma nova Carga
   if (lvClientes.Items.Count >= QTD_REG_PAGINA_CLIENTES) and (lvClientes.Tag >= 0) then begin

      //Monitora o 10� Item, quando a parte de baixo do elemento entrar na ListView, � executado uma nova consulta
      if (lvClientes.GetItemRect(lvClientes.Items.Count - 5).Bottom <= lvClientes.Height) then begin
         ListarClientes(lvClientes.Tag + 1, edtBuscarClientes.Text, false);
      end;

   end;
end;

procedure TFrmPrincipal.lvClientesUpdateObjects(const Sender: TObject; const AItem: TListViewItem);
begin
   LayoutListViewCliente(AItem);
end;

procedure TFrmPrincipal.lvNotificacoesItemClickEx(const Sender: TObject;
  ItemIndex: Integer; const LocalClickPos: TPointF;
  const ItemObject: TListItemDrawable);
begin
    if Assigned(ItemObject) then
        if ItemObject.Name = 'imgMenu' then
        begin
            menu_notificacao.TagString := ItemObject.TagString; // Cod Notificacao...
            menu_notificacao.ShowMenu;
        end;
end;

procedure TFrmPrincipal.lvNotificacoesPaint(Sender: TObject; Canvas: TCanvas; const ARect: TRectF);
begin
   //Verifica se a Rolagem atingiu o limite para uma nova Carga
   if (lvNotificacoes.Items.Count >= QTD_REG_PAGINA_NOTIFICACOES) and (lvNotificacoes.Tag >= 0) then begin

      //Monitora o 10� Item, quando a parte de baixo do elemento entrar na ListView, � executado uma nova consulta
      if (lvNotificacoes.GetItemRect(lvNotificacoes.Items.Count - 5).Bottom <= lvNotificacoes.Height) then begin
         ListarNotificacoes(lvNotificacoes.Tag + 1, false);
      end;

   end;
end;

procedure TFrmPrincipal.LayoutListViewNotificacao(AItem: TListViewItem);
var txt: TListItemText;
begin
   txt := TListItemText(AItem.Objects.FindDrawable('txtTitulo'));
   if (txt.TagString = 'N') then begin
     txt.Font.Style := [TFontStyle.fsBold];
   end;

   txt := TListItemText(AItem.Objects.FindDrawable('txtMessage'));
   txt.Width := lvNotificacoes.Width - txt.PlaceOffset.X - txt.PlaceOffset.Y; //X e Y S�o as bordas do Componente
   txt.Height:= GetTextHeight(txt, txt.width, txt.text) + 5; // 5 - Valor de seguran�a para n�o cortar o Texto do Componente

   AItem.Height := Trunc(txt.PlaceOffset.Y + txt.Height);
end;

procedure TFrmPrincipal.lbiLogoutClick(Sender: TObject);
begin
   try
     dtmUsuario.Logout;

     //Limpa os dados da sess�o do Usu�rio
     TSession.COD_USUARIO := 0;
     TSession.NOME        := '';
     TSession.EMAIL       := '';
     TSession.TOKEN_JWT   := '';
     ////////////

     if not Assigned(FrmLogin) then begin
        Application.CreateForm(TFrmLogin, FrmLogin);
     end;

     //Transfere para o Login o MainForm
     Application.MainForm := FrmLogin;

     FrmLogin.Show;
     FrmPrincipal.Close;
   except on e: Exception do
     begin
       fancy.Show(TIconDialog.Error, 'Erro', 'Erro ao fazer logout: '+e.Message, 'Ok');
     end;
   end;
end;

procedure TFrmPrincipal.lbiPerfilClick(Sender: TObject);
begin
   if not Assigned(FrmPerfilCad) then begin
      Application.CreateForm(TFrmPerfilCad, FrmPerfilCad);
   end;

   FrmPerfilCad.Show;
end;

procedure TFrmPrincipal.lbiProdutosClick(Sender: TObject);
begin
   if not (Assigned(FrmProduto)) then begin
      Application.CreateForm(TFrmProduto, FrmProduto);
   end;

   FrmProduto.Show;
end;

procedure TFrmPrincipal.lbiSenhaClick(Sender: TObject);
begin
   if not Assigned(FrmSenhaCad) then begin
      Application.CreateForm(TFrmSenhaCad, FrmSenhaCad);
   end;

   FrmSenhaCad.Show;
end;

procedure TFrmPrincipal.lbiSincronizarClick(Sender: TObject);
begin
   if not Assigned(FrmSincronizacao) then begin
      Application.CreateForm(TFrmSincronizacao, FrmSincronizacao);
   end;

   FrmSincronizacao.Show;
end;

procedure TFrmPrincipal.LayoutListViewCliente(AItem: TListViewItem);
var txt: TListItemText;
begin
   txt := TListItemText(AItem.Objects.FindDrawable('txtEndereco'));
   txt.Width := lvClientes.Width - txt.PlaceOffset.X - txt.PlaceOffset.Y; //X e Y S�o as bordas do Componente
   txt.Height:= GetTextHeight(txt, txt.width, txt.text) + 5; // 5 - Valor de seguran�a para n�o cortar o Texto do Componente

   AItem.Height := Trunc(txt.PlaceOffset.Y + txt.Height);
end;

procedure TFrmPrincipal.lvNotificacoesUpdateObjects(const Sender: TObject; const AItem: TListViewItem);
begin
   //Evento disparado de acordo com a altera��o da Tela, aqui � tratado a responsividade do Form no App
   LayoutListViewNotificacao(AItem);
end;

procedure TFrmPrincipal.RefreshListagemPedido;
begin
   ListarPedidos(1, edtBuscarPedidos.text, true);
end;

procedure TFrmPrincipal.lvPedidoItemClick(const Sender: TObject;
  const AItem: TListViewItem);
begin
   if not Assigned(FrmPedidoCad) then begin
      Application.CreateForm(TFrmPedidoCad, FrmPedidoCad);
   end;

   FrmPedidoCad.Cod_Pedido := AItem.TagString.ToInteger();
   FrmPedidoCad.Modo       := 'A';
   FrmPedidoCad.ExecuteOnClose := RefreshListagemPedido;
   FrmPedidoCad.show;
end;

procedure TFrmPrincipal.lvPedidoPaint(Sender: TObject; Canvas: TCanvas; const ARect: TRectF);
begin
   //Verifica se a Rolagem atingiu o limite para uma nova Carga
   if (lvPedido.Items.Count >= QTD_REG_PAGINA_PEDIDOS) and (lvPedido.Tag >= 0) then begin

      //Monitora o 10� Item, quando a parte de baixo do elemento entrar na ListView, � executado uma nova consulta
      if (lvPedido.GetItemRect(lvPedido.Items.Count - 5).Bottom <= lvPedido.Height) then begin
         ListarPedidos(lvPedido.Tag + 1, edtBuscarPedidos.Text, false);
      end;

   end;
end;

procedure TFrmPrincipal.ThreadClientesTerminate(Sender: TObject);
begin
   //N�o carregar mais Dados
   if (dtmCliente.qryCnsCliente.RecordCount < QTD_REG_PAGINA_CLIENTES) then lvClientes.Tag := -1;

   while not dtmCliente.qryCnsCliente.eof do begin
      addClienteListView(
         dtmCliente.qryCnsCliente.fieldByName('cod_cliente_local').asString,
         dtmCliente.qryCnsCliente.fieldByName('nome').asString,
         dtmCliente.qryCnsCliente.fieldByName('endereco').asString,
         dtmCliente.qryCnsCliente.fieldByName('numero').asString,
         dtmCliente.qryCnsCliente.fieldByName('complemento').asString,
         dtmCliente.qryCnsCliente.fieldByName('bairro').asString,
         dtmCliente.qryCnsCliente.fieldByName('cidade').asString,
         dtmCliente.qryCnsCliente.fieldByName('uf').asString,
         dtmCliente.qryCnsCliente.fieldByName('fone').asString,
         dtmCliente.qryCnsCliente.fieldByName('ind_sincronizar').asString
      );
      dtmCliente.qryCnsCliente.next;
   end;

   lvClientes.EndUpdate;

   //Marca que o processo terminou
   lvClientes.TagString := '';

   //Caso n�o tenha registros, � apresentado a imagem ao usu�rio
   imgSemCliente.Visible := (lvClientes.items.Count = 0);

   //Se deu erro na Thread, � apresentado a mensagem
   if Sender is TThread then begin
      if Assigned(TThread(Sender).FatalException) then begin
         fancy.Show(TIconDialog.Error, 'Aviso', Exception(TThread(sender).FatalException).Message, 'OK');
         exit;
      end;
   end;
end;

procedure TFrmPrincipal.ThreadNotificacoesTerminate(Sender: TObject);
var iCont: integer;
begin
   //N�o carregar mais Dados
   if (dtmNotificacao.qryCnsNotificacao.RecordCount < QTD_REG_PAGINA_NOTIFICACOES) then lvNotificacoes.Tag := -1;

   iCont := 0;
   while not dtmNotificacao.qryCnsNotificacao.eof do begin
      addNotificaoListView(
         dtmNotificacao.qryCnsNotificacao.fieldByName('cod_notificacao').asString,
         formatDateTime('dd/mm/yy hh:nn', dtmNotificacao.qryCnsNotificacao.fieldByName('data_notificacao').asDateTime),
         dtmNotificacao.qryCnsNotificacao.fieldByName('titulo').asString,
         dtmNotificacao.qryCnsNotificacao.fieldByName('texto').asString,
         dtmNotificacao.qryCnsNotificacao.fieldByName('ind_lido').asString
      );

      if (dtmNotificacao.qryCnsNotificacao.fieldByName('ind_lido').asString = 'N') then begin
         Inc(iCont);
      end;

      dtmNotificacao.qryCnsNotificacao.next;
   end;

   lvNotificacoes.EndUpdate;

   //Habilita o Circulo Vermelho
   cNotificacao.Visible := (iCont > 0);

   //Marca que o processo terminou
   lvNotificacoes.TagString := '';

   //Caso n�o tenha registros, � apresentado a imagem ao usu�rio
   imgSemNotificacoes.Visible := (lvNotificacoes.Items.Count = 0);

   //Se feu erro na Thread, � apresentado a mensagem
   if Sender is TThread then begin
      if Assigned(TThread(Sender).FatalException) then begin
         fancy.Show(TIconDialog.Error, 'Aviso', Exception(TThread(sender).FatalException).Message, 'OK');
         exit;
      end;
   end;
end;

procedure TFrmPrincipal.StartThreadNotificacao;
begin
    thread_notificacao := TThread.CreateAnonymousThread(procedure
    var
        cont: integer;
    begin
        cont := 0;

        while NOT thread_notificacao.CheckTerminated do
        begin
            sleep(1000);
            inc(cont);

            if cont >= 5 then
            begin
                // Busca por novas notificacoes...
                DtmNotificacao.ListarNotificacoesWS;

                with DtmNotificacao.TabNotificacoes do
                begin
                    while NOT EOF do
                    begin
                        DtmNotificacao.InserirNotificacao(FieldByName('cod_notificacao').AsInteger,
                                                         FormataData(FieldByName('data_notificacao').AsString),
                                                         FieldByName('titulo').AsString,
                                                         FieldByName('texto').AsString);

                        Next;
                    end;
                end;

                if DtmNotificacao.TabNotificacoes.RecordCount > 0 then
                    TThread.Synchronize(TThread.CurrentThread, procedure
                    begin
                        cNotificacao.Visible := true;
                    end);

                cont := 0;
            end;
        end;
    end);

    thread_notificacao.FreeOnTerminate := false;
    thread_notificacao.Start;
end;

procedure TFrmPrincipal.ThreadPedidosTerminate(Sender: TObject);
begin
   //N�o carregar mais Dados
   if (dtmPedido.qryCnsPedido.RecordCount < QTD_REG_PAGINA_PEDIDOS) then lvPedido.Tag := -1;

   while not dtmPedido.qryCnsPedido.eof do begin
      addPedidoListView(
         dtmPedido.qryCnsPedido.fieldByName('cod_pedido_local').asString,
         dtmPedido.qryCnsPedido.fieldByName('cod_pedido_oficial').asString,
         dtmPedido.qryCnsPedido.fieldByName('nome').asString,
         FormatDateTime('dd/mm/yyyy', dtmPedido.qryCnsPedido.fieldByName('data_pedido').asDateTime),
         dtmPedido.qryCnsPedido.fieldByName('ind_sincronizar').asString,
         dtmPedido.qryCnsPedido.fieldByName('valor_total').asfloat
      );
      dtmPedido.qryCnsPedido.next;
   end;

   lvPedido.EndUpdate;

   //Marca que o processo terminou
   lvPedido.TagString := '';

   //Caso n�o tenha registros, � apresentado a imagem ao usu�rio
   imgSemPedido.Visible := (lvPedido.items.Count = 0);

   //Se deu erro na Thread, � apresentado a mensagem
   if Sender is TThread then begin
      if Assigned(TThread(Sender).FatalException) then begin
         fancy.Show(TIconDialog.Error, 'Aviso', Exception(TThread(sender).FatalException).Message, 'OK');
         exit;
      end;
   end;

   //Ao consultar os Pedidos, � Atualizado as Notifica��es
   ListarNotificacoes(1, true);

   //Dispara a Thread respons�vel por ficar consultando as notifica��es
   StartThreadNotificacao;
end;

end.
