unit UntSincronizacao;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.Objects,
  FMX.StdCtrls, FMX.Controls.Presentation, FMX.Layouts, uFancyDialog, uLoading,
  uFunctions, uSession;

type
  TFrmSincronizacao = class(TForm)
    rectToolbarProdutos: TRectangle;
    lblTitulo: TLabel;
    btnSalvar: TSpeedButton;
    btnVoltar: TSpeedButton;
    Image1: TImage;
    btnSincronizar: TSpeedButton;
    lytSync: TLayout;
    lblCliente: TLabel;
    lytCliente: TLayout;
    Image2: TImage;
    Label2: TLabel;
    lblPedido: TLabel;
    lytProduto: TLayout;
    Image3: TImage;
    Label4: TLabel;
    lblProduto: TLabel;
    lytPedidos: TLayout;
    Image4: TImage;
    Label6: TLabel;
    procedure btnVoltarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure btnSincronizarClick(Sender: TObject);
  private
    { Private declarations }
    fancy : TFancyDialog;
    dt_servidor : string;
    procedure ThreadSincTerminate(Sender: TObject);
    procedure FecharTela(Sender: TObject);
    procedure ObterDataServidor;
    procedure DownloadClientes(dt_ult_sinc: string);
    procedure UploadClientes(dt_ult_sinc: string);
    procedure FinalizarSinc(dt_ult_sinc: string);
    procedure DownloadProdutos(dt_ult_sinc: string);
    procedure UploadProdutos(dt_ult_sinc: string);
    procedure DownloadFotos(cod_produto_oficial: integer);
  public
    { Public declarations }
  end;

var
  FrmSincronizacao: TFrmSincronizacao;

implementation

{$R *.fmx}

uses UntPrincipal, DataModule.Global, DataModule.Usuario, DataModule.Cliente,
  DataModule.Produto, uConstantes;

procedure TFrmSincronizacao.FecharTela(Sender: TObject);
begin
   close;
end;

procedure TFrmSincronizacao.ThreadSincTerminate(Sender: TObject);
begin
   TLoading.Hide;

   //Se deu erro na Thread, � apresentado a mensagem
   if Sender is TThread then begin
      if Assigned(TThread(Sender).FatalException) then begin
         fancy.Show(TIconDialog.Error, 'Aviso', Exception(TThread(sender).FatalException).Message, 'OK');
         exit;
      end;
   end;

   //showmessage(dt_servidor);
   fancy.Show(TIconDialog.Success, 'Sincroniza��o', 'Sincroniza��o conclu�da com sucesso!', 'OK', FecharTela);
end;

procedure TFrmSincronizacao.ObterDataServidor;
begin
   TThread.Synchronize(TThread.CurrentThread, procedure
   begin
      TLoading.ChangeText('Obtendo data...');
   end);

   dt_servidor := dtmUsuario.ObterDataServidorWS;
end;

procedure TFrmSincronizacao.DownloadClientes(dt_ult_sinc: string);
var pagina : integer;
    latitude, longitude : double;
    b_next_page: boolean;
begin
   TThread.Synchronize(TThread.CurrentThread, procedure
   begin
      TLoading.ChangeText('Carregando o cadastro de clientes ...');
   end);

   sleep(3000);

   b_next_page := true;
   pagina := 1;

   while b_next_page do begin

     dtmCliente.ListarClientesWS(dt_ult_sinc, pagina);
     while not dtmCliente.TabCliente.eof do begin

        latitude    := 0;
        if (dtmCliente.TabCliente.fieldByName('latitude').asString <> '') then begin
           latitude := dtmCliente.TabCliente.fieldByName('latitude').asFloat;
        end;

        longitude   := 0;
        if (dtmCliente.TabCliente.fieldByName('longitude').asString <> '') then begin
           longitude := dtmCliente.TabCliente.fieldByName('longitude').asFloat;
        end;

        //Verifica se o cliente j� tem cadastro no App
        if (dtmCliente.ListarClienteId(0, dtmCliente.TabCliente.fieldByName('cod_cliente').asinteger)) then begin
           dtmCliente.EditarClienteId(
              dtmCliente.qryCliente.fieldByName('cod_cliente_local').AsInteger,
              dtmCliente.TabCliente.fieldByName('cnpj_cpf').asString,
              dtmCliente.TabCliente.fieldByName('nome').asString,
              dtmCliente.TabCliente.fieldByName('fone').asString,
              dtmCliente.TabCliente.fieldByName('email').asString,
              dtmCliente.TabCliente.fieldByName('endereco').asString,
              dtmCliente.TabCliente.fieldByName('numero').asString,
              dtmCliente.TabCliente.fieldByName('complemento').asString,
              dtmCliente.TabCliente.fieldByName('bairro').asString,
              dtmCliente.TabCliente.fieldByName('cidade').asString,
              dtmCliente.TabCliente.fieldByName('uf').asString,
              dtmCliente.TabCliente.fieldByName('cep').asString,
              'N',
              dtmCliente.TabCliente.fieldByName('limite_disponivel').asFloat,
              latitude,
              longitude
           );
        end else begin
           dtmCliente.InserirClienteId(
              dtmCliente.TabCliente.fieldByName('cnpj_cpf').asString,
              dtmCliente.TabCliente.fieldByName('nome').asString,
              dtmCliente.TabCliente.fieldByName('fone').asString,
              dtmCliente.TabCliente.fieldByName('email').asString,
              dtmCliente.TabCliente.fieldByName('endereco').asString,
              dtmCliente.TabCliente.fieldByName('numero').asString,
              dtmCliente.TabCliente.fieldByName('complemento').asString,
              dtmCliente.TabCliente.fieldByName('bairro').asString,
              dtmCliente.TabCliente.fieldByName('cidade').asString,
              dtmCliente.TabCliente.fieldByName('uf').asString,
              dtmCliente.TabCliente.fieldByName('cep').asString,
              dtmCliente.TabCliente.fieldByName('limite_disponivel').asFloat,
              latitude,
              longitude,
              'N',
              dtmCliente.TabCliente.fieldByName('cod_cliente').asinteger
           );
        end;
        dtmCliente.TabCliente.next;
     end;

     inc(pagina);
     b_next_page := (dtmCliente.TabCliente.recordCount > 0);
   end;
end;

procedure TFrmSincronizacao.DownloadFotos(cod_produto_oficial: integer);
var foto : TBitmap;
    url  : string;
begin
   try
     foto := TBitmap.Create;
     url  := BASE_URL +'/produtos/foto/'+cod_produto_oficial.ToString;

     try
        //Faz o dowload da Foto e atualizao banco de dados
        LoadImageFromURL(foto, url, TSession.TOKEN_JWT);
        if (not foto.isEmpty) then begin
           dtmProduto.EditarFoto(cod_produto_oficial, foto);
        end;
     except

     end;
   finally
     foto.disposeOf;
   end;
end;

procedure TFrmSincronizacao.DownloadProdutos(dt_ult_sinc: string);
var pagina : integer;
    b_next_page: boolean;
begin
   TThread.Synchronize(TThread.CurrentThread, procedure
   begin
      TLoading.ChangeText('Buscanco produtos ...');
   end);

   sleep(3000);

   b_next_page := true;
   pagina := 1;

   while b_next_page do begin

     dtmProduto.ListarProdutosWS(dt_ult_sinc, pagina);
     while not dtmProduto.TabProduto.eof do begin

        //Verifica se o produto j� tem cadastro no App
        if (dtmProduto.ListarProdutoId(0, dtmProduto.TabProduto.fieldByName('cod_produto').asinteger)) then begin
           dtmProduto.EditarProdutoId(
              dtmProduto.qryProduto.fieldByName('cod_produto_local').AsInteger,
              dtmProduto.TabProduto.fieldByName('descricao').asString,
              'N',
              dtmProduto.TabProduto.fieldByName('valor').asFloat,
              dtmProduto.TabProduto.fieldByName('qtd_estoque').asFloat,
              nil
           );
        end else begin
           dtmProduto.InserirProdutoId(
              dtmProduto.TabProduto.fieldByName('descricao').asString,
              'N',
              dtmProduto.TabProduto.fieldByName('valor').asFloat,
              dtmProduto.TabProduto.fieldByName('qtd_estoque').asFloat,
              nil,
              dtmProduto.TabProduto.fieldByName('cod_produto').asinteger
           );
        end;

        //Download da Foto
        DownloadFotos(dtmProduto.TabProduto.fieldByName('cod_produto').asinteger);

        dtmProduto.TabProduto.next;
     end;

     inc(pagina);
     b_next_page := (dtmProduto.TabProduto.recordCount > 0);
   end;
end;

procedure TFrmSincronizacao.UploadClientes(dt_ult_sinc: string);
begin
   TThread.Synchronize(TThread.CurrentThread, procedure
   begin
      TLoading.ChangeText('Enviando clientes ...');
   end);

   //Carrega os Registros pendentes de Sincroniza��o
   dtmCliente.ListarClientes(0, '', 'S');
   while not dtmCliente.qryCnsCliente.Eof do begin
      sleep(500);

      //Percorre os registros, enviando o cadastro dos clientes para o Server
      dtmCliente.InserirClienteWS(
         dtmCliente.qryCnsCliente.fieldByName('cod_cliente_oficial').AsInteger,
         dtmCliente.qryCnsCliente.fieldByName('cod_cliente_local').AsInteger,
         dtmCliente.qryCnsCliente.fieldByName('cnpj_cpf').AsString,
         dtmCliente.qryCnsCliente.fieldByName('nome').AsString,
         dtmCliente.qryCnsCliente.fieldByName('fone').AsString,
         dtmCliente.qryCnsCliente.fieldByName('email').AsString,
         dtmCliente.qryCnsCliente.fieldByName('endereco').AsString,
         dtmCliente.qryCnsCliente.fieldByName('numero').AsString,
         dtmCliente.qryCnsCliente.fieldByName('complemento').AsString,
         dtmCliente.qryCnsCliente.fieldByName('bairro').AsString,
         dtmCliente.qryCnsCliente.fieldByName('cidade').AsString,
         dtmCliente.qryCnsCliente.fieldByName('uf').AsString,
         dtmCliente.qryCnsCliente.fieldByName('cep').AsString,
         dtmCliente.qryCnsCliente.fieldByName('latitude').AsFloat,
         dtmCliente.qryCnsCliente.fieldByName('longitude').AsFloat,
         dtmCliente.qryCnsCliente.fieldByName('limite_disponivel').AsFloat,
         dt_ult_sinc
      );

      //Ap�s atualizar os dados do Cliente no Server, � marcado no mobile que a Sincroniza��o foi feita
      dtmCliente.MarcarClienteSincronizado(
          dtmCliente.qryCnsCliente.fieldByName('cod_cliente_local').AsInteger,
          dtmCliente.TabCliente.fieldByName('cod_cliente').AsInteger
      );

      dtmCliente.qryCnsCliente.next;
   end;
end;

procedure TFrmSincronizacao.UploadProdutos(dt_ult_sinc: string);
var arq: string;
begin
   TThread.Synchronize(TThread.CurrentThread, procedure
   begin
      TLoading.ChangeText('Enviando produtos ...');
   end);

   //Carrega os Registros pendentes de Sincroniza��o
   dtmProduto.ListarProdutos(0, '', 'S');
   while not dtmProduto.qryCnsProduto.Eof do begin
      sleep(500);

      //Percorre os registros, enviando o cadastro dos produtos para o Server
      dtmProduto.InserirProdutoWS(
         dtmProduto.qryCnsProduto.fieldByName('cod_produto_local').AsInteger,
         dtmProduto.qryCnsProduto.fieldByName('cod_produto_oficial').AsInteger,
         dtmProduto.qryCnsProduto.fieldByName('descricao').AsString,
         dtmProduto.qryCnsProduto.fieldByName('valor').AsFloat,
         dtmProduto.qryCnsProduto.fieldByName('qtd_estoque').AsFloat,
         dt_ult_sinc
      );

      //Ap�s atualizar os dados dos Produtos no Server, � marcado no mobile que a Sincroniza��o foi feita
      dtmProduto.MarcarProdutoSincronizado(
          dtmProduto.TabProduto.fieldByName('cod_produto_local').AsInteger,
          dtmProduto.TabProduto.fieldByName('cod_produto').AsInteger
      );

      //Envio da foto do produto ...
      if (dtmProduto.qryCnsProduto.fieldByName('foto').AsString <> '') then begin

         //Salva a imagem Local para depois enviar para o Server
         arq := SaveBlobToFile(
                  dtmProduto.qryCnsProduto.fieldByName('foto'),
                  dtmProduto.qryCnsProduto.fieldByName('cod_produto_oficial').AsInteger
                );

         dtmProduto.EditarFotoWS(
            dtmProduto.qryCnsProduto.fieldByName('cod_produto_oficial').AsInteger,
            arq
         );
      end;


      dtmProduto.qryCnsProduto.next;
   end;
end;

procedure TFrmSincronizacao.FinalizarSinc(dt_ult_sinc: string);
begin
   dtmUsuario.SalvarConfig('DATA-ULT-SINC', dt_ult_sinc);
end;

procedure TFrmSincronizacao.btnSincronizarClick(Sender: TObject);
var t: TThread;
begin
   TLoading.Show(FrmSincronizacao, '');

   t := TThread.CreateAnonymousThread(procedure
   var dt_config: string;
   begin
      //Busca a data da Ultima Sincronizacao
      dt_config := dtmUsuario.BuscarConfig('DATA-ULT-SINC');

      ObterDataServidor;

      DownloadClientes(dt_config);
      UploadClientes(dt_servidor);

      DownloadProdutos(dt_config);
      UploadProdutos(dt_servidor);

      FinalizarSinc(dt_servidor);

   end);

   t.OnTerminate := ThreadSincTerminate;
   t.Start;
end;

procedure TFrmSincronizacao.btnVoltarClick(Sender: TObject);
begin
   close;
end;

procedure TFrmSincronizacao.FormClose(Sender: TObject; var Action: TCloseAction);
begin
   Action := TCloseAction.caFree;
   FrmSincronizacao := nil
end;

procedure TFrmSincronizacao.FormCreate(Sender: TObject);
begin
   fancy := TFancyDialog.Create(FrmSincronizacao);
end;

procedure TFrmSincronizacao.FormDestroy(Sender: TObject);
begin
   fancy.DisposeOf;
end;

procedure TFrmSincronizacao.FormShow(Sender: TObject);
begin
   try
     lblCliente.Text := dtmGlobal.EstatisticaSinc('tab_cliente').ToString;
     lblProduto.Text := dtmGlobal.EstatisticaSinc('tab_produto').ToString;
     lblPedido.Text  := dtmGlobal.EstatisticaSinc('tab_pedido').ToString;
   except on e: Exception do
    begin
       fancy.Show(TIconDialog.Error, 'ERRO', e.message, 'OK');
    end;
   end;
end;

end.
