unit UntProdutoBusca;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.Objects,
  FMX.StdCtrls, FMX.Controls.Presentation, FMX.Edit, FMX.ListView.Types,
  FMX.ListView.Appearances, FMX.ListView.Adapters.Base, FMX.ListView,
  uFancyDialog, uFunctions, Data.DB;

type
  TExecuteOnClick = procedure(cod_produto_local: integer;
                              descricao: string;
                              valor: double) of Object;

  TFrmProdutoBusca = class(TForm)
    rectToolbar: TRectangle;
    Label3: TLabel;
    btnVoltar: TSpeedButton;
    Image4: TImage;
    rectBusca: TRectangle;
    edtBuscaProduto: TEdit;
    btnBuscaProduto: TSpeedButton;
    lvProduto: TListView;
    imgSemProduto: TImage;
    imgIconeCamera: TImage;
    imgIconeEstoque: TImage;
    imgIconeValor: TImage;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure lvProdutoPaint(Sender: TObject; Canvas: TCanvas;
      const ARect: TRectF);
    procedure lvProdutoUpdateObjects(const Sender: TObject;
      const AItem: TListViewItem);
    procedure lvProdutoItemClick(const Sender: TObject;
      const AItem: TListViewItem);
    procedure btnBuscaProdutoClick(Sender: TObject);
    procedure btnVoltarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    fancy: TFancyDialog;
    FExecuteOnClick: TExecuteOnClick;
    procedure AddProdutoListview(cod_produto_local, descricao: string; valor,
      estoque: double; foto: TStream);
    procedure LayoutListviewProduto(AItem: TListViewItem);
    procedure ListarProdutos(pagina: integer; busca: string;
      ind_clear: boolean);
    procedure ThreadProdutosTerminate(Sender: TObject);
    { Private declarations }
  public
    property ExecuteOnClick: TExecuteOnClick read FExecuteOnClick write FExecuteOnClick;
  end;

var
  FrmProdutoBusca: TFrmProdutoBusca;

implementation

{$R *.fmx}

uses UntPrincipal, DataModule.Produto, uConstantes;

procedure TFrmProdutoBusca.AddProdutoListview(cod_produto_local, descricao: string;
                                         valor, estoque: double;
                                         foto: TStream);
var
    item: TListViewItem;
    txt: TListItemText;
    img: TListItemImage;
    bmp: TBitmap;
begin
    try
        item := lvProduto.Items.Add;

        with item do
        begin
            Height := 85;
            Tag := cod_produto_local.ToInteger;
            TagString := descricao;
            TagFloat := valor;

            // Descricao...
            txt := TListItemText(Objects.FindDrawable('txtDescricao'));
            txt.Text := descricao;

            // Valor...
            txt := TListItemText(Objects.FindDrawable('txtValor'));
            txt.Text := FormatFloat('R$#,##0.00', valor);
            txt.TagFloat := valor;

            // Estoque...
            txt := TListItemText(Objects.FindDrawable('txtEstoque'));
            txt.Text := FormatFloat('#,##', estoque);

            // Icone Valor...
            img := TListItemImage(Objects.FindDrawable('imgValor'));
            img.Bitmap := imgIconeValor.Bitmap;

            // Icone Estoque...
            img := TListItemImage(Objects.FindDrawable('imgEstoque'));
            img.Bitmap := imgIconeEstoque.Bitmap;

            // Foto...
            img := TListItemImage(Objects.FindDrawable('imgFoto'));
            if foto <> nil then
            begin
                bmp := TBitmap.Create;
                bmp.LoadFromStream(foto);

                img.OwnsBitmap := true;
                img.Bitmap := bmp;
            end
            else
                img.Bitmap := imgIconeCamera.Bitmap;
        end;

        LayoutListviewProduto(item);

    except on ex:exception do
        fancy.Show(TIconDialog.Error, 'Erro', 'Erro ao inserir pedido na lista: ' + ex.Message, 'OK');
    end;
end;

procedure TFrmProdutoBusca.btnBuscaProdutoClick(Sender: TObject);
begin
    ListarProdutos(1, edtBuscaProduto.Text, true);
end;

procedure TFrmProdutoBusca.btnVoltarClick(Sender: TObject);
begin
    close;
end;

procedure TFrmProdutoBusca.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    Action := TCloseAction.caFree;
    FrmProdutoBusca := nil;
end;

procedure TFrmProdutoBusca.FormCreate(Sender: TObject);
begin
    fancy := TFancyDialog.Create(FrmProdutoBusca);
end;

procedure TFrmProdutoBusca.FormDestroy(Sender: TObject);
begin
    fancy.DisposeOf;
end;

procedure TFrmProdutoBusca.FormShow(Sender: TObject);
begin
    ListarProdutos(1, '', true);
end;

procedure TFrmProdutoBusca.LayoutListviewProduto(AItem: TListViewItem);
var
    txt: TListItemText;
    img: TListItemImage;
    posicao_y: Extended;
begin
    txt := TListItemText(AItem.Objects.FindDrawable('txtDescricao'));
    txt.Width := lvProduto.Width - 92;
    txt.Height := GetTextHeight(txt, txt.Width, txt.Text) + 3;

    posicao_y := txt.PlaceOffset.Y + txt.Height;


    TListItemText(AItem.Objects.FindDrawable('txtValor')).PlaceOffset.Y := posicao_y;
    TListItemText(AItem.Objects.FindDrawable('txtEstoque')).PlaceOffset.Y := posicao_y;
    TListItemImage(AItem.Objects.FindDrawable('imgValor')).PlaceOffset.Y := posicao_y;
    TListItemImage(AItem.Objects.FindDrawable('imgEstoque')).PlaceOffset.Y := posicao_y;


    AItem.Height := Trunc(posicao_y + 30);
end;

procedure TFrmProdutoBusca.ThreadProdutosTerminate(Sender: TObject);
var
    foto: TStream;
begin
    // Nao carregar mais dados...
    if DtmProduto.qryCnsProduto.RecordCount < QTD_REG_PAGINA_PRODUTOS then
        lvProduto.Tag := -1;

    with DtmProduto.qryCnsProduto do
    begin
        while NOT EOF do
        begin
            if fieldbyname('foto').asstring <> '' then
                foto := DtmProduto.qryCnsProduto.CreateBlobStream(fieldbyname('foto'),
                                                                  TBlobStreamMode.bmRead)
            else
                foto := nil;

            AddProdutoListview(fieldbyname('cod_produto_local').asstring,
                               fieldbyname('descricao').asstring,
                               fieldbyname('valor').asfloat,
                               fieldbyname('qtd_estoque').asfloat,
                               foto);

            Next;
        end;
    end;

    lvProduto.EndUpdate;

    // Marcar quer o processo terminou...
    lvProduto.TagString := '';

    // Aviso de tela vazia...
    imgSemProduto.Visible := lvProduto.Items.Count = 0;

    // Deu erro na Thread?
    if Sender is TThread then
    begin
        if Assigned(TThread(Sender).FatalException) then
        begin
            fancy.Show(TIconDialog.Error, 'Erro', Exception(TThread(sender).FatalException).Message, 'OK');
            exit;
        end;
    end;
end;

procedure TFrmProdutoBusca.ListarProdutos(pagina: integer; busca: string; ind_clear: boolean);
var
    t: TThread;
begin
    imgSemProduto.Visible := false;

    // Evitar processamento concorrente...
    if lvProduto.TagString = 'S' then
        exit;

    // Em processamento...
    lvProduto.TagString := 'S';

    lvProduto.BeginUpdate;

    // Limpar a lista...
    if ind_clear then
    begin
        pagina := 1;
        lvProduto.ScrollTo(0);
        lvProduto.Items.Clear;
    end;

    {
    Tag: contem a pagina atual solicitada ao servidor...
    >= 1 : faz o request para buscar mais dados
    -1 : indica que n�o tem mais dados
    }
    // Salva a pagina atual a ser exibida...
    lvProduto.Tag := pagina;

    // Requisicao por mais dados...
    t := TThread.CreateAnonymousThread(procedure
    begin
        DtmProduto.ListarProdutos(pagina, busca, '');
    end);

    t.OnTerminate := ThreadProdutosTerminate;
    t.Start;

end;


procedure TFrmProdutoBusca.lvProdutoItemClick(const Sender: TObject;
  const AItem: TListViewItem);
begin
    if Assigned(ExecuteOnClick) then
        ExecuteOnClick(AItem.Tag, AItem.TagString,
                       TListItemText(AItem.Objects.FindDrawable('txtValor')).TagFloat);

    close;
end;

procedure TFrmProdutoBusca.lvProdutoPaint(Sender: TObject; Canvas: TCanvas;
  const ARect: TRectF);
begin
    // Verifica se a rolagem atingiu o limite para uma nova carga...
    if (lvProduto.Items.Count >= QTD_REG_PAGINA_PRODUTOS) and (lvProduto.Tag >= 0) then
        if lvProduto.GetItemRect(lvProduto.Items.Count - 5).Bottom <= lvProduto.Height then
            ListarProdutos(lvProduto.Tag + 1, edtBuscaProduto.Text, false);
end;

procedure TFrmProdutoBusca.lvProdutoUpdateObjects(const Sender: TObject;
  const AItem: TListViewItem);
begin
    LayoutListviewProduto(AItem);
end;

end.
