unit UntInicial;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.TabControl,
  FMX.Objects, FMX.Layouts, FMX.Controls.Presentation, FMX.StdCtrls, uFancyDialog;

type
  TfrmInicial = class(TForm)
    TabControl: TTabControl;
    TabItem1: TTabItem;
    TabItem2: TTabItem;
    TabItem3: TTabItem;
    TabItem4: TTabItem;
    Layout1: TLayout;
    Image1: TImage;
    Label1: TLabel;
    Label2: TLabel;
    Layout2: TLayout;
    btnProximo01: TSpeedButton;
    StyleBook1: TStyleBook;
    Layout3: TLayout;
    Image2: TImage;
    Label3: TLabel;
    Label4: TLabel;
    Layout4: TLayout;
    btnProximo02: TSpeedButton;
    btnVoltar02: TSpeedButton;
    Layout5: TLayout;
    Image3: TImage;
    Label5: TLabel;
    Label6: TLabel;
    Layout6: TLayout;
    btnProximo03: TSpeedButton;
    btnVoltar03: TSpeedButton;
    Layout7: TLayout;
    Image4: TImage;
    Label7: TLabel;
    Layout8: TLayout;
    btnCriar: TSpeedButton;
    btnAcessar: TSpeedButton;
    TimerLoad: TTimer;
    procedure btnProximo01Click(Sender: TObject);
    procedure btnVoltar02Click(Sender: TObject);
    procedure btnProximo02Click(Sender: TObject);
    procedure btnVoltar03Click(Sender: TObject);
    procedure btnProximo03Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btnAcessarClick(Sender: TObject);
    procedure btnCriarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure TimerLoadTimer(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    fancy : TFancyDialog;
    procedure AbrirAba(index: integer);
  public

  end;

var
  frmInicial: TfrmInicial;

implementation

uses UntLogin, DataModule.Usuario, UntPrincipal, uSession;

{$R *.fmx}

procedure TfrmInicial.AbrirAba(index: integer);
begin
   TabControl.GotoVisibleTab(index);
end;

procedure TfrmInicial.btnAcessarClick(Sender: TObject);
begin
   try
     //Desativar o onBoarding
     dtmUsuario.DesativarOnboarding;
   except on e: exception do
     begin
       fancy.Show(TIconDialog.Error, 'Erro', e.Message, 'Ok');
     end;
   end;

   if not Assigned(FrmLogin) then begin
      Application.CreateForm(TFrmLogin, FrmLogin);
   end;

   FrmLogin.TabControl.ActiveTab := FrmLogin.tabLogin;

   Application.MainForm := FrmLogin;
   FrmLogin.Show;
   frmInicial.Close;
end;

procedure TfrmInicial.btnCriarClick(Sender: TObject);
begin
   try
     //Desativar o onBoarding
     dtmUsuario.DesativarOnboarding;
   except on e: exception do
     begin
       fancy.Show(TIconDialog.Error, 'Erro', e.Message, 'Ok');
     end;
   end;

   if not Assigned(FrmLogin) then begin
      Application.CreateForm(TFrmLogin, FrmLogin);
   end;

   FrmLogin.TabControl.ActiveTab := FrmLogin.tabCriarConta;

   Application.MainForm := FrmLogin;
   FrmLogin.Show;
   frmInicial.Close;
end;

procedure TfrmInicial.btnProximo01Click(Sender: TObject);
begin
   AbrirAba(TSpeedButton(Sender).tag);
end;

procedure TfrmInicial.btnProximo02Click(Sender: TObject);
begin
   AbrirAba(TSpeedButton(Sender).tag);
end;

procedure TfrmInicial.btnProximo03Click(Sender: TObject);
begin
   AbrirAba(TSpeedButton(Sender).tag);
end;

procedure TfrmInicial.btnVoltar02Click(Sender: TObject);
begin
   AbrirAba(TSpeedButton(Sender).tag);
end;

procedure TfrmInicial.btnVoltar03Click(Sender: TObject);
begin
   AbrirAba(TSpeedButton(Sender).tag);
end;

procedure TfrmInicial.FormClose(Sender: TObject; var Action: TCloseAction);
begin
   Action := TCloseAction.caFree;
   FrmInicial := nil;
end;

procedure TfrmInicial.FormCreate(Sender: TObject);
begin
   fancy := TFancyDialog.Create(frmInicial);
   TabControl.ActiveTab := TabItem1;
end;

procedure TfrmInicial.FormDestroy(Sender: TObject);
begin
   fancy.disposeOf;
end;

procedure TfrmInicial.FormShow(Sender: TObject);
begin
   TimerLoad.Enabled := true;
end;

procedure TfrmInicial.TimerLoadTimer(Sender: TObject);
begin
   TimerLoad.Enabled := false;

   try
      if not Assigned(dtmUsuario) then begin
         Application.CreateForm(TdtmUsuario, dtmUsuario);
      end;
      dtmUsuario.ListarUsuarios;

      if (dtmUsuario.qryCnsUsuario.FieldByName('IND_LOGIN').AsString = 'S') then begin
         if not Assigned(FrmPrincipal) then begin
            Application.CreateForm(TFrmPrincipal, FrmPrincipal);
         end;

         //Trata a sess�o do Usu�rio
         TSession.COD_USUARIO := dtmUsuario.qryCnsUsuario.FieldByName('COD_USUARIO').AsInteger;
         TSession.NOME        := dtmUsuario.qryCnsUsuario.FieldByName('NOME').AsString;
         TSession.EMAIL       := dtmUsuario.qryCnsUsuario.FieldByName('EMAIL').AsString;
         TSession.TOKEN_JWT   := dtmUsuario.qryCnsUsuario.FieldByName('TOKEN_JWT').AsString;
         ////////////

         //Troca o Form Main para poder destruir o Atual
         Application.MainForm := FrmPrincipal;
         FrmPrincipal.Show;
         frmInicial.Close;
      end else if (dtmUsuario.qryCnsUsuario.FieldByName('ind_onboarding').AsString = 'N') then begin
         AbrirAba(3);
      end else begin
         AbrirAba(0);
      end;
   except on e: exception do
     begin
       fancy.Show(TIconDialog.Error, 'Erro', e.Message, 'Ok');
     end;
   end;
end;

end.
