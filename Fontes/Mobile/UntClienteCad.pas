unit UntClienteCad;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.Objects,
  FMX.StdCtrls, FMX.Controls.Presentation, FMX.ListBox, FMX.Layouts,
  uFancyDialog, u99Permissions, System.Sensors, System.Sensors.Components,
  uCombobox;

type
  TExecuteOnClose = procedure of object;

  TFrmClienteCad = class(TForm)
    rectToolbarProdutos: TRectangle;
    lblTitulo: TLabel;
    btnSalvar: TSpeedButton;
    Image4: TImage;
    btnVoltar: TSpeedButton;
    Image1: TImage;
    ListBox1: TListBox;
    lbiCNPJ: TListBoxItem;
    lbiNome: TListBoxItem;
    lbiTelefone: TListBoxItem;
    lbiEmail: TListBoxItem;
    lbiEndereco: TListBoxItem;
    lbiNumero: TListBoxItem;
    lbiComplemento: TListBoxItem;
    lbiBairro: TListBoxItem;
    lbiCidade: TListBoxItem;
    lbiUF: TListBoxItem;
    lbiCep: TListBoxItem;
    lbiAllBottom: TListBoxItem;
    Label1: TLabel;
    lblCPFCNPJ: TLabel;
    Image10: TImage;
    Label2: TLabel;
    lblNome: TLabel;
    Image2: TImage;
    Label4: TLabel;
    lblTelefone: TLabel;
    Image3: TImage;
    Label6: TLabel;
    lblEmail: TLabel;
    Image5: TImage;
    Label8: TLabel;
    lblEndereco: TLabel;
    Image6: TImage;
    Label10: TLabel;
    lblNumero: TLabel;
    Image7: TImage;
    Label12: TLabel;
    lblComplemento: TLabel;
    Image8: TImage;
    Label14: TLabel;
    lblBairro: TLabel;
    Image9: TImage;
    Label16: TLabel;
    lblCidade: TLabel;
    Image11: TImage;
    Label18: TLabel;
    lblUF: TLabel;
    Image12: TImage;
    Label20: TLabel;
    lblCep: TLabel;
    Image13: TImage;
    Line1: TLine;
    Line2: TLine;
    Line3: TLine;
    Line4: TLine;
    Line5: TLine;
    Line6: TLine;
    Line7: TLine;
    Line8: TLine;
    Line9: TLine;
    Line10: TLine;
    Line11: TLine;
    lbilimite: TListBoxItem;
    Image14: TImage;
    lblLimite: TLabel;
    Line12: TLine;
    btnExcluir: TSpeedButton;
    Image15: TImage;
    btnLocalizacao: TSpeedButton;
    Image16: TImage;
    LocationSensor: TLocationSensor;
    procedure btnVoltarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure btnSalvarClick(Sender: TObject);
    procedure ListBox1ItemClick(const Sender: TCustomListBox;
      const Item: TListBoxItem);
    procedure btnExcluirClick(Sender: TObject);
    procedure btnLocalizacaoClick(Sender: TObject);
    procedure LocationSensorLocationChanged(Sender: TObject; const OldLocation,
      NewLocation: TLocationCoord2D);
  private
    permissao: T99Permissions;
    fancy : TFancyDialog;
    GeoCoder: TGeoCoder;
    FCod_Cliente: integer;
    FModo : String;
    FExecuteOnClose: TExecuteOnClose;
    combo : TCustomCombo;
    latitude, longitude: double;
    procedure ClickDelete(Sender: TObject);
    procedure ErroLocalizacao(Sender: TObject);
    procedure ObterLocalizacao(Sender: TObject);
    procedure OnGeocodeReverseEvent(const Address: TCivicAddress);
    {$IFDEF MSWINDOWS}
    procedure onComboClick(Sender: TObject);
    {$ELSE}
    procedure onComboClick(Sender: TObject; const Point: TPointF);
    {$ENDIF}
    procedure FormatarCampos(Sender: TObject);
    { Private declarations }
  public
    { Public declarations }
  published
    property Modo: String read FModo write FModo;
    property Cod_Cliente: integer read FCod_Cliente write FCod_Cliente;
    property ExecuteOnClose : TExecuteOnClose read FExecuteOnClose write FExecuteOnClose;
  end;

var
  FrmClienteCad: TFrmClienteCad;

implementation

{$R *.fmx}

uses UntPrincipal, DataModule.Cliente, uFunctions, UntEdicao, uFormat;

procedure TFrmClienteCad.ClickDelete(Sender: TObject);
begin
   try
     dtmCliente.ExcluirClienteId(Cod_Cliente);
     if (Assigned(ExecuteOnClose)) then ExecuteOnClose;
     close;
   except on e: Exception do
     begin
       fancy.Show(TIconDialog.Error, 'ERRO', e.message, 'OK');
     end;
   end;
end;

procedure TFrmClienteCad.btnExcluirClick(Sender: TObject);
begin
   fancy.Show(TIconDialog.Question, 'Confirma��o', 'Confirma a exclus�o do cliente?', 'Sim', ClickDelete, 'N�o');
end;

procedure TFrmClienteCad.ObterLocalizacao(Sender: TObject);
begin
   LocationSensor.Active := true
end;

procedure TFrmClienteCad.ErroLocalizacao(Sender: TObject);
begin
   fancy.Show(TIconDialog.Error, 'Permiss�o', 'Voc� n�o possui acesso ao GPS do aparelho.', 'OK');
end;

procedure TFrmClienteCad.btnLocalizacaoClick(Sender: TObject);
begin
   permissao.Location(ObterLocalizacao, ErroLocalizacao);
end;

procedure TFrmClienteCad.btnSalvarClick(Sender: TObject);
begin
   if (Trim(lblCPFCNPJ.Text) = emptyStr) then begin
      fancy.Show(TIconDialog.Warning, 'Informe Nro. do Documento do cliente.', 'OK');
      exit;
   end;
   if (Trim(lblNome.Text) = emptyStr) then begin
      fancy.Show(TIconDialog.Warning, 'Informe o nome do cliente.', 'OK');
      exit;
   end;
   if (Trim(lblEmail.Text) = emptyStr) then begin
      fancy.Show(TIconDialog.Warning, 'Informe o email do cliente.', 'OK');
      exit;
   end;
   if (StringToFloat(lblLimite.Text) <= 0.00) then begin
      fancy.Show(TIconDialog.Warning, 'Informe o Limite de Cr�dito do cliente', 'OK');
      exit;
   end;

   try
      if (Modo = 'I') then begin
         dtmCliente.InserirClienteId(
            lblCPFCNPJ.Text,
            lblNome.Text,
            lblTelefone.Text,
            lblEmail.Text,
            lblEndereco.Text,
            lblNumero.Text,
            lblComplemento.Text,
            lblBairro.Text,
            lblCidade.Text,
            lblUF.Text,
            lblCep.Text,
            StringToFloat(lblLimite.Text),
            latitude,
            longitude,
            'S',
            0
         );
      end else begin
         dtmCliente.EditarClienteId(
            Cod_Cliente,
            lblCPFCNPJ.Text,
            lblNome.Text,
            lblTelefone.Text,
            lblEmail.Text,
            lblEndereco.Text,
            lblNumero.Text,
            lblComplemento.Text,
            lblBairro.Text,
            lblCidade.Text,
            lblUF.Text,
            lblCep.Text,
            'S',
            StringToFloat(lblLimite.Text),
            latitude,
            longitude
         );
      end;

      if (Assigned(ExecuteOnClose)) then ExecuteOnClose;

      close;
   except on e: exception do
     begin
        fancy.Show(TIconDialog.Error, 'Erro ao salvar dados do cliente: '+e.Message, 'OK');
     end;
   end;
end;

procedure TFrmClienteCad.btnVoltarClick(Sender: TObject);
begin
   close;
end;

procedure TFrmClienteCad.FormClose(Sender: TObject; var Action: TCloseAction);
begin
   Action := TCloseAction.caFree;
   FrmClienteCad := nil;
end;

{$IFDEF MSWINDOWS}
procedure TFrmClienteCad.onComboClick(Sender: TObject);
begin
    combo.HideMenu;
    lblUF.Text := combo.CodItem;
    //lblUF.Text := combo.DescrItem;
end;
{$ELSE}
procedure TFrmClienteCad.onComboClick(Sender: TObject; const Point: TPointF);
begin
    combo.HideMenu;
    lblUF.Text := combo.CodItem;
end;
{$ENDIF}

procedure TFrmClienteCad.FormCreate(Sender: TObject);
begin
   permissao:= T99Permissions.Create;
   fancy    := TFancyDialog.create(FrmClienteCad);
   combo    := TCustomCombo.Create(FrmClienteCad);

   // Listagem dos estados...
   combo := TCustomCombo.Create(FrmClienteCad);
   combo.TitleMenuText := 'Estado do Cliente';
   //combo.TitleFontSize := 16;
   //combo.TitleFontColor := $FF4162FF;
   //combo.SubTitleMenuText := 'Escolha uma UF';
   //combo.SubTitleFontSize := 14;
   //combo.SubTitleFontColor := $FFA3A3A3;
   combo.BackgroundColor := $FFF2F2F8;
   combo.ItemBackgroundColor := $FFFFFFFF;
   //combo.ItemFontSize := 15;
   //combo.ItemFontColor := $FF000000;

   combo.AddItem('AC', 'Acre');
   combo.AddItem('AL', 'Alagoas');
   combo.AddItem('AP', 'Amap�');
   combo.AddItem('AM', 'Amazonas');
   combo.AddItem('BA', 'Bahia');
   combo.AddItem('CE', 'Cear�');
   combo.AddItem('DF', 'Distrito Federal');
   combo.AddItem('ES', 'Esp�rito Santo');
   combo.AddItem('GO', 'Goi�s');
   combo.AddItem('MA', 'Maranh�o');
   combo.AddItem('MT', 'Mato Grosso');
   combo.AddItem('MS', 'Mato Grosso do Sul');
   combo.AddItem('MG', 'Minas Gerais');
   combo.AddItem('PA', 'Par�');
   combo.AddItem('PB', 'Para�ba');
   combo.AddItem('PR', 'Paran�');
   combo.AddItem('PE', 'Pernambuco');
   combo.AddItem('PI', 'Piau�');
   combo.AddItem('RJ', 'Rio de Janeiro');
   combo.AddItem('RN', 'Rio Grande do Norte');
   combo.AddItem('RS', 'Rio Grande do Sul');
   combo.AddItem('RO', 'Rond�nia');
   combo.AddItem('RR', 'Roraima');
   combo.AddItem('SC', 'Santa Catarina');
   combo.AddItem('SP', 'S�o Paulo');
   combo.AddItem('SE', 'Sergipe');
   combo.AddItem('TO', 'Tocantins');

   combo.OnClick := onComboClick;
end;

procedure TFrmClienteCad.FormDestroy(Sender: TObject);
begin
   fancy.DisposeOf;
   permissao.DisposeOf;
   combo.DisposeOf;

   if (Assigned(GeoCoder)) then begin
      GeoCoder.DisposeOf;
   end;
end;

procedure TFrmClienteCad.FormShow(Sender: TObject);
begin
   lblTitulo.text   := 'Incluir Cliente';
   try
     btnExcluir.Visible := (Modo = 'A');
     latitude := 0;
     longitude:= 0;

     if (Modo = 'A') then begin
        dtmCliente.ListarClienteId(Cod_Cliente, 0);

        lblCPFCNPJ.text:= dtmCliente.qryCliente.fieldByName('CNPJ_CPF').AsString;
        lblNome.text:= dtmCliente.qryCliente.fieldByName('NOME').AsString;
        lblTelefone.text:= dtmCliente.qryCliente.fieldByName('FONE').AsString;
        lblEmail.text:= dtmCliente.qryCliente.fieldByName('EMAIL').AsString;
        lblEndereco.text:= dtmCliente.qryCliente.fieldByName('ENDERECO').AsString;
        lblNumero.text:= dtmCliente.qryCliente.fieldByName('NUMERO').AsString;
        lblComplemento.text:= dtmCliente.qryCliente.fieldByName('COMPLEMENTO').AsString;
        lblBairro.text:= dtmCliente.qryCliente.fieldByName('BAIRRO').AsString;
        lblCidade.text:= dtmCliente.qryCliente.fieldByName('CIDADE').AsString;
        lblUF.text:= dtmCliente.qryCliente.fieldByName('UF').AsString;
        lblCep.text:= dtmCliente.qryCliente.fieldByName('CEP').AsString;
        lblLimite.text:= FormatFloat('###,###,##0.00', dtmCliente.qryCliente.fieldByName('LIMITE_DISPONIVEL').AsFloat);

        latitude := dtmCliente.qryCliente.fieldByName('LATITUDE').AsFloat;
        longitude:= dtmCliente.qryCliente.fieldByName('LONGITUDE').AsFloat;

        lblTitulo.text   := 'Editar Cliente';
     end;
   except on e: exception do
     begin
        fancy.Show(TIconDialog.Error, 'Erro ao carregar dados do produto: '+#13+e.message, 'OK');
     end;
   end;
end;

procedure TFrmClienteCad.FormatarCampos(Sender: TObject);
begin
    if TLabel(Sender) = lblCPFCNPJ then
        Formatar(Sender, TFormato.CNPJorCPF)
    else if TLabel(Sender) = lblTelefone then
        Formatar(Sender, TFormato.TelefoneFixo)
    else if TLabel(Sender) = lblCEP then
        Formatar(Sender, TFormato.CEP);
end;

procedure TFrmClienteCad.ListBox1ItemClick(const Sender: TCustomListBox; const Item: TListBoxItem);
begin
   if (Item = lbiCNPJ) then begin
      FrmEdicao.Editar(lblCPFCNPJ, TTipoCampo.Edit, 'CNPJ/CPF do Cliente',
                       'Informe o CNPJ ou CPF', lblCPFCNPJ.text, true, 20, FormatarCampos);
   end else if (Item = lbiNOME) then begin
      FrmEdicao.Editar(lblNome, TTipoCampo.Edit, 'Nome do Cliente',
                       'Informe o Nome do Cliente', lblNome.text, true, 100);
   end else if (Item = lbiTelefone) then begin
      FrmEdicao.Editar(lblTELEFONE, TTipoCampo.Edit, 'Telefone do Cliente',
                       'Informe o Telefone do Cliente', lblTELEFONE.text, false, 20, FormatarCampos);
   end else if (Item = lbiEmail) then begin
      FrmEdicao.Editar(lblEmail, TTipoCampo.Edit, 'Email do Cliente',
                       'Informe o Email do Cliente', lblEmail.text, true, 100);
   end else if (Item = lbiEndereco) then begin
      FrmEdicao.Editar(lblEndereco, TTipoCampo.Memo, 'Endere�o do Cliente',
                       'Informe o endere�o do cliente', lblEndereco.text, false, 500);
   end else if (Item = lbiNumero) then begin
      FrmEdicao.Editar(lblNumero, TTipoCampo.Edit, 'N�mero Ende. do Cliente',
                       'Informe o Nro. do Ende. do Cliente', lblNumero.text, false, 50);
   end else if (Item = lbiComplemento) then begin
      FrmEdicao.Editar(lblComplemento, TTipoCampo.Edit, 'Complemento ende. do Cliente',
                       'Informe o complemento do End. do Cliente', lblComplemento.text, false, 50);
   end else if (Item = lbiBairro) then begin
      FrmEdicao.Editar(lblBairro, TTipoCampo.Edit, 'Bairro do Cliente',
                       'Informe o Bairro do Cliente', lblBairro.text, false, 50);
   end else if (Item = lbiCidade) then begin
      FrmEdicao.Editar(lblCidade, TTipoCampo.Edit, 'Cidade do Cliente',
                       'Informe a cidade do cliente', lblCidade.text, true, 50);
   end else if (Item = lbiUf) then begin
      combo.ShowMenu;
      //FrmEdicao.Editar(lblUf, TTipoCampo.Edit, 'UF do Cliente',
      //                 'Informe a UF do cliente', lblUF.text, true, 2);
   end else if (Item = lbiCep) then begin
      FrmEdicao.Editar(lblCEP, TTipoCampo.Edit, 'CEP do Cliente',
                       'Informe o CEP do cliente', lblCEP.text, false, 10, FormatarCampos);
   end else if (Item = lbiLimite) then begin
      FrmEdicao.Editar(lblLimite, TTipoCampo.Valor, 'Limite de Cr�dito',
                       'Informe o limite para o cliente', lblLimite.text, true, 0);
   end;


end;

procedure TFrmClienteCad.OnGeocodeReverseEvent(const Address: TCivicAddress);
begin
   lblEndereco.Text := Address.Thoroughfare;
   lblNumero.Text   := Address.SubThoroughfare;
   lblBairro.Text   := Address.SubLocality;
   lblCidade.Text   := Address.Locality;
   lblUF.Text       := ObterUF(Address.AdminArea);
   lblCep.Text      := Address.PostalCode;

    {
    AdminArea: Estado (UF);
    CountryCode: BR;
    CountryName: Pa�s;
    FeatureName: Numero;
    Locality: Cidade;
    PostalCode: CEP;
    SubAdminArea: Cidade;
    SubLocality: Bairro;
    SubThoroughfare: Numero da rua;
    Thoroughfare: Rua sem o numero;
    }
end;

procedure TFrmClienteCad.LocationSensorLocationChanged(Sender: TObject;
  const OldLocation, NewLocation: TLocationCoord2D);
begin
   LocationSensor.Active := false;
   latitude := NewLocation.Latitude;
   longitude:= NewLocation.Longitude;

   try
     if not (Assigned(GeoCoder)) then begin
        if Assigned(TGeocoder.Current) then begin
           GeoCoder := TGeocoder.Current.Create;
        end;
        if Assigned(Geocoder) then begin
           GeoCoder.OnGeocodeReverse := OnGeocodeReverseEvent;
        end;
     end;

     //Se conseguiu instanciar e n�o est� executando o GPS no momento
     if (Assigned(GeoCoder)) and (not  GeoCoder.Geocoding) then begin
        GeoCoder.GeocodeReverse(NewLocation);
     end;
   except on e: exception do
     begin
        fancy.Show(TIconDialog.Error, 'Erro', 'Ocorreu um erro: '+e.Message);
     end;
   end;
end;

end.
