unit UntProdutoCad;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.Objects,
  FMX.StdCtrls, FMX.Controls.Presentation, FMX.Layouts, uActionSheet, u99Permissions,
  FMX.MediaLibrary.Actions, System.Actions, FMX.ActnList, FMX.StdActns,
  FMX.DialogService, uFancyDialog;

type
  TExecuteOnClose = procedure of object;

  TFrmProdutoCad = class(TForm)
    rectToolbarProdutos: TRectangle;
    lblTitulo: TLabel;
    btnSalvar: TSpeedButton;
    Image4: TImage;
    btnVoltar: TSpeedButton;
    Image1: TImage;
    Layout1: TLayout;
    imgFoto: TImage;
    lblFoto: TLabel;
    rectDescricao: TRectangle;
    Label1: TLabel;
    Image10: TImage;
    lblDescricao: TLabel;
    rectValor: TRectangle;
    Label2: TLabel;
    Image2: TImage;
    lblValor: TLabel;
    rectEstoque: TRectangle;
    Label3: TLabel;
    Image3: TImage;
    lblEstoque: TLabel;
    ActionList1: TActionList;
    ActBlbiotecaFotos: TTakePhotoFromLibraryAction;
    ActCamera: TTakePhotoFromCameraAction;
    OpenDialog: TOpenDialog;
    Layout2: TLayout;
    btnExcluir: TSpeedButton;
    Image5: TImage;
    procedure btnVoltarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure imgFotoClick(Sender: TObject);
    procedure ActBlbiotecaFotosDidFinishTaking(Image: TBitmap);
    procedure ActCameraDidFinishTaking(Image: TBitmap);
    procedure rectDescricaoClick(Sender: TObject);
    procedure rectValorClick(Sender: TObject);
    procedure rectEstoqueClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnSalvarClick(Sender: TObject);
    procedure btnExcluirClick(Sender: TObject);
  private
    { Private declarations }
    menu  : TActionSheet;
    permissao: T99Permissions;
    fancy : TFancyDialog;
    FCod_Produto: integer;
    FModo: String;
    FExecuteOnClose: TExecuteOnClose;
    procedure ClickBliotecaFotos(Sender: TObject);
    procedure ClickTirarFoto(Sender: TObject);
    procedure ErrorPermissaoFotos(Sender: TObject);
    procedure ClickDelete(Sender: TObject);
  public
    { Public declarations }
  published
    property Modo: String read FModo write FModo;
    property Cod_Produto: integer read FCod_Produto write FCod_Produto;
    property ExecuteOnClose : TExecuteOnClose read FExecuteOnClose write FExecuteOnClose;
  end;

var
  FrmProdutoCad: TFrmProdutoCad;

implementation

{$R *.fmx}

uses UntPrincipal, UntEdicao, DataModule.Produto, uFunctions, Data.DB;

procedure TFrmProdutoCad.ActBlbiotecaFotosDidFinishTaking(Image: TBitmap);
begin
   imgFoto.Bitmap := Image;
end;

procedure TFrmProdutoCad.ActCameraDidFinishTaking(Image: TBitmap);
begin
   imgFoto.Bitmap := Image;
end;

procedure TFrmProdutoCad.ClickDelete(Sender: TObject);
begin
   try
     dtmProduto.ExcluirProdutoId(Cod_Produto);
     if (Assigned(ExecuteOnClose)) then ExecuteOnClose;
     close;
   except on e: Exception do
     begin
       fancy.Show(TIconDialog.Error, 'Aviso', e.message, 'OK');
     end;
   end;
end;

procedure TFrmProdutoCad.btnExcluirClick(Sender: TObject);
begin
   fancy.Show(TIconDialog.Question, 'Confirma��o', 'Confirma a exclus�o do produto?', 'Sim', ClickDelete, 'N�o');
   { TDialogService.MessageDialog(
       'Confirma a exclus�o do produto?',
       TMsgDlgType.mtConfirmation,
       [TMsgDlgBtn.mbYes, TMsgDlgBtn.mbNo],
       TMsgDlgBtn.mbNo,
       0,
       procedure(const AResult: TModalResult)
       begin
          if (AResult = mrYes) then begin
             try
               dtmProduto.ExcluirProdutoId(Cod_Produto);

               if (Assigned(ExecuteOnClose)) then ExecuteOnClose;
               close;
             except on e: Exception do
               begin
                  ShowMessage(e.Message);
               end;
             end;
          end;
       end
    ); }
end;

procedure TFrmProdutoCad.btnSalvarClick(Sender: TObject);
begin
   if (Trim(lblDescricao.Text) = emptyStr) then begin
      fancy.Show(TIconDialog.Warning, 'Informe a descri��o do Produto', 'OK');
      exit;
   end;

   try
      if (Modo = 'I') then begin
         dtmProduto.InserirProdutoId(
            lblDescricao.Text,
            'S',
            StringToFloat(lblValor.Text),
            lblEstoque.Text.ToInteger(),
            imgFoto.Bitmap,
            0
         );
      end else begin
         dtmProduto.EditarProdutoId(
            Cod_Produto,
            lblDescricao.Text,
            'S',
            StringToFloat(lblValor.Text),
            lblEstoque.Text.ToInteger(),
            imgFoto.Bitmap
         );
      end;

      if (Assigned(ExecuteOnClose)) then ExecuteOnClose;

      close;
   except on e: exception do
     begin
        fancy.Show(TIconDialog.Error, 'Erro ao salvar dados do produto: '+e.Message, 'OK');
     end;
   end;
end;

procedure TFrmProdutoCad.btnVoltarClick(Sender: TObject);
begin
   close;
end;

procedure TFrmProdutoCad.FormClose(Sender: TObject; var Action: TCloseAction);
begin
   Action := TCloseAction.caFree;
   FrmProdutoCad := nil;
end;

procedure TFrmProdutoCad.ErrorPermissaoFotos(Sender: TObject);
begin
   fancy.Show(TIconDialog.Error, 'Voc� n�o possui acesso a esse recurso no aparelho', 'OK');
end;

procedure TFrmProdutoCad.ClickBliotecaFotos(Sender: TObject);
begin
   menu.HideMenu;

   permissao.PhotoLibrary(ActBlbiotecaFotos, ErrorPermissaoFotos);
end;

procedure TFrmProdutoCad.ClickTirarFoto(Sender: TObject);
begin
   menu.HideMenu;

   permissao.Camera(ActCamera, ErrorPermissaoFotos);
end;

procedure TFrmProdutoCad.FormCreate(Sender: TObject);
begin
   permissao := T99Permissions.Create;
   fancy := TFancyDialog.Create(FrmProdutoCad);

   menu  := TActionSheet.Create(FrmProdutoCad);

   menu.TitleFontSize := 12;
   menu.TitleMenuText := 'O que deseja fazer?';
   menu.TitleFontColor:= $FFA3A3A3;

   menu.CancelMenuText := 'Cancelar';
   menu.CancelFontSize := 15;
   menu.CancelFontColor:= $FFDA4F3F;

   menu.BackgroundOpacity := 0.5;
   menu.MenuColor := $FFFFFFFF;

   menu.AddItem('', 'Biblioteca de Fotos', ClickBliotecaFotos);
   menu.AddItem('', 'Tirar Foto', ClickTirarFoto);
end;

procedure TFrmProdutoCad.FormDestroy(Sender: TObject);
begin
   menu.DisposeOf;
   fancy.DisposeOf;
   permissao.DisposeOf;
end;

procedure TFrmProdutoCad.FormShow(Sender: TObject);
begin
   lblTitulo.text   := 'Incluir Produto';
   try
     btnExcluir.Visible := (Modo = 'A');

     if (Modo = 'A') then begin
        dtmProduto.ListarProdutoId(Cod_Produto, 0);

        if (Trim(dtmProduto.qryProduto.fieldByName('FOTO').asString) <> emptyStr) then begin
           LoadBitMapFromBlob(imgFoto.Bitmap, TBlobField(dtmProduto.qryProduto.fieldByName('FOTO')));
        end;

        lblDescricao.text:= dtmProduto.qryProduto.fieldByName('DESCRICAO').AsString;
        lblValor.text    := FormatFloat('#,###,###,##0.00', dtmProduto.qryProduto.fieldByName('VALOR').AsFloat);
        lblEstoque.text  := FormatFloat('###0', dtmProduto.qryProduto.fieldByName('QTD_ESTOQUE').AsFloat);
        lblTitulo.text   := 'Editar Produto';
     end;
   except on e: exception do
     begin
        fancy.Show(TIconDialog.Error, 'Erro ao carregar dados do produto: '+#13+e.message, 'OK');
     end;
   end;
end;

procedure TFrmProdutoCad.imgFotoClick(Sender: TObject);
begin
   {$IFDEF MSWINDOWS}
   if (OpenDialog.Execute) then begin
      imgFoto.Bitmap.LoadFromFile(OpenDialog.FileName);
   end;
   {$ELSE}
   menu.ShowMenu;
   {$ENDIF}
end;

procedure TFrmProdutoCad.rectDescricaoClick(Sender: TObject);
begin
   FrmEdicao.Editar(
      lblDescricao,
      TTipoCampo.Edit,
      'Descri��o do Produto',
      'Informe a Descri��o',
      lblDescricao.text,
      true,
      200
   );
end;

procedure TFrmProdutoCad.rectEstoqueClick(Sender: TObject);
begin
   FrmEdicao.Editar(
      lblEstoque,
      TTipoCampo.Inteiro,
      'Quantidade Estoque',
      '',
      lblEstoque.text,
      true,
      0
   );
end;

procedure TFrmProdutoCad.rectValorClick(Sender: TObject);
begin
   FrmEdicao.Editar(
      lblValor,
      TTipoCampo.Valor,
      'Valor do Produto',
      '',
      lblValor.text,
      true,
      0
   );
end;

end.
